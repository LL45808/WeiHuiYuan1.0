import {Util} from "../../../utils/common";

var app = getApp()
const page_code = 'wxpro_crm_integral_rules'
Page({
    /**
     * 页面的初始数据
     */
    data: {
        page_configs: {
            integral_rules_illu: '',
            integral_rules_get: '',
            richtext: {nodes: ''}
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.page_config();
    },
    /**
     * 积分说明接口获取
     */
    page_config: function () {
        var that = this;
        app.crmApi(
            app.apiService.crm.pageConfig,
            {page_code: page_code},
            that.pageConfigSuccess,
        )
    },
    pageConfigSuccess: function (res) {
        var that = this
        const config = res.data.data[page_code].configs;

        var content = Util.get(config, 'integral_rules_illu.value_text', '');
        // console.log("content",content);
        // var temp_get = app.convertHtmlToText(that.data.page_configs.integral_rules_get);
        getApp().wxParse.wxParse('richtext', 'html', content, this, 5);
    },

    /**
     * 积分说明接口获取 失败
     */
    IntegralIlluFail: function (res) {
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
