var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      award:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var page = this;
      page.awardList();
  },
  awardList: function () {
      var that = this;
      wx.request({
          url: 'https://apixinya.tianxiarongtong.com/',
          data: {
              service: 'App.Pos.GetWinningList',
              token: app.storage.getAuth().token,
              user_id: '1'
          },
          success: function (res) {
              if (res.data.ret = 200) {
                  that.setData({
                      awardItem: res.data.data.data
                  })
              }
          }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})