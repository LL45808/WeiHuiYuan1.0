var tcity = require("../../utils/citys.js");
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    provinces: [],
    province: "",
    citys: [],
    city: "",
    countys: [],
    county: '',
    value: [0, 0, 0],
    values: [0, 0, 0],
    condition: false,
    addrList: [],
    address_id: [],
    default: true,
    addrNo: false,
    addr_url_type: '',
    addr_list: undefined,
    is_switch: true,//开关
    shop_id: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var page = this;
    /**
     * 获取加载插件实例
     */
    page.loadingBox = page.selectComponent('#loading')

    page.loadingBox.showLoading()

    page.dialog = page.selectComponent('#dialog')

    //提领券地址
    var addr_url_type = ''

    if (options.type == 1) {

      page.setData({

        addr_url_type: 1,
        shop_id: options.shop_id

      })

    } else if (options.type == 'store') {

      page.setData({

        addr_url_type: 2

      })

    }

    //外卖地址
    if (options.type == 2) {

      page.setData({

        addr_url_type: 2,

        shop_id: options.shop_id

      })

    }

  },

  //外卖和提领券兑换获取地址
  TLconponTap: function (e) {
    var that = this
    var addr_url_type = that.data.addr_url_type
    var list = that.data.addrList//addrList中的值
    var idx = e.currentTarget.dataset.idx//当前索引值
    var data = list[idx]
    if (addr_url_type == 1) {
      var pagelist = getCurrentPages();
      if (data.is_out_of_range == 1) {
        this.dialog.showDialog({
          'type': 'warning',
          'title': '超出范围！',
          'content': '该地址不在配送范围内，请重新选择～'
        })
        return
      }
      if (pagelist.length > 1) {
        that.setData({
          addr_url_type: ''
        })
        //获取上一个页面实例对象
        var prePage = pagelist[pagelist.length - 2];
        prePage.getBackData(data);
        wx.navigateBack({
          delta: 1
        })
      }
    } else if (addr_url_type == 2) {
      var pagelist = getCurrentPages();
      if (pagelist.length > 1) {
        that.setData({
          addr_url_type: ''
        })
        //获取上一个页面实例对象
        var prePage = pagelist[pagelist.length - 2];
        prePage.getBackData(data);
        wx.navigateBack({
          delta: 1
        })
      }
    }
  },

  /**
   * 地址列表接口
   */
  address: function () {
    var that = this;
    const params = {
      token: app.storage.getAuth().token,
      shop_id: that.data.shop_id
    }
    
    app.crmApi(
      app.apiService.crm.getAddressList, params,
      res => {

        that.loadingBox.hideAniEnd()
        var addr_list = res.data.data.data
        
        if (res.data.data.records == 0 || !res.data.data.records) {
          that.setData({
            addrNo: true,
          })
          return
        }
        
        if (res.data.ret == 200) {
          that.setData({
            addrList: addr_list
          })
        }
      },
      msg => {

        that.loadingBox.hideAniEnd()
      }
    )
  },

  //删除地址接口
  addressDel: function (addressid) {
    var that = this;
    const params = {
      token: app.storage.getAuth().token,
      address_id: addressid
    }
    app.crmApi(
      app.apiService.crm.addrDel, params,
      res => {
        if (res.data.ret = 200) {
        }
      }
    )
  },
  //删除地址
  addrDeleTap: function (e) {
    var that = this;
    wx.showModal({
      title: '',
      content: '是否删除该地址',
      success: function (res) {
        if (res.confirm) {
          var currentid = e.currentTarget.dataset.addressid//data-addressid
          /**** 查找唯一索引，删除**** */
          var idx = e.currentTarget.dataset.idx
          var list = that.data.addrList
          list.splice(idx, 1)
          that.setData({
            addrList: list
          })
          that.addressDel(currentid)
        }
      }
    })
  },
  //编辑地址
  addrEditTap: function (e) {
    var that = this
    var list = that.data.addrList//addrList中的值
    var idx = e.currentTarget.dataset.idx//当前索引值
    var data = JSON.stringify(list[idx])  //JSON.stringify()从一个对象中解析出字符串
    if (!that.data.is_switch) {
      return
    }
    else {
      wx.navigateTo({
        url: '/pages/address/edit/edit?address=' + data
      })
      that.setData({
        is_switch: false
      })
    }

  },
  /**
   * 添加新地址链接跳转
   */
  addAddrTap: function () {
      wx.navigateTo({
        url: '/pages/address/edit/edit'
      })
  },
  //单选按钮判断
  radioCheck: function (e) {
    var that = this
    var list = that.data.addrList//addrList中的值
    for (var i = 0; i < list.length; i++) {
      list[i].default = false
      if (e.currentTarget.dataset.idx == i) {
        list[i].default = true
      }
    }
    that.setData({
      addrList: list
    })
    that.set_default(e.currentTarget.id);
  },
  //设为默认接口
  set_default: function (addressid) {

    var that = this;

    var data = {

      token: app.storage.getAuth().token,

        address_id: addressid

    }
    app.crmApi(

      app.apiService.crm.addrDefault,

      data,

      that.balanceListSuccess,

      that.balanceListFail,

      extraParams

    )

  },

  balanceListSuccess: function (res) {

    var that = this;

    var info = res.data

    if (info.qw == 200) {

    } else {

    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    var that = this
    that.setData({
      is_switch: true
    })
    var address_list = wx.getStorageSync('address_list')
    var addrNos = wx.getStorageSync('addrNo')
    if (address_list) {
      that.setData({
        addrList: address_list,
        addrNo: addrNos
      })
    }
    that.address()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
