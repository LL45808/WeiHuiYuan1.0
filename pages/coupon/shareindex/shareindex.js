// pages/coupon/shareindex/shareindex.js
import {getCouponService} from "../../../module/user/coupon";
import {Toast} from "../../../utils/Toast";
import {UserUtil} from "../../../utils/UserUtil";
import {Util} from "../../../utils/common";
import pageConfig from "../../../utils/page";
import PageUtil, {PageRedirectUtil} from "../../../utils/PageUtil";
import ValidateUtil from "../../../utils/ValidateUtil";
import {getUserService} from "../../../module/user/user";

const config = {
    /**
     * 页面的初始数据
     */
    data: {
        couponValid: true,
        donateUsername: '',
        donateCardId: '',
        coupon: {
            name: '',
            type: '',
            startDate: '',
            endDate: '',
            id: 0
        },
        userAuth: true,
        userBean: null,
        couponCode: 0
    },
    receive: function () {
        console.log('receive');
        this.loading();
        getCouponService().fetchDonateCoupon({
            id: this.data.coupon.id,
            mobile: UserUtil.getUserBean().getMobile()
        }, (response) => {
            this.closeLoading();
            Toast.tip(response.getMsg(), {
                end: () => {
                    if (response.isSuccess()) {
                        //更新erp数据
                        getUserService().clearErpUserInfo();
                        PageRedirectUtil.clear();
                        if (PageUtil.hasPage('index')) {
                            PageUtil.redirectTo('couponlist');
                        } else {
                            PageUtil.redirectTo('index');
                        }

                    }
                }
            });
        });
    },
    toIndex: function () {
        PageRedirectUtil.clear();
        PageUtil.redirectTo('index');
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // PageRedirectUtil.parse(options);
        this.customOnload(options);
        console.log("share options", options);
        const couponCode = Util.get(options, 'couponCode', 0);
        this.data.couponCode = couponCode;
        this.loading();
        getCouponService().getDonateCoupon({couponCode: couponCode}, (response) => {
            this.closeLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg() + ",3秒后将返回首页");
                this.setData({couponValid: false});
                //返回首页
                setTimeout(() => {
                    PageUtil.redirectTo('index');
                }, 3000);
                // PageUtil.redirectTo('index');
                return;
            }
            const bean = response.getData();
            const coupon = {
                name: bean.getName(),
                type: bean.getType(),
                startDate: bean.getStartDate(),
                endDate: bean.getEndDate(),
                id: bean.getId()
            };
            this.setData({coupon: coupon, donateUsername: bean.getDonateUsername(), fromCardId: bean.getFromCardId()});
        });
        this.fetchWxUserInfo();
    },
    fetchWxUserInfo() {
        console.log("userInfo", UserUtil.getUserInfo());
        const userInfo = UserUtil.getUserInfo();
        if (Util.isEmpty(userInfo)) {
            PageRedirectUtil.clear();
            PageUtil.redirectTo('index');
        } else {
            const mobile = Util.get(userInfo, 'phone', '');
            if (!ValidateUtil.isMobile(mobile)) {
                PageRedirectUtil.clear();
                PageUtil.redirectTo('index');
            }
        }
        /* const _this = this;
         wx.getSetting({
             success(res) {
                 if (res.authSetting['scope.userInfo']) {
                     _this.setData({userAuth: true});
                     /!*  getUserService().fetchWxUserInfo((response) => {
                           if (!response.isSuccess()) {
                               Toast.tip(response.getMsg());
                               return;
                           }
                       });*!/
                     console.log("userInfo", UserUtil.getUserInfo());
                     const userInfo = UserUtil.getUserInfo();
                     if (Util.isEmpty(userInfo)) {
                         PageUtil.redirectTo('index');
                     } else {
                         const mobile = Util.get(userInfo, 'phone', '');
                         if (!ValidateUtil.isMobile(mobile)) {
                             PageUtil.redirectTo('index');
                         }
                     }
                 } else {
                     //未授权获取用户信息，进入首页
                     PageUtil.redirectTo('index');
                     _this.setData({userAuth: false});
                 }
             },
             fail() {
                 _this.setData({userAuth: false});
             }
         });*/
    },
    // 确认授权回调
    onMyAccredit(e) {
        var that = this;
        if (e.detail.data.errMsg == "getUserInfo:ok") {
            this.fetchWxUserInfo();
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
