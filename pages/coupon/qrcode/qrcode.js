var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    qrcode: "",
    barcode:'',
    coupon_code:'',

    shops: "",

    validate_time: "",

    at_least: ""

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var that = this

    var time = ''

    if (options.last_long == "1") {

      time = '长期有效'

    } else {

      time = options.start_time + ' ~ ' + options.end_time

    }

    that.setData({

      qrcode: options.qrcode,

      coupon_code: options.coupon_code,

      at_least: parseFloat(options.at_least) ? ('消费满 ¥ ' + options.at_least + " 可用") : '不限制',

      validate_time: time

    })

    that.exchange(options.coupon_code)

  },
  /**
   * 优惠券兑换二维码接口
   */
  exchange: function (coupon_code) {
    var that = this;
    var data = {
      token: app.storage.getAuth().token,
      coupon_code: coupon_code
    }
    app.crmApi(
      app.apiService.crm.couponExchange,
      data,
      that.exchangeSuccess
    )
  },
  exchangeSuccess: function (res) {
    var that = this;
    if (res.data.ret == 200) {
      that.setData({
        qrcode: res.data.data.qr_code,
        barcode: res.data.data.barcode,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})