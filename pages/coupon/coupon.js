import PageUtil from "../../utils/PageUtil";

var app = getApp()
import RequestUtil from "../../module/api/request";
import {
  ApiConst
} from "../../module/api/const";
import {
  Util
} from "../../utils/common";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    couponItem: [], //优惠券列表
    norecord: false, //暂无记录
    page: 1, //下一个页码
    page_count: 0, //总页码数
    page_num: 30, //每页显示条数
    nomore: false, //已加载所有
    actTabs: ['active', '', '', '', ''],
    addr_url_type: '',
    ispramt: false, //使用条件弹窗开关
    itemstate: 0,
    accId: 0,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    /**
     * 获取加载插件实例
     */
    that.loadingBox = that.selectComponent('#loading')

    that.loadingBox.showLoading()

    if (options.type == 1) {
      that.setData({
        addr_url_type: 1
      })
    }
    that.couponList(1, that.data.page, that.data.page_num, 0);

  },

  /**
   * 优惠券二维码兑换跳转
   */
  exchangeTap: function(e) {
    var that = this;
    var idx = e.currentTarget.dataset.idx;
    var info = that.data.couponItem;
    var couponInfo = info[idx]
    if (that.data.addr_url_type == 1) {
      var pagelist = getCurrentPages();
      if (pagelist.length > 1) {
        //获取上一个页面实例对象
        var prePage = pagelist[pagelist.length - 2];
        prePage.getCouponData(couponInfo);
        wx.navigateBack({
          delta: 1
        })
      }
    } else {
      var data = {}
      wx.navigateTo({
        url: '/pages/coupon/qrcode/qrcode?qrcode=' + info[idx].qr_code + '&coupon_code=' + info[idx].coupon_code + '&shops=' + info[idx].shops + '&at_least=' + info[idx].at_least + '&start_time=' + info[idx].start_time + '&end_time=' + info[idx].end_time + '&last_long=' + info[idx].last_long,
      })
    }
  },
  /**弹框 */
  ifdatil: function(e) {
    var p = this;
    var sta = e.currentTarget.dataset.statu;
    var idx = e.currentTarget.dataset.idx;
    if (sta == 'start') {
      console.log('str', e, p.data.couponItem)
      var data = p.data.couponItem[idx];
      console.log(data)
      p.setData({
        ispramt: !p.data.ispramt,
        tanname: data.coupon_name,
        use_amount_limit: data.use_amount_limit,
        goods_range: data.goods_range,
        note: data.note,
      })
    } else if (sta == 'close') {
      console.log('clo', e)
      p.setData({
        ispramt: !p.data.ispramt
      })
    }

  },
  /**
   * 优惠券列表接口获取
   */
  couponList: function(func, page, page_num, types) {
    //参数func,等于1加载第一页，等于2刷新当前页，等于3加载下一页
    var that = this;
    var data = {
      token: app.storage.getAuth().token,
      page: page,
      page_num: page_num,
      type: types
    }
    var extraParams = {
      func: func,
      page: page,
      page_num: page_num
    }
    app.crmApi(
      app.apiService.crm.couponList,
      data,
      that.couponListSuccess,
      that.couponListFail,
      extraParams
    )
  },
  /**
   * 优惠券列表接口获取 成功
   */
  couponListSuccess: function(res, param) {
    var that = this;

    that.loadingBox.hideAniEnd()
    /**
     * 去除日期的时分秒
     */
    var newData = res.data.data.data;
    var start_time = [];
    var end_time = [];
    var money = [];
    for (var i = 0; i < newData.length; i++) {
      var obj = newData[i];

      if (obj.percentage < 1) {
        //console.log(obj.percentage + '*' + '10' + '=' + obj.percentage * 10, Util.retainPoint(0.681 * 10, 2, true), 'asdasdsdasd')
        obj.percentDiscount = Util.retainPoint(obj.percentage * 10, 2, true);
      } else {
        obj.percentDiscount = Util.retainPoint(obj.percentage / 10, 2, true).toFixed(2);
      }
      money[i] = parseInt(newData[i].money);
      newData[i].money = money[i];
      if (newData[i].start_time) {
        start_time[i] = newData[i].start_time;
        var startTime = start_time[i].substr(0, 10);
        newData[i].start_time = startTime;
      }

      if (newData[i].end_time) {
        end_time[i] = newData[i].end_time;
        var endTime = end_time[i].substr(0, 10);
        newData[i].end_time = endTime;
      }
    }
    var func = param.func;
    var page = param.page;
    var page_num = param.page_num;
    var info = res.data.data
    // 总条数为0或者未定义时
    if (info.records == 0 || !info.records) {
      that.setData({
        couponItem: false
      })
      return
    }
    // 计算出的总页码，Math.ceil向上取整
    var page_count = Math.ceil(info.records / page_num)

    // 页码数大于计算出的页码数和没有数据的情况
    if (page > page_count && !newData) {
      that.setData({
        nomore: true
      })
      return
    }
    // 接口成功的时候重新赋值
    if (res.data.ret == 200) {
      that.setData({
        page: page,
        page_count: page_count
      })
      // 加载
      if (func == 3) {
        var oldData = that.data.couponItem
        that.setData({
          couponItem: oldData.concat(newData)
        })
      } else {
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
        that.setData({
          couponItem: newData
        })
      }
    }

  },
  /**
   * 优惠券列表接口获取 失败
   */
  couponListFail: function() {
    var that = this;
    that.loadingBox.hideAniEnd()
    wx.showToast({
      title: '加载出错！',
      image: '/images/wrong-load.png'
    })
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  coupons: function() {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    PageUtil.getPage();
    var that = this;
    that.setData({
      page: 1,
      nomore: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var that = this;
    that.couponList(2, 1, that.data.page_num);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    var page = that.data.page + 1;
    that.couponList(3, page, that.data.page_num);
  },
  //赠卷 分享成功调用
  pcoupons_ad: function(e) {


  },
  //取消赠送
  delecoupon: function(e) {
    var p = this;
    var code = e.currentTarget.dataset.num;
    var cardid = app.storage.getAccount().card_id;
    console.log(app.storage.getUserInfo())
    wx.showModal({
      title: '取消赠送',
      content: '是否确定？',
      success: function(res) {
        if (res.confirm) {
          RequestUtil.commonRequest(
            ApiConst.FETCH_COUPON_DONATE_CANCEL, {
              id: code,
              cardId: cardid
            },
            (res) => {
              wx.showToast({
                title: '取消成功',
                icon: 'none'
              });
              p.couponList(2, p.data.page, p.data.page_num, 0);

            });
        }
      },
      complete: function(res) {
        p.setData({
          couponItem: p.data.couponItem
        })
      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(e) {
    var code = e.target.dataset.coucode;
    var phone = app.storage.getUserInfo().phone;
    if (e.from == 'button') {
      setTimeout(() => {
        RequestUtil.commonRequest(
          ApiConst.FETCH_COUPON_DONATE, {
            id: code,
            mobile: phone
          },
          (res) => {
            wx.showToast({
              title: res.data.data.msg,
              icon: 'none'
            })
          });
      }, 1500);
      var path = 'pages/coupon/shareindex/shareindex?couponCode=' + code + '&fromSource=donateReceiveCoupon';
      console.log("path:" + path + "   phone:" + phone);
      var obj = {
        title: '您有一张优惠券待领取！！！',
        imageUrl: '/images/liqu_bg@2x.png',
        path: path,
        success: function(res) {
          console.log("coupon success");
        },
        fail: function(res) {
          // 转发失败
          console.log('coupon fail')
        },
        complete: function() {
          console.log('coupon complete')

        }
      }
      return obj;
    } else {
      console.log("------1");
      return {};
    }
  },

  touchTap(el) {
    var that = this;
    that.loadingBox = that.selectComponent('#loading')
    that.loadingBox.showLoading()
    var i = el.currentTarget.dataset.seq;
    var actives = ['', '', '', '', ''];
    actives[i] = 'active';
    this.setData({
      actTabs: actives,
      accId: i,
    });
    console.log(i, "当前Tab-----ID", this.data.actTabs)
    // <!--优惠券状态：0未领用，1已领取（未使用），2已使用，3已过期 ,4转赠中-- >
    that.couponList(1, that.data.page, that.data.page_num, i);
  }
})

// var that = this;
// that.loadingBox = that.selectComponent('#loading')
// that.loadingBox.showLoading()
// that.loadingBox.hideAniEnd()