// pages/game/game.js
import {UserUtil} from "../../../utils/UserUtil";
import {createGameBean, getGameService} from "../../../module/game/game";
import {Toast} from "../../../utils/Toast";
import pageConfig from "../../../utils/page";
import {getTempValue} from "../../../utils/temputil";
import ValidateUtil from "../../../utils/ValidateUtil";
import {GameConfig} from "../../../api/config";

const mobile = UserUtil.getUserBean().getMobile();


var config = {

    /**
     * 页面的初始数据
     */
    data: {
        url: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        const name = options.name;
        const game = getTempValue(name);
        if (ValidateUtil.isEmpty(game)) {
            Toast.tip('游戏信息无效');
            return;
        }

        const mobile = UserUtil.getUserBean().getMobile();
        getGameService().fetchUserType({mobile: mobile}, (response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const gameUrl = GameConfig.getGameUrl(createGameBean(game), response.getData());
            console.log('gameUrl:' + gameUrl);
            this.setData({url: gameUrl});
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
}
var options = {...config, ...pageConfig};
Page(options);
