// pages/game/list/list.js
import pageConfig from "../../../utils/page";
import {getGameService} from "../../../module/game/game";
import {Toast} from "../../../utils/Toast";
import {setTempValue} from "../../../utils/temputil";
import PageUtil from "../../../utils/PageUtil";
import {UrlUtil} from "../../../utils/UrlUtil";

const config = {

    /**
     * 页面的初始数据
     */
    data: {
        gamelist: [],
        bgImage: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        const bgImage = UrlUtil.getRequestBaseUrl() + '/html/game/images/bggame.png';
        this.setData({bgImage: bgImage});
        console.log(bgImage);
        getGameService().fetchGameList((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const gameList = response.getData().getGameList();
            for (let i = 0; i < gameList.length; i++) {
                const obj = gameList[i];
                obj.image = UrlUtil.getRequestBaseUrl() + "/" + obj.image;
            }
            console.log(gameList);
            this.setData({gamelist: gameList});
        })
    },
    nav: function (e) {
        const index = e.currentTarget.dataset.index;
        const gamelist = this.data.gamelist;
        if (gamelist[index]) {
            const name = 'gamename';
            setTempValue(name, gamelist[index]);
            PageUtil.toPage('game', {name: name});
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
