// pages/web/web.js
import pageConfig from "../../utils/page";
import {Util} from "../../utils/common";
const config = {

    /**
     * 页面的初始数据
     */
    data: {
        url: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        const name=options.name;
        const obj=Util.getTempValue(name);
        const url = String(obj.url).replace(/&amp;/g, '&');
        console.log(url);
        const title = obj.title;
        this.setTitle(title);
        this.setData({url: url, title: title});
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
