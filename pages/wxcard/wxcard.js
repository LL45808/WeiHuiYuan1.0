// pages/test/test.js
import {UserUtil} from "../../utils/UserUtil";

Page({

    /**
     * 页面的初始数据
     */
    data: {
        url: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var params = options.params;
        var obj = JSON.parse(params);
        var cardId = obj.cardId;
        var uniacid = obj.uniacid;
        // var url = 'https://wx3.bzqch.cn/html/active.html?uniacid=' + uniacid + "&cardId=" + cardId;
        var url = UserUtil.getWxCardWebUrl(uniacid, cardId);
        console.log(url);
        this.setData({url: url});
    },
    receiveMessage: function (e) {
        console.log("bindmessage", e);
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
