var app = getApp()
Page({
    /**
     * 页面的初始数据
     */
    data: {
        money: '',
        reward: '',//奖励金额
        actualMoney: 0,//实际到账
        itemShow: [],
        itemGreen: [],
        rechargebalance: '',
        rechargeRadio: [],//充值列表
        oldIndex: undefined,
        currentidx: undefined,
        inputAvailable: false,
        rid: 0,
    },
    /**
     * 充值规则判断接口获取
     */
    giveMoney: function (money) {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            money: money
        }
        app.crmApi(
            app.apiService.crm.getGiveMoney,
            data,
            that.giveMoneySuccess,
            that.giveMoneyFail
        )
    },
    /**
     * 充值规则判断接口获取  成功
     */
    giveMoneySuccess: function (res) {
        var that = this;
        if (res.data.ret = 200) {
            var actualMoney = parseFloat(that.data.money) + parseFloat(res.data.data.reward_money)
            that.setData({
                actualMoney: parseFloat(actualMoney).toFixed(2)
            })
        }
    },

    /**
     * 充值规则显示接口获取
     */
    rechargeRule: function (money) {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            money: money
        }
        app.crmApi(
            app.apiService.crm.getRechargeRule,
            data,
            that.rechargeRuleSuccess,
            that.rechargeRuleFail
        )
    },
    /**
     * 充值规则显示接口获取  成功
     */
    rechargeRuleSuccess: function (res) {
        var that = this;
        if (res.data.ret = 200) {
            that.makePayItem(res.data.data)
        }
    },

    /**
     * 充值输入事件 （等于100.500.1000时选择金额）
     */
    valueTap: function (e) {
        console.log("-------------1");
        var that = this
        var value = e.detail.value
        var txtValue = value + ''
        if (isNaN(txtValue)) {
            that.setData({
                money: txtValue.substr(0, txtValue.length - 1)
            })
            return
        }
        if (value !== 100 && value !== 500 && value !== 1000) {
            that.choose_cancel(e)
        }
        if (value == 100) {
            that.choose_select(0)
        }
        if (value == 500) {
            that.choose_select(1)
        }
        if (value == 1000) {
            that.choose_select(2)
        }
        that.setData({
            money: value
        })
        that.giveMoney(value)
    },
    /**
     * 单选处理（ 输入充值金额不等于100.500.1000 时取消当前状态）
     */
    choose_cancel: function (currentidx) {
        var that = this
        var tmpArr = that.data.itemShow
        tmpArr[currentidx] = 'show'
        var tmpColor = that.data.itemGreen
        tmpColor[currentidx] = 'choosedbtn'
        var oldidx = that.data.oldIndex
        if (oldidx == currentidx) {
            //如果上一个被点击的元素和当前被点击的元素是同一个，则取消选择
            tmpArr[currentidx] = 'hide'
            tmpColor[currentidx] = 'choosenobtn'
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor,
            });
            that.setData({oldIndex: ''})
        }
        else {
            tmpArr[oldidx] = 'hide'
            tmpColor[oldidx] = 'choosenobtn'
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor
            })
            that.setData({oldIndex: currentidx})
        }
    },
    /**
     * 余额的单选处理
     */
    choose_select: function (currentidx) {
        var that = this
        var tmpArr = that.data.itemShow
        tmpArr[currentidx] = 'show'
        var tmpColor = that.data.itemGreen
        tmpColor[currentidx] = 'choosedbtn'
        var oldidx = that.data.oldIndex
        //先判断是否存在上一个被选择的元素(没有一个被选中的状态和单选)
        if (!oldidx && oldidx != 0) {
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor
            })
            that.setData({oldIndex: currentidx})
        }
        else if (oldidx == currentidx) {
            //如果上一个被点击的元素和当前被点击的元素是同一个，则取消选择(取消本身的选择状态)
            tmpArr[currentidx] = 'hide'
            tmpColor[currentidx] = 'choosenobtn'
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor,
            });
            that.setData({oldIndex: ''})
        } else {
            tmpArr[oldidx] = 'hide'
            tmpColor[oldidx] = 'choosenobtn'
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor
            })
            that.setData({oldIndex: currentidx})
        }
    },
    /**
     * 选择金额显示到输入框
     */
    chooseItem: function (e) {
        var that = this
        //记录当前点击的对象的序号
        var value = parseFloat(e.currentTarget.dataset.value)
        var rule_id = e.currentTarget.dataset.id
        that.setData({
            money: value,
            rule_id: rule_id
        })
        var currentidx = e.currentTarget.dataset.idx
        var oldidx = that.data.oldIndex
        var reward = e.currentTarget.dataset.reward ? parseFloat(e.currentTarget.dataset.reward) : 0
        if (currentidx == oldidx) {
            that.setData({money: ''})
            that.choose_cancel(currentidx)
            that.setData({
                actualMoney: 0
            })
        } else {
            that.choose_select(currentidx)
            that.setData({
                actualMoney: (parseFloat(value) + parseFloat(reward)).toFixed(2)
            })
        }
    },
    /**
     * 充值协议
     */
    agreementTap: function () {
        wx.navigateTo({
            url: '/pages/balance/agreement/agreement',
        })
    },
    /**
     * 余额充值接口获取
     */
    recharge: function (money, rid) {

        var that = this

        var device_info

        if (!that.data.actualMoney) {

            that.loadingWave.hideAniEnd()

            that.dialog.showDialog({

                'title': '出错了！',

                'content': '请选择充值金额～',

                'type': 'warning'

            })

        }

        try {

            device_info = wx.getSystemInfoSync()

        } catch (e) {

            device_info = 'not found'

        }

        var data = {

            token: app.storage.getAuth().token,

            money: money,

            device_info: device_info.model,

            rule_id: rid

        }

        this.loadingWave.showLoading()

        app.crmApi(
            app.apiService.crm.recharge,
            data,
            that.rechargeSuccess,
            that.rechargeFail
        )

    },
    /**
     * 余额充值接口获取 成功
     */
    rechargeSuccess: function (res) {
        var that = this;
        that.loadingWave.hideAniEnd()
        if (res.data.ret == 200) {
            var info = res.data.data
            wx.requestPayment({
                'timeStamp': info.timeStamp.toString(),
                'nonceStr': info.nonceStr,
                'package': info.package,
                'signType': info.signType,
                'paySign': info.paySign,
                'success': function (res_two) {
                    var acct = app.storage.getAccount();
                    acct.balance = (parseFloat(acct.balance) + parseFloat(that.data.actualMoney)).toFixed(2)
                    app.storage.setAccount(acct)
                    wx.navigateBack({
                        delta: 1
                    });
                },
                'fail': function (res_two) {
                }
            })
        }
    },
    /**
     * 余额充值接口获取 失败
     */
    rechargeFail: function (res) {
    },
    /**
     * 可选金额接口获取
     */
    rechargeValue: function () {
        var that = this;
        var data = {
            token: app.storage.getAuth().token
        }
        app.crmApi(
            app.apiService.crm.recargeValues,
            data,
            that.rechargeValueSuccess,
            that.rechargeValueFail
        )
    },
    /**
     * 可选金额接口获取 成功
     */
    rechargeValueSuccess: function (res) {
        var that = this;
        if (res.data.ret = 200) {
            that.setData({
                rechargeMoney: res.data.data
            })
            var money = res.data.data
            var tmpArr = []
            var tmpColor = []
            for (var i = 0; i < money.length; i++) {
                tmpArr.push('hide')
                tmpColor.push('choosenobtn');
            }
            that.setData({
                itemShow: tmpArr,
                itemGreen: tmpColor
            })
        }

    },
    /**
     * 可选金额接口获取 失败
     */
    rechargeValueFail: function (res) {
    },
    makePayItem(rules) {
        var items = []
        var that = this
        rules.forEach(rule => {
            var item = {
                value: rule.money,
                name: rule.name,
                id: rule.id
            }
            if (rule.reward_type == 1) {
                item.reward = rule.reward_money
            } else if (rule.reward_type == 2) {
                item.reward = rule.money * (rule.reward_percentage / 100)
            } else {
                item.reward = 0
            }
            items.push(item)
        })
        that.setData({
            rechargeMoney: items
        })
        var money = items
        var tmpArr = []
        var tmpColor = []
        for (var i = 0; i < money.length; i++) {
            tmpArr.push('hide')
            tmpColor.push('choosenobtn');
        }
        that.setData({
            itemShow: tmpArr,
            itemGreen: tmpColor
        })
    },
    /**
     * 支付提交
     */
    paySubmit: function (e) {

        var that = this

        that.recharge(e.detail.value.money, that.data.rule_id);

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.loadingWave = this.selectComponent('#wave')
        this.dialog = this.selectComponent('#dialog');
        this.rechargeRule();

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
