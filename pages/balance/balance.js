var app = getApp()
var util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

    accountBalance: '',//余额

    balanceItem: [],//余额列表

    norecord: false,//暂无记录

    page: 1, //下一个页码

    page_count: 0, //总页码数

    page_num: 8, //每页显示条数

    nomore: false,//已加载所有

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.loadingWave = this.selectComponent('#wave')

  },

  /**
   * 余额列表跳转到详情
   */
  detailTap: function (e) {
    var that = this
    var list = that.data.balanceItem//所有的列表
    var idx = e.currentTarget.dataset.idx//当前索引
    var data = JSON.stringify(list[idx]) //当前的内容
    wx.navigateTo({
      url: '/pages/balance/detail/detail?data=' + data
    })
  },
  /**
   * 余额列表接口获取
   */
  balanceList: function (func, page, page_num) {
    var that = this;
    var data = {
      token: app.storage.getAuth().token,
      page: page,
      page_num: page_num
    }
    var extraParams = {
      func: func, page: page, page_num: page_num
    }
    if (func == 3 || func == 1) {

    }
    app.crmApi(
      app.apiService.crm.getBalanceList,
      data,
      that.balanceListSuccess,
      that.balanceListFail,
      extraParams
    )

  },

  /** 
   * 余额列表接口获取 成功
   */
  balanceListSuccess: function (res, param) {
    var that = this;
    var func = param.func;
    var page = param.page;
    var page_num = param.page_num;
    var info = res.data.data
    wx.hideToast();

    var list = res.data.data.data;
    if (!Array.isArray(list)) {
      return
    }
    // 总条数为0或者未定义时
    if (info.records == 0 || !info.records) {
      that.setData({
        balanceItem: false,
      })
      return
    }
    // 计算出的总页码，Math.ceil向上取整
    var page_count = Math.ceil(info.records / page_num)
    var newData = res.data.data.data
    // 页码数大于计算出的页码数和没有数据的情况
    if (!newData || page > page_count) {
      that.setData({
        nomore: true
      })
      return
    }
    // 接口成功的时候重新赋值
    if (res.data.ret = 200) {
      newData.map( it => {  
        it.total = (it.amount + it.reward).toFixed(2)  
      })
      that.setData({
        page: page,
        page_count: page_count
      })
      // 加载
      if (func == 3) {
        var oldData = that.data.balanceItem
        that.setData({
          balanceItem: oldData.concat(newData)
        })
      } else {
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
        that.setData({
          balanceItem: newData
        })
      }
    }
  },
  /**
   * 余额列表接口获取 失败
   */
  balanceListFail: function () {
    var that = this;
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    this.balanceList(1, 1, this.data.page_num);

    this.setData({

      accountBalance: app.storage.getAccount().balance

    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.balanceList(2, 1, that.data.page_num);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var page = that.data.page + 1;
    that.balanceList(3, page, that.data.page_num);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
  
})