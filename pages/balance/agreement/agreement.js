var app = getApp()
const page_code = 'wxpro_crm_recharge_rules';
Page({
    /**
     * 页面的初始数据
     */
    data: {
        page_configs: {
            recharge_rules: ''
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.page_config()
    },
    /**
     * 充值协议接口获取
     */
    page_config: function () {
        var that = this;
        app.crmApi(
            app.apiService.crm.pageConfig,
            { page_code: page_code },
            that.pageConfigSuccess,
        )
    },
    pageConfigSuccess: function (res) {
        var that = this
        const config = res.data.data[page_code].configs
        var tmpConfig = that.data.page_configs
        tmpConfig.recharge_rules = app.convertHtmlToText(config.recharge_rules.value_text)
        that.setData({
          page_configs: tmpConfig
        })
    },


   
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})