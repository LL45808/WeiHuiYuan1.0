var app = getApp()
const page_code = 'wxpro_crm_exchange_rules';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        exchange_addr: false,
        addrList: [],
        address_id: 0,
        code: '',
        page_configs: {
            exchange_rules: ''
        },
        is_switch: true,//开关
    },
    getBackData: function (e) {
        var page = this;
        page.setData({
            addr: e,
            address_id: e.address_id,
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        if (options.data) {
            //立即配送
            var data = JSON.parse(options.data)
            that.setData({
                code: data
            })
        }
        //配送地址
        that.default_addr();
        that.page_config()
    },
    /**
     * 充值协议接口获取
     */
    page_config: function () {
        var that = this;
        app.crmApi(
            app.apiService.crm.pageConfig,
            { page_code: page_code },
            that.pageConfigSuccess,
        )
    },
    pageConfigSuccess: function (res) {
      var that = this
      const config = res.data.data[page_code].configs;
      var tmpConfig = that.data.page_configs
      tmpConfig.exchange_rules = config.exchange_rules.value_type == 2 ? app.convertHtmlToText(config.exchange_rules.value_text) : config.exchange_rules.value;
      that.setData({
        page_configs: tmpConfig
      })
    },

    /**
     * 兑换按钮
     */
    exchangeSubmit: function (e) {
        var info = e.detail.value;
        var that = this;
        if (!that.data.is_switch) {
            return
        }
        else {
            that.TLexchange(info);
        }
    },

    /**
     * 提领券兑换接口获取
     */
    TLexchange: function (e) {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            address_id: e.address_id,
            id_code: e.id_code,
            code: e.code,
            dis_type: 0
        }
        app.crmApi(
            app.apiService.crm.getTLexchange,
            data,
            that.TLexchangeSuccess,
            that.TLexchangeFail
        )
    },

    /**
     * 提领券兑换接口获取  成功
     */
    TLexchangeSuccess: function (res) {
        var that = this;
        if (res.data.ret == 200) {
            wx.showToast({
                title: '兑换成功',
            })
        }
        that.setData({
            is_switch: false
        })
    },
    TLexchangeFail: function (res) {
        var that = this;
        that.setData({
            is_switch: true
        })
    },
    /**
     * 默认地址接口获取
     */
    default_addr: function () {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            default: true
        }
        app.crmApi(
            app.apiService.crm.getAddressDetail,
            data,
            that.default_addrSuccess,
            that.default_addrFail
        )
    },

    /**
     * 默认地址接口获取 成功
     */
    default_addrSuccess: function (res) {
        var that = this;
        if (res.data.ret = 200) {
            that.setData({
                addr: res.data.data,
                address_id: res.data.data.address_id,
            })
        }
    },

    /**
     * 默认地址接口获取 失败
     */
    default_addrFail: function (res) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})