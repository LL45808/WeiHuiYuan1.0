var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        TLcoupondetail:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var page = this;
        var detail_data = JSON.parse(options.data)
        page.setData({
            TLcoupondetail: detail_data
        })   
    },
    
    /**
     * 二维码链接
     */
    ewmTap: function (e) {
        var that = this
        var list = that.data.TLcoupondetail//addrList中的值
        var data = JSON.stringify(list)  //JSON.stringify()从一个对象中解析出字符串 
        wx.navigateTo({
            url: '/pages/TLcoupon/qrcode/qrcode?data=' + data 
        })
    },

    /**
     * 立即配送链接
     */
    psTap: function (e) {
        var that = this
        var list = that.data.TLcoupondetail//addrList中的值
        var data = JSON.stringify(list)  //JSON.stringify()从一个对象中解析出字符串 
        wx.navigateTo({
            url: '/pages/TLcoupon/exchange/exchange?data=' + data
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})