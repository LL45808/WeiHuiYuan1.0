// pages/savetea/orderlist/orderlist.js
import {
    UserConfigUtil,
    UserUtil
} from "../../../utils/UserUtil";
import {
    getReserveService
} from "../../../module/reserve/reserve";
import {
    getUserService
} from "../../../module/user/user";
import {
    Toast
} from "../../../utils/Toast";
import {ToolUtil} from "../../../utils/ToolUtil";
import pageConfig from "../../../utils/page";
import {Util} from "../../../utils/common";

var payjs = require('../saveutils.js');

const config = {

    /**
     * 页面的初始数据
     */
    data: {
        pageIns: null,
        pageSize: 7,
        datalist: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);
        // console.log(options);
        let p = this;
        //p.loading = p.selectComponent("#loading");
        //p.loading.showLoading();
        //p.loading.hideLoading();
        p.getdata(options.id);
    },

    getdata(shopId) {
        let p = this;
        const cardId = UserUtil.getAccountBean().getCardId();
        getReserveService().fetchShopOrderList({
            cardId: cardId
        }, (response) => {
            //console.log("meiyou", response.getData())
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const orderList = response.getData();
            if (orderList.length == 0) {
                this.setData({
                    nolist: true
                });
                return;
            }
            const pageIns = Util.createPaging(orderList, this.data.pageSize);
            pageIns.setRender((isEnd, list) => {
                console.log("isEnd:", isEnd);
                if (isEnd) {
                    this.setData({
                        nodata: true
                    })
                    return;
                }
                const newList = this.data.datalist;
                for (let i = 0; i < list.length; i++) {
                    const obj = list[i];
                    obj.hasLoad = false;
                    newList.push(obj);
                }

                const taskIns = ToolUtil.initAsync();

                this.loading();
                for (let i = 0; i < list.length; i++) {
                    const obj = list[i];
                    obj.index = i;
                    taskIns.then((complete, params) => {
                        if (params.hasLoad) {
                            complete();
                            return;
                        }
                        this.getgoods(obj, (respon, orderObj) => {
                            const list = this.data.datalist;
                            orderObj.tolist = respon;
                            orderObj.tolistconut = respon.length;
                            orderObj.hasLoad = true;
                            list[orderObj.index] = orderObj;
                            console.log("console.log", list)
                            this.setData({datalist: list});
                            complete();
                        });
                    }, obj);
                }
                taskIns.then(() => {
                    console.log('end----------');
                    this.closeLoading();
                })
                taskIns.execute();
            });
            console.log("订单内容", pageIns);
            this.setData({pageIns: pageIns, nodata: false});
            pageIns.execute();
        });

    },
    //取第一个商品
    getgoods(orderObj, back) {
        //console.log("orderId:",orderObj)
        getReserveService().fetchShopOrderGoodsList({
            orderId: orderObj.orderId
        }, (response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            let pamrs = {
                thumbnail: 1,
                width: 150,
                height: 150
            }
            payjs.commonimage(pamrs, response.getData(), (ress) => {
              
              // Util.retainPoint 单价
              for(let kt in ress){
                ress[kt].price = Util.retainPoint(ress[kt].amount/ress[kt].number,2)
              }
              console.log('commonimage', ress)
                back(ress, orderObj)
            })
        });
    },

    orderdetail: function (e) {
        let p = this;
        let isde = e.currentTarget.dataset.index;
        let toData = p.data.datalist[isde];
        wx.navigateTo({
            url: 'orderdetail?orderId=' + toData.orderId + '&shopdata=' + JSON.stringify(toData)
        })
        //console.log(isde, toData)
    },

    binpay(e) {
      this.loading();
        let p = this;
        let id = e.currentTarget.dataset.orderid;
        payjs.pay(id, (res) => {
          this.closeLoading();
            Toast.tip(res);
            // p.loading.showLoading();
            p.data.datalist=[];
            p.getdata(id);
            // console.log('支付data',res)
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.data.pageIns.execute();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
};

var options = {...config, ...pageConfig};
Page(options);
