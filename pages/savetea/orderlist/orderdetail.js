// pages/savetea/orderlist/orderdetail.js
import {
  UserConfigUtil,
  UserUtil
} from "../../../utils/UserUtil";
import {
  getReserveService
} from "../../../module/reserve/reserve";
import {
  getUserService
} from "../../../module/user/user";
import {
  Toast
} from "../../../utils/Toast";
var payjs = require('../saveutils.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    summoney:0,
    orderid:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let p=this;
      p.loading = p.selectComponent("#loading");
      p.loading.showLoading();
      //p.loading.hideLoading();
    let orderid= options.orderId;
    let shopdata= JSON.parse(options.shopdata) 
    console.log("shopdata", shopdata)  
    p.setData({
      shopdata: shopdata
    })
    p.data.orderid=orderid;
    p.getdata(orderid)
  },
  getdata(orderid){
    let p = this;
    let sum=0;
    getReserveService().fetchShopOrderGoodsList({
      orderId: orderid
    }, (response) => {
      //p.loading.hideLoading();
      for (const key in response.getData()){
        sum = sum + response.getData()[key].amount;
      }
      if (!response.isSuccess()) {
        Toast.tip(response.getMsg());
        return;
      }
        let pamrs={
            thumbnail:1,
            width:150,
            height:150
        }
        payjs.commonimage(pamrs,response.getData(),(ress)=>{
            p.loading.hideLoading();
            p.setData({
                datalist:ress,
                summoney:sum
            })
            console.log('commonimage',ress)
        })

    });
  },
  binpay(){
    let p=this;
      this.loading.showLoading();
    payjs.pay(p.data.orderid,(res)=>{
      console.log('支付data',res)
        this.loading.hideLoading();
        Toast.tip(res);
        p.loading.showLoading();
        //p.loading.hideLoading();
        p.getdata(p.data.orderid);
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})