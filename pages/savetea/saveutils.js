import {
    UserConfigUtil,
    UserUtil
} from "../../utils/UserUtil";
import {
    getReserveService
} from "../../module/reserve/reserve";
import {
    getUserService
} from "../../module/user/user";
import {
    Toast
} from "../../utils/Toast";

function pay(orderid, back) {
    const mobile = UserUtil.getUserBean().getMobile();
    console.log('支付手机号', mobile)
    getReserveService().fetchShopOrderPay({
        mobile: mobile,
        orderId: orderid
    }, (response) => {
        //console.log(response.getMsg(),'支付信息')
        // if (!response.isSuccess()) {
        //   Toast.tip(response.getMsg());
        //   return;
        // }
        // console.log(response.getData());
        back(response.getMsg())
    });
}


function getimages(data, back) {
    // goodsSn:商品编码,多个使用逗号分隔,
    //     thumbnail:[1|0] 1=>缩略图,0不是缩略图
    // width:缩略图宽,
    //     height:缩略图高
    getReserveService().fetchGoodsImage({
        goodsSn: data.goodsSn,
        thumbnail: data.thumbnail,
        width: data.width,
        height: data.height
    }, (response) => {
        if (!response.isSuccess()) {
            Toast.tip(response.getMsg());
            return;
        }
        //console.log(response.getData(), response.getMsg());
        back(response.getData())
    });
}

function commonimage(parms, data, backdata) {
    var thum = '';
    for (const key in data) {
        // thum.push(data[key].goodsSn) // thum.push(data[key].goodsSn)
        thum = thum + ',' + data[key].goodsSn
        // data[key].image= data[key].image
    }
    parms.goodsSn = thum;
    //console.log('key',parms)
    this.getimages(parms, (res) => {
        //console.log('getimage',res)
        for (const keys in data) {
            data[keys].image = res[keys].image
        }
        backdata(data)
    })
}

module.exports = {
    pay: pay,
    getimages: getimages,
    commonimage: commonimage
}