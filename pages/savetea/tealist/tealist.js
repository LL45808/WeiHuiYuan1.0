// pages/savetea/tealist/tealist.js
import {
    UserConfigUtil,
    UserUtil
} from "../../../utils/UserUtil";
import {
    getReserveService
} from "../../../module/reserve/reserve";
import {
    getUserService
} from "../../../module/user/user";
import {
    Toast
} from "../../../utils/Toast";
import CommonUtil from "../../../utils/lon_lat";

Page({

    /**
     * 页面的初始数据
     */
    data: {
        shoplist: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // const userInfo = UserUtil.getUserInfo();
        // const account = UserUtil.getAccount();
        // console.log(userInfo, "userinfo", account)

        let p = this;
        p.loading = p.selectComponent("#loading");
        p.loading.showLoading();
        const vipId = UserUtil.getAccountBean().getVipId();
        getReserveService().fetchShopList({
            vipId: vipId
        }, (response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            console.log(response.getData());
            var lists = p.handleDistance(response.getData(), (res) => {
                console.log(response.getData(), res);
                p.loading.hideLoading();
                p.setData({
                    shoplist: res,
                })
            })
        });

    },
    navig: function (e) {
        console.log(e)
        let data = this.data.shoplist[e.currentTarget.dataset.id];
        JSON.stringify(data)
        console.log(JSON.stringify(data))
        wx.navigateTo({
            url: '../storetea/storetea?shop=' + JSON.stringify(data)
        })
    },
    //处理距离
    handleDistance: function (data, data1) {
        let p = this;
        wx.getLocation({
            type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
            success: function (res) {
                console.log('定位成功！', res)
                let spot1 = {
                    lat: res.latitude,
                    lng: res.longitude
                }
                for (var key in data) {
                    let spot2 = {
                        lat: data[key].lat,
                        lng: data[key].lng
                    }
                    //console.log('距离点', spot1, spot2, CommonUtil )
                    var locinto = CommonUtil.getDistance(spot1, spot2);
                    console.log('距离', locinto)
                    data[key].distance = locinto;
                }
                data1(data);
            },
            fail: function (e) {
                console.log("fail", e)
            }
        })

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})