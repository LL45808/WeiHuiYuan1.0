// pages/savetea/storetea/storetea.js
import {
    UserConfigUtil,
    UserUtil
} from "../../../utils/UserUtil";
import {
    getReserveService
} from "../../../module/reserve/reserve";
import {
    getUserService
} from "../../../module/user/user";
import {
    Toast
} from "../../../utils/Toast";

var payjs = require('../saveutils.js');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        datalist: [],//商品
        shopData: [],//店铺

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let p = this;
        p.loading = p.selectComponent("#loading");
        p.loading.showLoading();
        const shopData = JSON.parse(options.shop)
        console.log(shopData)
        p.getdata(shopData.shopId);
        p.setData({
            shopData: shopData
        })
    },
    getdata(shopId) {
        let p = this;
        const mobile = UserUtil.getUserBean().getMobile();
        const vipId = UserUtil.getAccountBean().getVipId();
        
        getReserveService().fetchShopGoodsList({
            shopId: shopId,
            vipId: vipId
        }, (response) => {
            p.loading.hideLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            let pamrs = {
                thumbnail: 1,
                width: 100,
                height: 100
            }
            payjs.commonimage(pamrs, response.getData(), (ress) => {
                p.setData({
                    datalist: ress
                })
                console.log('commonimage',)
            })

            // console.log(response.getData());
        });
    },


    coll: function () {
        let p = this;
        let phone = p.data.shopData.mobile;
        if (phone == '') {
            Toast.tip('手机号格式错误');
            return;
        } else {
            wx.makePhoneCall({
                phoneNumber: phone // 仅为示例，并非真实的电话号码
            })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})