import pageConfig from "../../../utils/page";
import {getFetchUserInfoConfig} from "../../../module/config/LoadUserConfig";

var app = getApp()
const page_code = 'wxpro_crm_member_qrcode'
var url = require("../../../api/url.js");

var config = {

    /**
     * 页面的初始数据
     */
    data: {
        barcode: '',
        card_id: "******************",
        page_configs: {
            head_logo: ''
        },
        qrcode: '',
        valid_time: 300,//有限时间
        created_at: 0, //创建时间
        brightness: 0
    },
    userInfoReady: function () {
        var that = this;
        var account = app.storage.getAccount()
        that.setData({
            barcode: account.barcode,
            qrcode: account.qrcode
        });

        wx.getScreenBrightness({
            success: value => {
                that.data.brightness = value.value
            }
        })
        that.getUpdate();//调用更新接口二维码
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);
        const configIns = getFetchUserInfoConfig().setErpFetchCache(true).setOnlineFetchCache(true);
        this.initSessionAndUserInfo(configIns.getConfig());
    },

    /**
     * 时间处理
     */
    updateCard: function () {

        var that = this;

        var maxTime = that.data.valid_time;

        var time = that.data.created_at;

        var timestamp = Math.round(Date.parse(new Date()) / 1000);//时间戳取整

        var minus = timestamp - time;

        if (minus < 0) {

            minus = 0;

        }

        var iCard = setInterval(function () {

            minus++;

            if (minus > maxTime) {

                that.getUpdate();//调用更新接口二维码

                clearInterval(iCard);

            }

        }, 1000)

    },

    /**
     * 二维码更新接口
     */
    getUpdate: function () {
        var that = this;
        var data = {
            token: app.storage.getAuth().token
        }
        app.crmApi(
            app.apiService.crm.getQRcode,
            data,
            that.getUpdateSuccess
        )
    },
    getUpdateSuccess: function (res) {
        var that = this;
        if (res.data.ret == 200) {
            var info = res.data.data;
            that.setData({
                card_id: info.pay_code.substr(0, 4) + ' ' + info.pay_code.substr(4, 4) + ' ' + info.pay_code.substr(8, 4) + ' ' + info.pay_code.substr(12, 4) + ' ' + info.pay_code.substr(16, 2),
                valid_time: info.valid_time,
                created_at: info.created_at,
                qrcode: info.card_id_qr_code,
                barcode: info.bar_code
            })

            var account = app.storage.getAccount()

            account.qrcode = info.card_id_qr_code

            account.barcode = info.bar_code

            app.storage.setAccount(account)

            that.updateCard()

        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

        this.page_config();

    },

    /**
     * 可配置项（logo）
     */
    page_config: function () {
        var that = this;
        var tmpConfig = that.data.page_configs
        var head_logo = app.storage.getShowConfig().head_logo
        if (head_logo) {
            tmpConfig.head_logo = head_logo
            that.setDate({
                page_configs: tmpConfig
            })
            return
        }
        app.crmApi(
            app.apiService.crm.pageConfig,
            {page_code: page_code},
            that.pageConfigSuccess,
        )
    },
    pageConfigSuccess: function (res) {
        var that = this
        const config = res.data.data[page_code].configs
        var tmpConfig = that.data.page_configs
        for (let k in tmpConfig) {
            tmpConfig[k] = config[k].value_type == 2 ? config[k].value_text : config[k].value
        }
        var showConfig = app.storage.getShowConfig()
        showConfig.head_logo = tmpConfig.head_logo
        app.storage.setShowConfig(showConfig)
        that.setData({
            page_configs: tmpConfig
        })
    },


    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        var that = this
        wx.setScreenBrightness({
            value: that.data.brightness,
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    lightUp: function () {
        var that = this
        var value = 0
        if (that.data.brightness >= 1) {
            that.data.brightness = 0
            value = 0
        } else {
            that.data.brightness += 0.1
            value = that.data.brightness
        }
        wx.setScreenBrightness({
            value: value
        })
    }

};
var options = {...config, ...pageConfig};
Page(options);

