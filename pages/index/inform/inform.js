var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    inform: [],

    page: 1,

    page_size: 12,

    isConnected: true, //网络状态

    informItem: false

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.loadingWave = this.selectComponent('#wave')

    this.informList(1, 1, this.data.page_size);

  },
  /**
   * 消息通知列表接口获取
   */
  informList: function (func, page, page_size) {

    var that = this;

    that.loadingWave.showLoading()

    var data = {

      token: app.storage.getAuth().token,

      page: page,

      page_size: page_size

    }

    var extraParams = {

      page: page, page_size: page_size, func: func

    }

    app.crmApi(

      app.apiService.crm.getNoticeList,

      data,

      that.informListSuccess,

      that.informListFail,

      extraParams

    )

  },

  /**
  * 消息通知列表接口获取 成功
  */
  informListSuccess: function (res, param) {

    var that = this

    if (res.data.data.page > 1 && res.data.data.page == this.data.page) {

      that.setData({

        nomore: true

      })

      return

    }

    var page = param.page;

    var page_size = param.page_size;

    var func = param.func;

    var info = res.data.data

    this.loadingWave.hideAniEnd()

    wx.stopPullDownRefresh()

    // 总条数为0或者未定义时
    if (!Array.isArray(info.list) && info.list.length == 0) {

      if (func == 3) {

        that.setData({

          nomore: true
        
        })

      }

    } else {

      if (func == 3) {

        that.setData({

          page: page,

          informItem: that.data.informItem.concat(info.list)

        })

      } else {

        that.setData({

          page: res.data.data.page,

          informItem: info.list

        })

      }

    }

  },
  /**
   * 消息通知列表接口获取 失败
   */
  informListFail: function (res, param) {

    var that = this;

    that.loadingWave.hideAniEnd()

    wx.stopPullDownRefresh()

    wx.hideNavigationBarLoading() //完成停止加载

    wx.stopPullDownRefresh() //停止下拉刷新

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    var that = this;

    that.informList(2, 1, that.data.page_size);

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    var that = this;

    var page = that.data.page + 1

    that.informList(3, page, that.data.page_size);

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

})