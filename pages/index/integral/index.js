//获取应用实例
import {getConfigService} from "../../../module/common/config";
import {Toast} from "../../../utils/Toast";
import ValidateUtil from "../../../utils/ValidateUtil";

var app = getApp()
Page({
    data: {
        urls: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        var that = this;

        getConfigService().fetchIndexNav((response) => {
            if (!response.isSuccess()) {
                Toast.tip("内容加载失败");
                return;
            }
            var list = response.getData().getNavList();
            var index = options.index;
            var indexs = options.indexs;
            var url = list[index][indexs]['url'];
            if (list[index]) {
                if (!ValidateUtil.isEmpty(url)) {
                    that.setData({
                        urls: url
                    });
                }
            } else {
                Toast.tip("积分商城加载失败");
            }
        });

        /**
         * 获取加载插件实例
         */
        that.loadingBox = that.selectComponent('#loading')
        that.loadingBox.showLoading()

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    }

})
