//获取应用实例
import ValidateUtil from "../../utils/ValidateUtil";
import {UserConfigUtil, UserUtil} from "../../utils/UserUtil";
import WxUtil from "../../utils/WxUtil";
import PageUtil, {PageRedirectUtil} from "../../utils/PageUtil";
import {getUserService} from "../../module/user/user";
import {getCardTypeService} from "../../module/system/cardtype";
import {getSessionBean, getSessionService} from "../../module/common/session";
import {Toast} from "../../utils/Toast";
import {getConfigService} from "../../module/common/config";
import {getWxAuthService} from "../../module/common/wxauth";
import pageConfig from "../../utils/page";
import {getFetchUserInfoConfig} from "../../module/config/LoadUserConfig";
import {getOpenidService} from "../../module/common/openid";
import {Util} from "../../utils/common";

var app = getApp()
var config = {
    data: {
        frequency: 0,
        title: '',
        animationData: {},
        animationData2: {},
        indicatorDots: true,
        autoplay: false,
        userInfo: {},
        navService: [],
        level: [],
        inform: [],
        page: 1,
        page_num: 5,
        norecord: false,
        navload: true,
        balanceAvailable: true,
        accountInfo: {
            balance: '--',
            intergral: '--',
            coupons: '--',
            card_id: '',
            barcode: '',
            card_type: '',
            qrcode: ''
        },
        notice: false,
        login: false, // 登录判断
        accredit: false, // 登录授权
        pageConfig: [], // 首页配置
        lat: '',
        lng: '',
        wxUserCardData: null,
        showCardType: {
            showCardTime: false,
            startDate: '',
            endDate: ''
        },
        registerByWxCard: false,
        wxCardRegister: {
            extraData: {}
        },
        options: {},
        supportList: {
            supportCoupon: true,
            supportIntegral: true,
            supportBalance: true
        },
        enableQueryBindWechat: false//第一次不能再onshow里面查询是否绑定了微信公众号，因为没有获取到小程序openid
    },
    toWeb(event){
        const data=event.currentTarget.dataset;
        const title=data.title;
        const url=data.url;
        const name='indexToWeb';
        const obj={title:title,url:url};
        Util.setTempValue(name,obj);
        PageUtil.toPage('web',{name:name});
    },  
    initWxCard() {
        const wxUserCardData = WxUtil.getWxCardInfo();
        console.log("wxUserCardData", wxUserCardData);
        if (wxUserCardData.valid) {
            // console.log("wxUserCardData", wxUserCardData);
            this.setData({wxUserCardData: wxUserCardData});
        }
    },
    initCardTime() {
        console.log("initCardTime");
        getCardTypeService().fetchCardTypeConfig((response) => {
            if (response.isSuccess()) {
                const configIns = response.getData();
                const bean = UserUtil.getAccountBean();
                const cardType = {
                    showCardTime: configIns.isShowCardTypeValidTime(),
                    startDate: bean.getCardTypeStartDate(),
                    endDate: bean.getCardTypeEndDate()
                }
                this.setData({
                    showCardType: cardType
                });
            }
        });
    },
    initIndexData: function (options, userComplete) {
        // this.commonLoad();
        this.initUserInfo();
        const sessionBean = getSessionBean();
        var title = sessionBean.getTitle();
        this.setData({
            title: title
        });
        //this.informList();

    },
    initUserInfo: function () {
        //240002
        // this.loading();
        /**
         * 加载用户基本数据，先从缓存读取
         */
        const account = UserUtil.getAccount();
        if (account) {
            this.setData({
                accountInfo: account
            })
        }
        const userInfo = UserUtil.getUserInfo();
        if (userInfo) {
            this.setData({
                userInfo: userInfo
            });
        }
        const configIns = getFetchUserInfoConfig().setErpFetchCache(false);
        configIns.setOnlineCallback(() => {
            const userInfo = UserUtil.getUserInfo();
            this.setData({
                userInfo: userInfo
            });
        });
        configIns.setErpCallback(() => {
            const account = UserUtil.getAccount();
            console.log("account:" + account);
            this.setData({
                accountInfo: account
            });
            this.checkShopInfo(account);
            this.initWxCard();
            this.initCardTime();
        });
        this.initSessionAndUserInfo(configIns.getConfig());
    },
    //用户信息准备完成
    userInfoReady: function () {
        this.closeLoading();
        this.toAttention();
    	getUserService().updateOnlineUserInfo(); 
        this.informList();      
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.customOnload(options);
        if (options.share_code) {
            app.share_code = options.share_code
        }
        if (options.scene) {
            //场景二维码，领券
            PageRedirectUtil.parse(options);
        }
        console.log("redirect", PageRedirectUtil.getData());
        this.loading();
        this.data.options = options;
    },
    checkShopInfo: function (account) {
        if (!WxUtil.enableCheckUserShopInfo()) {
            return;
        }
        var _this = this;
        wx.getLocation({
            type: 'gcj02',
            success: function (data) {
                WxUtil.updateUserBelongShop({
                    account: account,
                    lat: data.latitude,
                    lng: data.longitude
                });
            },
            fail: function (data) {
                console.log("定位信息获取失败");
            }
        });
    },
    getWxCard: function () {
        const account = app.storage.getAccount();
        const cardId = UserUtil.getWxCardId();
        if (ValidateUtil.isEmpty(cardId)) {
            Toast.tip("微信会员卡号无效");
            return;
        }
        const openid = '';
        const url = UserUtil.getWxCardWebUrl(2, cardId);
        /*        this.setData({
                    wxCard: {load: true, wxCardUrl: url}
                });*/
        wx.navigateTo({
            url: '/pages/wxcard/wxcard?params=' + JSON.stringify({
                cardId: cardId,
                openid: openid,
                uniacid: UserUtil.getUniacid()
            })
        });
    },
    onReady: function () {
        UserConfigUtil.initConfig();
        // this.initWxCard();
        // this.checkShopInfo(app.storage.getAccount());
    },
    commonLoad: function () {
        this.fetchPageConfig();
        //导航信息
        this.indexNav();
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.commonLoad();
        this.initUserInfo();
        if (this.data.enableQueryBindWechat) {
            this.toAttention();
        }
    },
    toAttention() {
        const openid = getSessionBean().getOpenid();
        if (Util.isEmpty(openid)) {
            return;
        }
        getOpenidService().fetchRelationOpenId2(openid, (response) => {
            if (!response.isSuccess()) {
                //关注
                PageUtil.navigateTo('attention', {openid: openid});
            }
            this.setData({enableQueryBindWechat: true});
        });
    },
    //加载用户信息
    loadUserInfo: function () {
        /*getSessionService().fetchSession((response) => {
            if (!response.isSuccess()) {
                // this.closeLoading();
                Toast.tip(response.getMsg());
                return;
            }
            const sessionBean = response.getData();
            console.log('sessionBean', sessionBean);
            if (sessionBean.hasRegister()) {
                this.initIndexData({}, (userLoadSucc) => {
                    if (userLoadSucc) {
                        PageRedirectUtil.redirect();
                    }
                });
            } else {
                //没有配置卡包，去注册页面
                PageUtil.redirectTo('register');
            }
        });*/
    },
    //小程序开卡组件
    submitUserInfo: function (e) {
        this.setData({
            registerByWxCard: false
        });
    },
    submitUserInfo2: function (wxCardResponse) {
        const bean = wxCardResponse.getData();
        const extraData = {
            encrypt_card_id: bean.getEncryptCardId(),
            biz: bean.getBiz(),
            outer_str: bean.getOuterStr(),
        };
        wx.navigateToMiniProgram({
            appId: 'wxeb490c6f9b154ef9',
            extraData: extraData,
            success: (res) => {
                console.log("success");
            },
            fail: (res) => {
                console.log("fail");
            }
        });
    },
    //如果通过小程序开卡组件开卡注册，直接注册用户
    register: function (userInfoResponse) {
        const wxUserBean = userInfoResponse.getData();
        const unionId = wxUserBean.getUnionId();
        console.log("unionId:" + unionId);
        getUserService().fetchCardSubmitUserInfo({unionId: unionId}, (response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const bean = response.getData();
            //来源信息设置
            const post = {};
            var data = {
                code: '',
                phone: bean.getMobile(),
                member_name: bean.getName(),
                birthday: bean.getBirthday(),
                token: UserUtil.getToken(),
                session_key: UserUtil.getSessionKey(),
                encryptedData: wxUserBean.getEncryptedData(),
                iv: wxUserBean.getIv(),
                is_code: 2,//不使用验证码
                recommendMobile: '',
                recommendCardId: '',
                lat: '',
                lng: '',
                post: JSON.stringify(post)
            };

            if (app.share_code) {
                data.share_code = app.share_code
            }
            console.log('data', data);
            app.crmApi(
                app.apiService.crm.bindPhone,
                data,
                () => {
                    //注册成功,
                    //重新获取用户信息
                    getSessionService().clearSession();
                    this.loading();
                    this.loadUserInfo();//加载注册完成的用户信息
                },
                () => {
                    Toast.tip("注册失败，请重新进入小程序");
                }
            )
        });
    },

    /**
     * 首页 鉴权回调函数
     */
    launchCallback() {
        /*   if (!app.storage.getAuth().mobile) {
               console.log("去注册");
               this.member_login()
               return false
           }
           console.log("------------获取用户信息");
           this.setData({
               title: app.storage.getAuth().title
           });
           /!**
            * 当程序用户基本信息缓存被清空时，onShow 方法将无法获取首页数据，此时程序会执行 auth.js
            * 完成登录，注册等权限操作，只有当 auth.js 执行完毕，相应的用户基本信息缓存数据才会建立，
            * 此时再调用此页面回调函数去获取首页相关数据，这个操作只在用户缓存数据被清空的情况下执行。
            *!/
           this.indexData();
           this.indexNav();*/
    },

    launchCallbacks() {
        /*      this.setData({
                  accountInfo: app.storage.getAccount()
              })*/
    },

    /**
     * 加载首页数据
     */
    indexData() {

        var that = this

        /**
         * 加载缓存用户基本数据
         */
        that.loadData()
        /**
         * 加载消息列表
         */
        //that.informList()
    },

    /**
     * 会员账户信息获取 失败
     */
    acctFailed() {

    },

    /**
     * 首页nav接口获取
     */
    indexNav() {
        getConfigService().fetchIndexNav((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const bean = response.getData();
            this.setData({
                navload: false,
                navService: bean.getNavList()
            })
        });
    },

    /**
     * 消息通知列表接口获取
     */
    informList() {
        getUserService().fetchNoticeInfo((response) => {
            const list = response.getData();
            let data = false;
            if (ValidateUtil.isArray(list)) {
                if (list.length > 0) {
                    data = list;
                }
            }
            this.setData({
                informItem: data,
            });
        });
    },

    /**
     * 消息通知列表接口获取 成功
     */
    informListSuccess(res) {

        var that = this;

        var info = res.data.data

        // 总条数为0或者未定义时

        if (!info.list || info.total == 0) {

            that.setData({

                informItem: false,

            })

            return

        }

        if (res.data.ret = 200) {

            that.setData({

                informItem: info.list

            })

        }

    },

    /**
     * 消息通知列表接口获取 失败
     */
    informListFail(res, param) {

        wx.stopPullDownRefresh() //停止下拉刷新

    },

    // showLogin() {
    //   var that = this;
    //   if (!app.storage.getShowLogin()) {
    //     that.setData({
    //       login: true
    //     })
    //     app.storage.setShowLogin(1)
    //   }
    // },

    showNotice() {
        var that = this;
        if (!app.storage.getShowNotice()) {
            that.setData({
                notice: true
            })
            app.storage.setShowNotice(1)
        }
    },
    /**
     * 图片动画
     */
    animationImg() {
        //实例化一个动画
        var animation = wx.createAnimation({

            duration: 600,

            timingFunction: 'ease',

        })

        this.animation = animation

        animation.rotate(30).step().rotate(0).step().rotate(-30).step().rotate(0).step();

        //导出动画
        this.setData({

            animationData: animation.export()

        })

    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

        var that = this;

        if (this.data.loading) return;

    },
    userInfoLoadComplete() {
        const mobile = UserUtil.getUserBean().getMobile();
        console.log("userInfoLoadComplete mobile:" + mobile);
        PageRedirectUtil.redirect();
    },
    /**
     * 从缓存中加载会员基本信息数据，数据来源于 auth.js 的登录操作
     */
    loadData() {
        var that = this;
        /**
         * 加载用户基本数据，先从缓存读取
         */
        var Account = UserUtil.getAccount();
        if (Account) {
            that.setData({
                accountInfo: Account
            })
        }

        that.setData({
            userInfo: UserUtil.getUserInfo()
        });

        app.crmApi(
            app.apiService.crm.memInfo,
            {token: app.storage.getAuth().token},
            res => {
                var info = app.storage.getUserInfo()
                if (res.data.ret == 200) {
                    var data = res.data.data
                    if (info.nick_name) {
                        Object.assign(info, data)
                    }
                    app.storage.setUserInfo(data)
                    that.setData({
                        userInfo: data
                    });
                    that.userInfoLoadComplete();
                }
            },
            err => {

            }
        )
        this.loadingBox.hideAniEnd();
        /*getSessionService().fetchErpAccount((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            this.checkShopInfo(account);
            this.initWxCard();
            this.initCardTime();
        })*/
        app.getAcctDetail((account) => {

        })

    },
    messageDetail(evt) {
        var dataset = evt.currentTarget.dataset
        wx.navigateTo({
            url: dataset.url + '?id=' + dataset.object_id,
        })
    },

    /**
     * 关闭操作提示
     */
    closeNotice() {

        var operationNotice = this.selectComponent('#operation_notice')

        operationNotice.hideNotice()

    },

    /**
     * 接收关闭loading事件
     */
    /*    closeLoading() {
            if (app.storage.getShowLogin()) {
                this.showNotice()
            }
        },*/

    /**
     * 跳转模块
     */
    toModule(evt) {
        wx.navigateTo({
            url: evt.currentTarget.dataset.url,
        })
    },

    toQrcode() {
        wx.navigateTo({
            url: 'qrcode/qrcode',
        })
    },
    onMyAccredit: function () {
        this.setData({accredit: false});
        getWxAuthService().userAuth((hasAuth) => {
            if (!hasAuth) {
                Toast.tip("请授权用户信息");
                this.setData({accredit: true});
                return;
            } else {
                this.loading();
                //授权用户信息之后获取用户信息
                this.loadUserInfo();
            }
        });
    },
    onMyEvent(e) {
        console.log("--------------login");
    },
    /**
     * 系统配置
     */
    fetchPageConfig() {
        getConfigService().fetchSystemConfig((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const bean = response.getData();
            const pageConfig = bean.getPageConfig();
            pageConfig.card_url = bean.getCardUrl();
            this.setData({
                pageConfig: pageConfig, support: {
                    supportBalance: bean.isSupportBalance(),
                    supportCoupon: bean.isSupportCoupon(),
                    supportIntegral: bean.isSupportIntegral()
                }
            });
        });
    }
};
var options = {...config, ...pageConfig};
Page(options);
