import ApiUtil from "../../../api/api";
import {Util} from "../../../utils/common";
import {TimeUtil} from "../../../utils/TimeUtil";

var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        mobile: '',
        member_name: '',
        title: '',
        userInfo: {},
        editBtn: '编辑',
        disabled: true,
        disabled_date: true,
        date: '请选择生日',
        date_select: '',
        phone: '',
        in_detail: '',
        is_default: true,
        rty: false,
        birthday: '',
        sex: [{name: '先生', value: 1, checked: false}, {name: '女士', value: 2, checked: false}],
        is_switch: true,//开关
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        if (app.storage.getAuth().title) {
            this.setData({
                title: app.storage.getAuth().title
            })
        }

        this.dialog = this.selectComponent('#dialog')
        this.loadingWave = this.selectComponent('#wave')

    },


    /**
     *  绑定手机号链接
     */
    phoneTap: function () {

        var that = this;

        if (that.data.disabled == false) {

            if (!that.data.is_switch) {

                return

            } else {

                wx.navigateTo({

                    url: '/pages/member/mobile/mobile',

                })

                that.setData({

                    is_switch: false

                })

            }

        }

    },
    /**
     * bindinput 获取输入的姓名
     */
    userNameBind: function (e) {
        var that = this;
        that.setData({
            member_name: e.detail.value
        });
    },

    /**
     *  用户资料接口获取
     */
    getUser: function () {
        var that = this;
        var data = {
            token: app.storage.getAuth().token
        }
        app.crmApi(
            app.apiService.crm.memberDetail,
            data,
            that.getUserSuccess,
            that.getUserFail
        )
    },

    /***
     * 用户资料接口获取 成功
     */
    getUserSuccess: function (res) {

        var that = this;

        if (res.data.ret == 200) {

            var userInfo = res.data.data;

            var old = app.storage.getUserInfo()

            for (var k in userInfo) {

                old[k] = userInfo[k]

            }

            app.storage.setUserInfo(old)

            that.loadData()

        } else {

            var asd = res.data.msg

        }

    },

    /**
     * 加载数据
     */
    loadData() {
        var that = this
        var sex = that.data.sex;
        var date = that.data.date;
        var userInfo = app.storage.getUserInfo();
        ApiUtil.fetchErpUserInfo((succ, res) => {
            if (!succ) {
                return;
            }
            const erpSex = Util.get(res, 'data.sex', '');
            const erpMemberName = Util.get(res, 'data.memberName', '');
            const birthday = Util.get(res, 'data.birthday', TimeUtil.getNowDate('Y-m-d'));
            this.setData({date: birthday, sex: erpSex});
            for (var i = 0; i < sex.length; i++) {
                if (sex[i].value == erpSex) {
                    sex[i].checked = true
                } else {
                    sex[i].checked = false
                }
                that.setData({
                    sex: sex
                })
            }
            userInfo.sex = erpSex;
            userInfo.birthday = birthday;
            userInfo.member_name = erpMemberName;
            that.setData({
                userInfo: userInfo,
                date: userInfo.birthday ? userInfo.birthday : '请选择生日'
            })
        });

    },

    /***
     * 用户资料接口获取 失败
     */
    getUserFail: function (res) {

    },

    /**
     * 用户资料编辑接口获取
     */
    userEdit: function (e) {
        var that = this;
        var info = e.detail.value
        var data = {
            token: app.storage.getAuth().token,
            member_name: info.member_name,
            phone: info.phone,
            sex: info.sex,
            encryptedData: e.encryptedData,
            iv: e.iv,
        }
        if (info.date) {
            data.birthday = info.date
        }
        that.loadingWave.showLoading()
        app.crmApi(
            app.apiService.crm.memberEdit,
            data,
            that.userEditSuccess,
            that.userEditFail,
            info
        )
    },
    /**
     * 用户资料编辑接口获取 成功
     */
    userEditSuccess: function (res, info) {
        var that = this;
        if (res.data.ret == 200 && info) {
            var userInfo = app.storage.getUserInfo()
            userInfo.member_name = info.member_name
            userInfo.sex = info.sex
            app.storage.setUserInfo(userInfo)
            that.loadData()
        }
        that.setData({
            disabled: true,
            disabled_date: true,
            editBtn: '编辑'
        })
        that.loadingWave.hideAniEnd()
    },
    /**
     * 用户资料编辑接口获取 失败
     */
    userEditFail: function (e) {
        var that = this;
        var msg = '未修改信息'
        if (e.data.msg) {
            msg = e.data.msg
        }
        wx.showToast({
            title: msg,
            image: '/images/wrong-load.png',
            mask: true,
            duration: 1500,
        })
        that.loadingWave.hideAniEnd()
        that.setData({
            disabled: true,
            disabled_date: true,
            editBtn: '编辑'
        })
    },
    /**
     * 提交
     */
    userSubmit: function (e) {
        var that = this;
        var info = e.detail.value
        var userInfo = app.storage.getUserInfo()
        if (that.data.disabled) {
            that.setData({
                disabled: false,
                editBtn: '确认',
            })
            if (app.storage.getUserInfo().birthday) {
                that.setData({
                    disabled_date: true,
                })
            } else {
                that.setData({
                    disabled_date: false,
                })
            }
        } else if (userInfo.member_name == info.member_name && (userInfo.birthday == info.date || !info.date) && userInfo.sex == info.sex) {
            wx.showToast({
                title: '未修改信息',
                image: '/images/wrong-load.png',
                mask: true,
                duration: 1000,
            })
            that.setData({
                disabled: true,
                editBtn: '编辑',
            })
            return false
        }
        else {
            wx.getUserInfo({
                withCredentials: true,
                lang: 'zh_CN',
                success: function (res) {
                    e.encryptedData = res.encryptedData
                    e.iv = res.iv
                    that.userEdit(e);
                },
                fail: function (res) {
                    e.encryptedData = ''
                    e.iv = ''
                    that.userEdit(e);
                }
            })
        }
    },
    //日期
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value,
            birthday: e.detail.value
        })
        var date_select = this.data.date
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var page = this;
        var that = this;
        that.setData({
            is_switch: true,
        })
        that.loadData()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    showNotice() {

        if (this.data.disabled) {

            this.dialog.showDialog({

                'type': 'warning',

                'title': '温馨提示',

                'content': '修改资料，请点击编辑按钮～'

            })

        }

    },

    address() {

        wx.navigateTo({

            url: '/pages/address/address',

        })

    },

    setMember() {
        var that = this
        wx.getSetting({
            success: (res) => {
                if (!res.authSetting['scope.userInfo']) {
                    wx.openSetting({
                        success(res) {
                            if (res.authSetting['scope.userInfo']) {
                                that.getMember()
                            }
                        }
                    })
                } else {
                    that.getMember()
                }
            }
        })
    },

    getMember() {
        var that = this
        wx.getUserInfo({
            lang: 'zh_CN',              //设置语言为中文
            success: function (res) {   //用户同意微信数据授权
                /**
                 * 将会员资料保存到APP的全局变量中，方便全局调用
                 */
                const wxUserInfo = res.userInfo;
                var userInfo = app.storage.getUserInfo()
                let data = {
                    token: app.storage.getAuth().token
                }
                if (userInfo.nick_name != wxUserInfo.nickName) {
                    userInfo.nick_name = wxUserInfo.nickName
                    data.nick_name = wxUserInfo.nickName
                }
                if (userInfo.pic_head != wxUserInfo.avatarUrl) {
                    userInfo.pic_head = wxUserInfo.avatarUrl
                    data.user_headimg = wxUserInfo.avatarUrl
                }
                if (userInfo.sex != wxUserInfo.gender) {
                    userInfo.sex = wxUserInfo.gender
                    data.sex = wxUserInfo.gender
                }
                /**
                 * 用户同意授权微信数据后，若用户数据有更新，则调用后台业务服务器接口，将用户最新信息更新到服务器
                 */
                app.crmApi(app.apiService.crm.memberEdit, data)
                app.storage.setUserInfo(userInfo)
                that.setData({
                    userInfo: userInfo
                })
                wx.showToast({
                    title: '授权成功',
                    image: '/images/34.png',
                    duration: 1000
                })
            },
            fail: function (res) { //用户拒绝微信数据授权

            }
        })
    }

})
