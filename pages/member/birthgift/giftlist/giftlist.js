import pageConfig from "../../../../utils/page";
import {getImageService} from "../../../../module/common/image";
import {getBirthGiftService} from "../../../../module/common/birthgift";
import {Toast} from "../../../../utils/Toast";
import {TimeUtil} from "../../../../utils/TimeUtil";
import PageUtil from "../../../../utils/PageUtil";
import {UserUtil} from "../../../../utils/UserUtil";
import {Util} from "../../../../utils/common";

var config = {
    /**
     * 页面的初始数据
     */
    data: {
        list: [],
        imageService: null
    },
    selectRow: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.list;
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            obj.selected = i == index;
        }
        this.setData({list: list});
    },
    imageError: function (e) {
        if (e.type == 'error') {
            //图片加载错误
            const index = e.currentTarget.dataset.index;
            const list = this.data.list;
            if (list[index]) {
                const obj = list[index];
                this.data.imageService.useDefaultImage(obj);
                this.setData({list: list});
            }
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);
        const imageService = getImageService('/images/cry.png');
        imageService.setImageField('image');

        this.loading();
        getBirthGiftService().fetchGiftList((response) => {
            this.closeLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const account = UserUtil.getAccount();
            const birthday = Util.get(account, "birthday", "");
            const timestamp = TimeUtil.transToTimestramp(birthday);
            const dateInfo = TimeUtil.getDateInfo(timestamp);
            const bean = response.getData();
            const list = bean.getList();
            const newList = [];
            for (let i = 0; i < list.length; i++) {
                const obj = list[i];
                obj.selected = true;
                const timeFormat = TimeUtil.transToTimestramp(obj.timeFormat);
                obj.month = TimeUtil.getDateInfo(timeFormat).month;
                if (obj.month != dateInfo.month) {
                    // continue;
                }
                imageService.format(list[i]);
                newList.push(obj);
            }
            this.setData({list: newList, imageService: imageService});
        });
    },
    giftDetail: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.list;
        if (list[index]) {
            const obj = list[index];
            PageUtil.navigateTo('giftdetail', {data: JSON.stringify(obj)});
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
