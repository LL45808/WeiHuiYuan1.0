import pageConfig from "../../../../utils/page";
import {getImageService} from "../../../../module/common/image";
import {getBirthGiftService} from "../../../../module/common/birthgift";
import {Toast} from "../../../../utils/Toast";
import {TimeUtil} from "../../../../utils/TimeUtil";
import {UserUtil} from "../../../../utils/UserUtil";
import {Util} from "../../../../utils/common";
import ValidateUtil from "../../../../utils/ValidateUtil";

var WxParse = require('../../../../wxParse/wxParse.js');
var config = {
    /**
     * 页面的初始数据
     */
    data: {
        goodsList: [],
        imageService: null,
        appointment: null,
        enableReceive: true,
        receiveMsg: '确认',
        richtext: {
            hidden: true
        },
        richtextcontent: '<div>text</div>',
    },
    selectRow: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.goodsList;
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            obj.selected = i == index;
        }
        this.setData({goodsList: list});
    },
    imageError: function (e) {
        if (e.type == 'error') {
            //图片加载错误
            const index = e.currentTarget.dataset.index;
            const list = this.data.goodsList;
            if (list[index]) {
                const obj = list[index];
                this.data.imageService.useDefaultImage(obj);
                this.setData({goodsList: list});
            }
        }
    },
    lookGoodsDetail: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.goodsList;
        if (list[index]) {
            const content = list[index]['content'];
            if (ValidateUtil.isEmpty(content)) {
                return;
            }
            WxParse.wxParse('richtextcontent', 'html', content, this, 5);
            this.setData({richtext: {hidden: false}});
        }
    },
    hideRichText: function () {
        this.setData({richtext: {hidden: true}});
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);
        const imageService = getImageService('/images/cry.png');
        imageService.setImageField('image');

        const obj = JSON.parse(options.data);
        this.data.appointment = obj;
        const appointId = obj.appointId;
        this.loading();
        getBirthGiftService().fetchGiftDetail({appointId: appointId}, (response) => {
            this.closeLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const bean = response.getData();
            const list = bean.getGoodsList();
            for (let i = 0; i < list.length; i++) {
                const obj = list[i];
                obj.selected = false;
                imageService.format(list[i]);
            }
            if (list.length > 0) {
                list[0].selected = true;
            }
            this.setData({goodsList: list, imageService: imageService});
        });
    },

    appointment: function () {
        if (!this.data.enableReceive) {
            return;
        }
        const appointItem = this.data.appointment;
        if (appointItem == null) {
            Toast.tip("预约信息无效");
            return;
        }
        const list = this.data.goodsList;
        if (list.length == 0) {
            Toast.tip('没有礼品信息');
            return;
        }
        let goods = null;
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            if (obj.selected) {
                goods = obj;
            }
        }
        if (goods == null) {
            Toast.tip('未选择礼品信息');
            return;
        }
        const account = UserUtil.getAccount();
        const userInfo = UserUtil.getUserInfo();
        const mobile = Util.get(userInfo, 'phone', '');
        if (!ValidateUtil.isMobile(mobile)) {
            Toast.tip("用户信息错误，请重新进入小程序");
            return;
        }
        const birthday = Util.get(account, 'birthday', '');

        this.setData({enableReceive: false, receiveMsg: '请求中...'});
        this.loading();
        getBirthGiftService().appointment({
            appointId: appointItem.appointId,
            mobile: mobile,
            birthday: birthday,
            goodsId: goods.goodsId
        }, (response) => {
            this.closeLoading();
            Toast.tip(response.getMsg());
            this.setData({enableReceive: true, receiveMsg: '确认'});
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
