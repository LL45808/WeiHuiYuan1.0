import {getCardTypeInfnoBean, getCardTypeService} from "../../../module/system/cardtype";
import {Toast} from "../../../utils/Toast";
import {Util} from "../../../utils/common";
import PageUtil, {PageRedirectUtil} from "../../../utils/PageUtil";
import {getCouponService} from "../../../module/user/coupon";
import {TimeUtil} from "../../../utils/TimeUtil";
import ValidateUtil from "../../../utils/ValidateUtil";
import {getSessionBean} from "../../../module/common/session";
import {clearWhenLogin} from "../../../module/common/StorageUtil";
import {UserUtil} from "../../../utils/UserUtil";
import {getOpenidService} from "../../../module/common/openid";
import {getShopService} from "../../../module/common/shop";
import {getUserService} from "../../../module/user/user";

var app = getApp()
const page_code = 'wxpro_crm_member_qrcode'
var maxTime = 60
var currentTime = maxTime //倒计时的事件（单位：s）
var interval = null
var hintMsg = null // 提示

Page({

    /**
     * 页面的初始数据
     */
    data: {
        code: "",
        rty: false,
        phoneValue: '',//手机号
        name: '',//姓名
        tapContent: '发送验证码',
        forbid: false,
        intervarID: undefined,
        submitForbid: true,
        submitContent: '注册 / 绑定会员卡',
        network: 1,
        dialog: {},
        date: '1990-01-01',
        focus: false,
        is_code: 1, // 1-使用验证码 2-不是用验证码
        nameDisabled: false,
        focusName: false,
        head_logo: '',
        accredit: false,
        result_accredit: {},
        location: {
            lat: '',//纬度
            lng: '',//经度
        },
        locationSuccess: false,
        cardType: {
            enableSelectCardType: false,
            cardTypeList: [],
            selectCardTypeName: '',
            selectCardType: null,
            config: null,
            autoUpgrade: false
        },
        wxUserInfo: {
            encryptedData: '',
            iv: ''
        },
        payInfo: null,
        registerSource: {},
        clickSubmitTime: 0,
        loadCardTypeInfoComplete: false,
        loadLocationInfoComplete: false,
        shopData: {
            list: [],
            name: '请选择门店',
            selectedShop: null,
            nameList: [],
            registerSelectShop: false,//注册选择门店
        },
        toAttentionPage: false,
        birthdayTypeName: '阳历',
        birthdayType: 1,
    },
    birthTypeChange: function (e) {
        if (e.detail.value == 1) {
            this.setData({birthdayType: 1});
        } else {
            this.setData({birthdayType: 2});
        }
    },
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },
    /**
     *选择卡类行
     */
    selectCardTypeRow: function (data) {
        const item = data.detail;
        /*        Util.updateComponentData(this, {
                    cardType: {
                        selectCardTypeName: item.rowName,
                        selectCardType: item
                    }
                });*/
        const cardType = this.data.cardType;
        cardType.selectCardTypeName = item.rowName;
        cardType.selectCardType = item;
        this.setData({cardType: cardType});
    },
    selectCardType: function () {
        this.poplist.show();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.poplist = this.selectComponent("#poplist");
        var mobile = UserUtil.getUserBean().getMobile();
        if (ValidateUtil.isMobile(mobile)) {
            wx.redirectTo({
                url: '/pages/index/index'
            })
            return
        }

        //设置注册来源
        const pageRedirectBean = PageRedirectUtil.getBean();
        if (pageRedirectBean.getFromSource() == 'donateReceiveCoupon') {
            //用户通过转赠注册
            const query = pageRedirectBean.getQuery();
            const couponCode = Util.get(query, 'couponCode', "");//转赠优惠券id
            if (!Util.isEmpty(couponCode)) {
                //获取优惠券转赠用户信息
                getCouponService().getDonateCoupon({couponCode: couponCode}, (response) => {
                    //如果优惠券有效
                    if (response.isSuccess()) {
                        const bean = response.getData();
                        const data = {
                            fromCardId: bean.getFromCardId(),
                            fromSource: 'donateReceiveCoupon'
                        }
                        this.setData({registerSource: data});
                    }
                });
            }
        }

        getCardTypeService().fetchCardTypeConfig((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const configIns = response.getData();
            let cardTypeList = configIns.getCardTypeList(true, true);

            let selectCardType = cardTypeList.length > 0 ? cardTypeList[0] : null;
            let selectCardTypeName = selectCardType ? selectCardType.rowName : '';
            if (!configIns.isRegisterSelectCardType()) {
                selectCardType = null;
                selectCardTypeName = '';
                cardTypeList = [];
            }
            const updateData = {
                cardType: {
                    enableSelectCardType: configIns.isRegisterSelectCardType(),
                    cardTypeList: cardTypeList,
                    config: configIns,
                    selectCardType: selectCardType,
                    selectCardTypeName: selectCardTypeName
                },
                loadCardTypeInfoComplete: true
            };
            this.setData(updateData);
            if (this.data.locationSuccess) {
                this.loadShopList();
            }
        });


        var that = this
        that.getUserInfo()
        that.page_config();
        var phoneValue = options.mobile;

        if (phoneValue) {
            that.setData({
                phoneValue: phoneValue,
                is_code: 2,
                nameDisabled: true
            })
        }
        that.dialog = that.selectComponent('#dialog')
        that.loadingWave = that.selectComponent('#wave')
        wx.onNetworkStatusChange(function (res) {
            if (!res.isConnected) {
                that.setData({
                    network: 0
                })

                that.dialog.showDialog({
                    'type': 'warning',
                    'title': '网络未链接！',
                    'content': '请检查您的网络～'
                });

            } else {
                that.setData({
                    network: 1
                })
            }
        });
        var _this = this;
        wx.getLocation({
            type: 'gcj02',
            success: function (data) {
                _this.data.location.lat = data.latitude;
                _this.data.location.lng = data.longitude;
                _this.data.locationSuccess = true;
                if (_this.data.loadCardTypeInfoComplete) {
                    _this.loadShopList();
                }
            },
            fail: function (data) {
                /*wx.showModal({
                    content:'定位信息获取失败，如果已拒绝授权，需要删除小程序重新进入',
                    showCancel:false
                });*/
            }
        });
    },
    loadShopList() {
        const configIns = this.data.cardType.config;
        if (configIns.isRegisterSelectShop()) {
            const loc = this.data.location;
            getShopService().fetchShopListWithDistanceAsc({name: '', lat: loc.lat, lng: loc.lng}, (response) => {
                if (!response.isSuccess()) {
                    return;
                }

                const unionid = getSessionBean().getUnionid();
                //加载扫码信息
                getUserService().fetchUserScanCodeInfo({unionId: unionid}, (userResponse) => {
                    const userResult = userResponse.getData();
                    let scanShopCode = '';
                    if (userResponse.isSuccess()) {
                        scanShopCode = userResult.shopCode;
                    }
                    const result = response.getData();
                    const list = result.list;
                    const nameList = [];
                    const newList = [];
                    let shopItem = null;
                    for (let i = 0; i < list.length; i++) {
                        const obj = list[i];
                        newList.push({name: obj.store_name, shopCode: obj.shopCode});
                        nameList.push(obj.store_name);
                        if (obj.shopCode == scanShopCode) {
                            shopItem = obj;
                        }
                    }
                    if (nameList.length <= 0) {
                        return;
                    }
                    if (shopItem == null) {
                        shopItem = list[0];
                    }
                    this.setData({
                        shopData: {
                            name: shopItem.store_name,
                            nameList: nameList,
                            registerSelectShop: true,
                            selectedShop: shopItem,
                            list: newList
                        }
                    });
                });
            });
        }
    },
    //绑定门店
    bindShopChange(e) {
        const index = e.detail.value;
        const shopItem = this.data.shopData.list[index];
        const shopData = this.data.shopData;
        shopData.name = shopItem.name;
        shopData.selectedShop = shopItem;
        this.setData({shopData: shopData});
    },
    toAttention() {
        const openid = getSessionBean().getOpenid();
        if (Util.isEmpty(openid)) {
            Toast.tip('openid无效');
            return;
        }
        getOpenidService().fetchRelationOpenId(openid, (response) => {
            if (!response.isSuccess()) {
                //没有关联信息
                PageUtil.navigateTo('attention', {openid: openid});
            }
        });
    },

    /**
     * 微信授权绑定手机号
     */
    getPhoneNumber: function (e) {
        var that = this
        that.loadingWave.showLoading()
        var data = {}
        if (e.detail.encryptedData) {
            data.iv = e.detail.iv
            data.encryptedData = e.detail.encryptedData
            that.wxPhone(data)
            return
        } else if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
        } else {
            that.dialog.showDialog({
                'type': 'warning',
                'title': '出错了',
                'content': '获取手机号失败，请检查您的网络！'
            });
        }
        that.loadingWave.hideAniEnd()
    },
    /**
     * 微信小程序授权手机号解密接口
     */
    wxPhone: function (e) {
        var that = this;
        var data = {
            encryptedData: e.encryptedData,
            iv: e.iv,
            session_key: app.storage.getAuth().session_key
        }
        app.crmApi(
            app.apiService.crm.getWxPhone,
            data,
            that.wxPhoneSuccess,
            that.wxPhoneFailed
        )
    },

    /**
     * 获取绑定手机号成功
     */
    wxPhoneSuccess(res) {
        var that = this;
        that.loadingWave.hideAniEnd()
        var info = res.data.data
        if (res.data.ret == 200 && info.purePhoneNumber) {
            that.setData({
                phoneValue: info.purePhoneNumber,
                is_code: 2,
                nameDisabled: true
            })
        }
    },

    /**
     *
     */
    wxPhoneFailed(res) {
        var that = this
        that.loadingWave.hideAniEnd()
        that.dialog.showDialog({
            'type': 'warning',
            'title': '出错了',
            'content': '该设备无法获取手机号，请手动输入！'
        });
    },

    /**
     * 注册协议跳转链接
     */
    agreementTap: function () {
        wx.navigateTo({
            url: '/pages/member/agreement/agreement',
        })
    },

    /**
     * bindinput 获取输入的姓名
     */
    userNameBind: function (e) {
        var that = this;
        var txt = e.detail.value
        that.setData({
            name: txt
        });
    },
    //bindinput 获取输入的手机号的值
    validatemobile: function (e) {
        var that = this;
        that.setData({
            phoneValue: e.detail.value
        });
    },
    //验证码输入
    yzmImport: function (e) {
        var that = this;
        var value = e.detail.value
        that.setData({
            code: value,
            submitForbid: false
        })
        if (value.length == 6) {
            that.setData({
                focus: false
            })
        }
    },
    //发送验证码验证手机格式
    sendTap: function (e) {
        var that = this;
        var phone = that.data.phoneValue;
        var myreg = /^[1][0-9]{10}$/;
        if (phone.length != 11 || !myreg.test(phone)) {
            wx.showToast({
                title: '手机号码有误',
                image: '/images/caution.png',
                duration: 1000
            })
            that.setData({
                rty: false
            })
            return false;
        }
        that.message(phone);
    },

    //保存提交
    bindingformSubmit: function (e) {
        if (!this.data.locationSuccess) {
            /* wx.showModal({content:'定位失败，请重新进入小程序',showCancel:false});
             return;*/
        }
        var that = this;
        var username = that.data.name//姓名
        if (!username) {
            Toast.tip("请填写姓名!");
            return
        }
        var phonevalue = that.data.phoneValue//手机号码
        var myreg = /^[1][0-9]{10}$/;
        if (phonevalue.length != 11 || !myreg.test(phonevalue)) {
            Toast.tip("手机号码有误");
            return
        }
        var birthday = that.data.date
        if (birthday == '请选择生日') {
            Toast.tip("请选择生日!");
            return
        }
        var lastTime = this.data.clickSubmitTime;
        var nowTime = TimeUtil.getNowTime();
        if (nowTime - lastTime <= 5000) {
            return;
        }
        this.data.clickSubmitTime = nowTime;

        var fetchUserInfo = function (fetchWxUserInfo, wxUserInfo) {
            if (fetchWxUserInfo) {
                wx.getUserInfo({
                    withCredentials: true,
                    lang: 'zh_CN',
                    success: function (res) {
                        e.encryptedData = res.encryptedData
                        e.iv = res.iv
                        that.getBindPhone(e);
                    },
                    fail: function (res) {
                        e.encryptedData = ''
                        e.iv = ''
                        that.getBindPhone(e);
                    }
                })
            } else {
                that.getBindPhone(wxUserInfo);
            }
        };
        fetchUserInfo(true);
        /* var startPreparePay = function () {
             const openid = that.data.payInfo.openid;
             const cardTypeInfo = that.data.cardType.selectCardType;
             const cardTypeInfoBean = getCardTypeInfnoBean(cardTypeInfo);
             getPayService().fetchCardTypePayInfo({
                 mobile: phonevalue,
                 cardCode: cardTypeInfoBean.getCardCode()
             }, (response) => {
                 if (!response.isSuccess()) {
                     console.log("支付信息不存在");
                     //没有支付信息，准备支付
                     getPayService().buyCardPreparePay({
                         openid: openid,
                         cardCode: cardTypeInfoBean.getCardCode(),
                         mobile: phonevalue,
                         amount: cardTypeInfoBean.getAmount(),
                         cardType: cardTypeInfoBean.getCardTypeName()
                     }, (response) => {
                         console.log("开始支付准备");
                         if (!response.isSuccess()) {
                             console.log(response.getMsg());
                             Toast.tip(response.getMsg());
                             return;
                         }
                         const bean = response.getData();
                         if (bean.isPay()) {
                             //如果已经支付,走正常支付流程
                             fetchUserInfo(false, that.data.wxUserInfo);
                             return;
                         }

                         getPayService().wxPay(bean, (response) => {
                             console.log("用户支付完成", that.data.wxUserInfo);
                             Toast.tip(response.getMsg());
                             if (response.isSuccess()) {
                                 //开始走注册流程
                                 fetchUserInfo(false, that.data.wxUserInfo);
                             }
                         })
                     });
                 } else {
                     console.log("支付信息存在");
                     //已经支付，进入注册流程
                     fetchUserInfo(false, that.data.wxUserInfo);
                 }
             });
         };
         var cardTypeObj = this.data.cardType;
         if (typeof cardTypeObj.config == 'object') {
             const configIns=cardTypeObj.config;
             //判断是否需要选择卡类信息
             if(!configIns.isRegisterSelectCardType()){
                 //如果不许需要选择卡类，直接注册
                 fetchUserInfo(true,null);
                 return;
             }

             var selectedCardType = cardTypeObj.selectCardType;
             if (ValidateUtil.isEmpty(selectedCardType)) {
                 Toast.tip("请选择卡类");
                 return;
             }
             var selectCardTypeBean = getCardTypeInfnoBean(selectedCardType);
             //判断卡类升级需要的金额
             if (selectCardTypeBean.getAmount() > 0) {
                 const tempObj = {erpUserBean: null};
                 ToolUtil.thenSequence((complete) => {
                     //查询用户在erp中是否已经存在
                     getUserService().fetchUserInErp({mobile: phonevalue}, (response) => {
                         if (response.isSuccess()) {
                             console.log("erp中存在该用户");
                             //erp会员信息存在
                             tempObj.erpUserBean = response.getData();
                         } else {
                             console.log("erp不存在该用户");
                         }
                         complete();
                     });
                 }).then((complete) => {
                     //查询用户能否自动升级
                     const erpUserBean = tempObj.erpUserBean;
                     if (erpUserBean != null) {
                         const erpCardType = erpUserBean.getCardType();
                         const cardId = erpUserBean.getCardId();
                         const selectedCardType = cardTypeObj.selectCardType.cardType;
                         const configIns = this.data.cardType.config;
                         if (configIns.cardTypeEqual(erpCardType, selectedCardType)) {
                             console.log("卡类相同自动升级");
                             //卡类相同
                             Util.updateComponentData(this, {
                                 cardType: {
                                     autoUpgrade: true
                                 }
                             });
                             complete();
                         } else {
                             console.log("获取是否能否自动升级");
                             //当前会员卡类等级，小于选择的会员卡类等级才需要升级
                             getUserService().fetchUserEnableUpgrade({
                                 cardType: selectedCardType,
                                 cardId: cardId
                             }, (response) => {
                                 if (response.isSuccess()) {
                                     if (response.getData().userEnableAutoUpgrade()) {
                                         console.log("自动升级");
                                     } else {
                                         console.log("不能自动升级");
                                     }
                                     Util.updateComponentData(this, {
                                         cardType: {
                                             autoUpgrade: response.getData().userEnableAutoUpgrade()
                                         }
                                     });
                                 } else {
                                     console.log("请求错误");
                                 }
                                 complete();
                             });
                         }
                     } else {
                         complete();
                     }
                 }).then((complete) => {
                     //如果不能自动升级，走支付流程
                     if (!this.data.cardType.autoUpgrade) {
                         Util.updateComponentData(this, {});
                         //支付，通过手机号识别,先支付，后注册,先查询支付记录是否存在一个未处理的记录
                         //如果有未处理的，标识用户之前已经支付过，但是没有继续注册
                         //支付
                         //判断是否已经支付过
                         if (this.data.payInfo == null) {
                             wx.getUserInfo({
                                 withCredentials: true,
                                 lang: 'zh_CN',
                                 success: function (res) {
                                     console.log("获取用户信息", res);
                                     getUserService().parseUserInfo({
                                         iv: res.iv,
                                         encryptedData: res.encryptedData
                                     }, (response) => {
                                         console.log("解析", response.getData());
                                         if (!response.isSuccess()) {
                                             Toast.tip(response.getMsg());
                                             return;
                                         }
                                         Util.updateComponentData(that, {
                                             wxUserInfo: {
                                                 iv: res.iv,
                                                 encryptedData: res.encryptedData
                                             }
                                         });
                                         const bean = response.getData();
                                         console.log("openId", bean.getOpenId());
                                         Util.updateComponentData(that, {
                                             payInfo: {openid: bean.getOpenId()}
                                         });
                                         startPreparePay();
                                     });
                                 },
                                 fail: function (res) {
                                     Toast.tip('用户信息获取失败');
                                 }
                             })
                         } else {
                             startPreparePay();
                         }
                     } else {
                         //如果自动升级，走正常流程
                         fetchUserInfo(true);
                     }
                 }).execute();
             } else {
                 fetchUserInfo(true);
             }
         } else {
             fetchUserInfo(true);
         }*/


    },

    getBindPhone: function (e) {
        var that = this;
        var name = that.data.name;
        var filterRule = /^[A-Za-z0-9\u4e00-\u9fa5]{1,20}$/;
        if (!filterRule.test(name)) {
            wx.showModal({
                title: '',
                content: '姓名只能输入1-20个字符，并且只能输入汉字、数字和英文~~~',
                showCancel: false
            })
            that.setData({
                submitContent: '注册 / 绑定会员卡',
                submitForbid: false
            })
        } else {
            that.setData({
                submitContent: '正在提交...',
                submitForbid: true
            });

            var cardTypeObj = this.data.cardType;
            var hasCardInfo = typeof cardTypeObj.config == 'object';
            var selectedCardType = that.data.cardType.selectCardType;
            var post = {
                hasCardInfo: hasCardInfo
            };
            var registerSource = this.data.registerSource;
            for (var key in registerSource) {
                if (typeof registerSource[key] == 'string') {
                    post[key] = registerSource[key];
                }
            }
            if (hasCardInfo) {
                var cardBean = getCardTypeInfnoBean(selectedCardType);
                post.cardCode = cardBean.getCardCode();
                post.cardName = cardBean.getCardTypeName();
                post.autoUpgrade = cardTypeObj.autoUpgrade;//是否自动升级
            }

            var shareData = app.shareData;
            var data = {
                code: that.data.code,
                phone: that.data.phoneValue,
                member_name: that.data.name,
                birthday: that.data.date,
                token: app.storage.getAuth().token,
                session_key: app.storage.getAuth().session_key,
                encryptedData: e.encryptedData,
                iv: e.iv,
                is_code: that.data.is_code,
                recommendMobile: shareData.mobile,
                recommendCardId: shareData.cardId,
                lat: that.data.location.lat,
                lng: that.data.location.lng,
                post: JSON.stringify(post),
                birthdayType: this.data.birthdayType,
            };
            let manualShopCode = '';
            const shopData = this.data.shopData;
            if (shopData.registerSelectShop) {
                const shopItem = shopData.selectedShop;
                if (shopItem != null) {
                    manualShopCode = shopItem.shopCode;
                }
            }
            data.manualShopCode = manualShopCode;

            if (app.share_code) {
                data.share_code = app.share_code
            }
            that.loadingWave.showLoading()
            app.crmApi(
                app.apiService.crm.bindPhone,
                data,
                that.bindSuccess,
                that.bindFail
            )
        }
    },

    bindSuccess: function (res) {
        var that = this
        if (res.data.ret == 200) {
            //清除session信息，注册完后重新获取session
            clearWhenLogin();
            /*if (res.data.data.card_id) {
                var account_info = {}
                if (res.data.data.account) {
                    account_info = res.data.data.account
                }
                app.storage.setAccount(account_info)
            }*/
            //清空推荐信息
            app.shareData = {mobile: '', cardId: ''};
            wx.redirectTo({
                url: '/pages/index/index?login=1'
            })
            that.setData({
                submitContent: '注册 / 绑定会员卡'
            });
            that.loadingWave.hideAniEnd()
        }
    },


    bindFail(res) {
        var that = this;
        that.loadingWave.hideAniEnd()
        that.setData({
            submitContent: '注册 / 绑定会员卡',
            submitForbid: false
        })

        if (!that.data.network) {
            this.dialog.showDialog({
                'type': 'warning',
                'title': '网络未链接！',
                'content': '请检查您的网络！'
            });
        } else {
            var msg = '验证码错误，请重新输入～'
            if (res.data.msg) {
                msg = res.data.msg
            }
            var ret = '出错了！'
            if (res.data.ret) {
                ret = res.data.ret
            }

            that.dialog.showDialog({
                'type': 'warning',
                'title': ret,
                'content': msg
            })
        }
    },

    //短信接口
    message: function (phone) {
        const data = {mobile: phone}
        var that = this
        that.setData({
            forbid: true,
            tapContent: '正在发送...'
        })
        app.crmApi(
            app.apiService.crm.sendVerify,
            data,
            that.verifySuccess,
            that.verifyFail
        )
    },

    verifySuccess: function (res) {
        var that = this;
        if (res.data.ret == 200) {
            currentTime = 60;
            interval = setInterval(function () {
                currentTime--;
                that.setData({
                    tapContent: currentTime
                })
                if (currentTime <= 0) {
                    clearInterval(interval)
                    currentTime = -1
                    that.setData({
                        tapContent: '发送验证码',
                        forbid: false
                    })
                }
            }, 1000)
        } else {
            that.setData({
                tapContent: '发送验证码',
                forbid: false
            })
        }
    },

    verifyFail: function (res) {
        this.dialog.showDialog({
            'type': 'warning',
            'title': '出错了',
            'content': '短信验证码发送失败，请检查您的网络！'
        });
        this.setData({
            tapContent: '发送验证码',
            forbid: false
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

        this.page_config();
        this.toAttention();
    },

    /**
     * 可配置项（logo）
     */
    page_config() {
        var that = this;
        var showConfig = app.storage.getShowConfig();
        if (showConfig.wxpro_crm_member_qrcode && showConfig.wxpro_crm_member_qrcode.configs) {
            var head_logo = showConfig.wxpro_crm_member_qrcode.configs.head_logo.value
            if (head_logo) {
                that.setData({
                    head_logo: head_logo
                })
                return
            }
        }
        app.crmApi(
            app.apiService.crm.pageConfig,
            {page_code: page_code},
            that.pageConfigSuccess,
        )
    },
    pageConfigSuccess: function (res) {
        var that = this
        const config = res.data.data[page_code].configs
        if (config.head_logo.value) {
            var showConfig = app.storage.getShowConfig();
            showConfig = typeof showConfig == 'object' ? showConfig : {};
            showConfig.head_logo = config.head_logo.value
            app.storage.setShowConfig(showConfig)
            that.setData({
                head_logo: showConfig.head_logo
            })
        }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    fillIn() {
        this.setData({
            phoneValue: '',
            is_code: 1,
            nameDisabled: false,
            focusName: true
        })

    },
    // 授权信息
    getUserInfo() {
        var that = this
        wx.getUserInfo({
            lang: 'zh_CN',
            success: function (res) {
                that.setData({
                    result_accredit: res
                })
            },
            fail: function (res) {
                that.setData({
                    accredit: true
                })
            }
        })
    },
    // 确认授权回调
    onMyAccredit(e) {
        var that = this
        if (e.detail.data.errMsg == "getUserInfo:ok") {
            that.setData({
                accredit: false,
                result_accredit: e.detail.data
            })
        }
    }
})
