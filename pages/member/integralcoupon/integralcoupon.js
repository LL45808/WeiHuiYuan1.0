// pages/member/cardtype/cardtype.js
import {Toast} from "../../../utils/Toast";
import pageConfig from "../../../utils/page";
import {getCouponService} from "../../../module/user/coupon";
import {UserUtil} from "../../../utils/UserUtil";
import {Util} from "../../../utils/common";
import {getUserService} from "../../../module/user/user";
import {getImageService} from "../../../module/common/image";
import ValidateUtil from "../../../utils/ValidateUtil";

var WxParse = require('../../../wxParse/wxParse.js');
var config = {
    /**
     * 页面的初始数据
     */
    data: {
        list: [],
        selectedCouponType: null,
        integralTotal: 0,
        imageIns: null,
        richtext: {
            hidden: true
        },
        richtextcontent: '<div>text</div>',
        style:'two'
    },
    exchangeCoupon: function (e) {
        const index = e.currentTarget.dataset.num;
        if (this.data.list[index] == null) {
            Toast.tip("请选择要兑换的优惠券");
            return;
        }
        const selectedCouponType = this.data.list[index];

        const account = UserUtil.getAccount();
        const accountBean = UserUtil.getAccountBean();
        const integral = this.data.integralTotal;
        const number = parseFloat(selectedCouponType.number);
        if (integral < number) {
            Toast.tip('积分不足,不能兑换优惠券');
            return;
        }
        const cardId = accountBean.getCardId();
        this.loading();
        getCouponService().integralExchangeCoupon({
            cardId: cardId,
            id: selectedCouponType.id
        }, (response) => {
            this.closeLoading();
            Toast.tip(response.getMsg());
            if (response.isSuccess()) {
                accountBean.reduceIntegral(number);
            }
        });
    },
    selectCouponType: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.list;
        let selectedCouponType = null;
        for (let i = 0; i < list.length; i++) {
            list[i].selected = false;
            if (i == index) {
                list[i].selected = true;
                selectedCouponType = list[i];
            }
        }
        this.setData({list: list, selectedCouponType: selectedCouponType});
    },
    //查看优惠券详情
    lookCouponDetail: function (e) {
        const index = e.currentTarget.dataset.index;
        const list = this.data.list;
        // console.log(list[index]);
        if (list[index]) {
            const content = list[index]['content'];
            WxParse.wxParse('richtextcontent', 'html', content, this, 5);
            this.setData({richtext: {hidden: false}});
        }
    },
    hideRichText: function () {
        this.setData({richtext: {hidden: true}});
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);

        this.loading();
        const imageIns = getImageService("").setImageField('url').imageEmptyNoShow();
        this.data.imageIns = imageIns;

        getCouponService().fetchExchangeCouponTypeList((response) => {
            this.closeLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const list = response.getData();
            if (list.length > 0) {
                for (let i = 0; i < list.length; i++) {
                    const obj = list[i];
                    obj.selected = i == 0;
                    obj.bgImage = '';
                    if (obj.amount <= 99) {
                        obj.bgImage = '/images/bg_01@2x.png';
                    } else {
                        obj.bgImage = '/images/bg_02@2x.png';
                    }
                    imageIns.format(obj);
                    if (i == 0) {
                        this.setData({selectedCouponType: obj});
                    }
                }
            }
            this.setData({list: list});
        });
        const mobile = UserUtil.getUserBean().getMobile();
        getUserService().fetchUserInErp({mobile: mobile}, (response) => {
            if (response.isSuccess()) {
                const bean = response.getData();
                this.setData({integralTotal: bean.getIntegral()});
            }
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
