// pages/member/cardtype/cardtype.js
import {
    getCardTypeInfnoBean,
    getCardTypeService
} from "../../../module/system/cardtype";
import {
    Toast
} from "../../../utils/Toast";
import pageConfig from "../../../utils/page";
import {
    getPayService
} from "../../../module/common/pay";
import {
    Util
} from "../../../utils/common";
import ValidateUtil from "../../../utils/ValidateUtil";
import {
    getCardTypePayBean,
    getUserService
} from "../../../module/user/user";
import {
    UserUtil
} from "../../../utils/UserUtil";

var config = {
    /**
     * 页面的初始数据
     */
    data: {
        cardTypeList: [],
        selectCardType: null,
        nowCardTypeName: '',
        config: null,
    },
    buyCardType: function () {
        if (this.data.selectCardType == null) {
            Toast.tip("请选择卡类");
            return;
        }
        const configIns = this.data.config;
        if (configIns == null) {
            Toast.tip("配置信息无效");
            return;
        }
        const app = getApp();
        const account = app.storage.getAccount();
        const userInfo = app.storage.getUserInfo();
        const mobile = Util.get(userInfo, 'phone', '');
        const openid = Util.get(userInfo, 'openid', '');
        const cardType = Util.get(account, 'card_type', '');
        if (!ValidateUtil.isMobile(mobile)) {
            Toast.tip("用户信息错误，请退出小程序重新进入");
            return;
        }
        const cardBean = getCardTypeInfnoBean(this.data.selectCardType);
        console.log(cardType,cardBean.getCardTypeName());
        if (configIns.cardTypeEqual(cardType, cardBean.getCardTypeName())) {
            Toast.tip("选择的卡类不能与当前卡类相同");
            return;
        }
        const changeUserCardType = function () {
            getUserService().upgradeCardTypeByPay({
                mobile: mobile,
                cardType: cardBean.getCardTypeName()
            }, (response) => {
                Toast.tip(response.getMsg(), {
                    end: function () {
                        if (response.isSuccess()) {
                            const app = getApp();
                            const account = app.storage.getAccount();
                            if (typeof account == 'object') {
                                account.card_type = cardBean.getCardTypeName();
                                app.storage.setAccount(account);
                                wx.redirectTo({
                                    url: '/pages/index/index?login=1'
                                })
                            }
                        }
                    }
                });
            });
        };

        getPayService().buyCardPreparePay({
            mobile: mobile,
            cardCode: cardBean.getCardCode(),
            amount: cardBean.getAmount(),
            cardType: cardBean.getCardTypeName(),
            openid: openid
        }, (response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const bean = response.getData();
            if (!bean.isPay()) {
                //没有支付信息，开始支付
                getPayService().wxPay(response.getData(), (response) => {
                    Toast.tip(response.getMsg());
                    if (response.isSuccess()) {
                        changeUserCardType();
                    }
                })
            } else {
                //支付过后直接处理支付后的逻辑
                changeUserCardType();
            }

        });
    },
    selectCardType: function (index) {
        const cardTypeList = this.data.cardTypeList;
        let selectCardType = null;
        for (let i = 0; i < cardTypeList.length; i++) {
            cardTypeList[i].selected = false;
            if (i == index) {
                cardTypeList[i].selected = true;
                selectCardType = cardTypeList[i];
            }
        }
        this.setData({
            cardTypeList: cardTypeList,
            selectCardType: selectCardType
        });

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var p = this;
        this.customOnload(options);
        this.loading();
        getCardTypeService().fetchCardTypeConfig((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const configIns = response.getData();
            const cardTypeList = configIns.getCardTypeList(false, false);
            if (cardTypeList.length == 0) {
                Toast.tip("未设置卡类信息");
                return;
            }

            const uid = UserUtil.getAccountBean().getUserId();
            getCardTypeService().fetchUserEnableDisplayCardTypeList({
                uid: uid
            }, cardTypeList, (response) => {
                this.closeLoading();
                const cardTypeList = response.getData();
                for (let i = 0; i < cardTypeList.length; i++) {
                    cardTypeList[i].selected = false;
                }
                let selectCardType = null;
                if (cardTypeList.length > 0) {
                    cardTypeList[0].selected = true;
                    selectCardType = cardTypeList[0];
                }
                this.setData({
                    cardTypeList: cardTypeList,
                    selectCardType: selectCardType,
                    config: configIns
                });
                console.log(cardTypeList)
            });
        });

        const app = getApp();
        const account = app.storage.getAccount();
        const cardTypeName = Util.get(account, 'card_type', '');
        this.setData({
            nowCardTypeName: cardTypeName,
            currents: 0
        });
    },
    //滑动卡卷时候出发
    cardbind: function (e) {
        var p = this;
        console.log(e.detail.current);
        this.selectCardType(e.detail.current);
        p.setData({
            currents: e.detail.current
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {
    ...config,
    ...pageConfig
};
Page(options);
