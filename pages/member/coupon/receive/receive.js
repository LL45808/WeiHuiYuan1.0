import pageConfig from "../../../../utils/page";
import {getCouponService} from "../../../../module/user/coupon";
import PageUtil from "../../../../utils/PageUtil";
import {UserUtil} from "../../../../utils/UserUtil";
import {Toast} from "../../../../utils/Toast";
import {Util} from "../../../../utils/common";
import ValidateUtil from "../../../../utils/ValidateUtil";
import {getSessionBean, getSessionService} from "../../../../module/common/session";

var config = {
    /**
     * 页面的初始数据
     */
    data: {
        topicId: 0,
        coupon: null,
        couponValid: false
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.customOnload(options);
        const topicId = Util.intval(Util.get(options, 'topicId', 0));
        if (topicId <= 0) {
            Toast.tip('参数无效');
            return;
        }
        const post = {
            topicId: topicId,
        };
        this.setData({topicId: topicId});
        this.loading();
        getCouponService().fetchErpSendCouponInfo(post, (response) => {
            this.closeLoading();
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
            const coupon = response.getData();
            this.setData({coupon: coupon, couponValid: true});
        });
    },
    receive: function () {
        const topicId = this.data.topicId;
        const mobile = getSessionBean().getMobile();
        if (!ValidateUtil.isMobile(mobile)) {
            Toast.tip('用户信息无效');
            return;
        }
        this.loading();
        getCouponService().receiveErpSendCoupon({topicId: topicId, mobile: mobile}, (response) => {
            this.closeLoading();
            Toast.tip(response.getMsg(), {
                end: () => {
                    if (response.isSuccess()) {
                        if (PageUtil.hasPage('index')) {
                            PageUtil.redirectTo('couponlist');
                        } else {
                            PageUtil.redirectTo('index');
                        }
                    }
                }
            })
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {
    ...config,
    ...pageConfig
};
Page(options);
