// pages/member/cardtype/cardtype.js
import {getCardTypeInfnoBean, getCardTypeService} from "../../../module/system/cardtype";
import {Toast} from "../../../utils/Toast";
import pageConfig from "../../../utils/page";
import {getPayService} from "../../../module/common/pay";
import {Util} from "../../../utils/common";
import ValidateUtil from "../../../utils/ValidateUtil";
import {getCardTypePayBean, getUserService} from "../../../module/user/user";
import {getCommonService} from "../../../module/common/common";

var config = {
    /**
     * 页面的初始数据
     */
    data: {
        mobileList: [],
        item:{address:''}
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        this.customOnload(options);
        getCommonService().fetchCustomerService((response) => {
            if (response.isSuccess()) {
                const mobileList = response.getData().getMobileList();
                const item=response.getData().getData();
                this.setData({mobileList: mobileList,item:item});
            }
        });
    },
    phoneCall: function (event) {
        var tel = event.currentTarget.dataset.tel;
        if (!ValidateUtil.isEmpty(tel)) {
            wx.makePhoneCall({
                phoneNumber: tel,
                success: function () {
                    console.log("拨打电话成功！")
                },
                fail: function () {
                    console.log("拨打电话失败！")
                }
            })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.customOnready();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
};
var options = {...config, ...pageConfig};
Page(options);
