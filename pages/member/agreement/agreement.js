var app = getApp()
Page({
    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var page = this;
        page.agreement()
    },
    /**
     * 充值协议接口获取
     */
    agreement: function () {
        var that = this
        var data = {
        }
        app.crmApi(
            app.apiService.crm.rechargeAgreement,
            data,
            that.agreementSuccess,
            that.argeementFail
        )
    },
    /**
     * 充值协议接口获取 成功
     */
    agreementSuccess: function (res) {
        var that = this;
        var agreement_txt = res.data.data
        if (res.data.ret = 200) {
            that.setData({
              agreementTxt: agreement_txt.wxpro_crm_register
            })
        }
    },
    /**
     * 充值协议接口获取 失败
     */
    argeementFail: function () {

    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})