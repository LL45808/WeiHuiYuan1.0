// pages/test/pay/pay.js
import {getPayService} from "../../../module/common/pay";
import {Toast} from "../../../utils/Toast";
import {Util} from "../../../utils/common";
import {getUserService} from "../../../module/user/user";
import {ToolUtil} from "../../../utils/ToolUtil";
import {getConfigService} from "../../../module/common/config";
import {UserUtil} from "../../../utils/UserUtil";
import {getSessionService} from "../../../module/common/session";
import RequestUtil from "../../../module/api/request";
import {ApiConst} from "../../../module/api/const";

Page({

    /**
     * 页面的初始数据
     */
    data: {
        card: {
            list: [1, 2, 3, 4, 5],
            a: 1,
            b: 2
        },
        url: ''
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (e) {
        var code = "123456";
        var phone = "18750198649";
        if (e.from == 'button') {
            var path = 'pages/coupon/shareindex/shareindex?couponCode=' + code + '&fromSource=donateReceiveCoupon';
            console.log("path:" + path + "   phone:" + phone);
            var obj = {
                title: '您有一张优惠券待领取！！！',
                path: path,
                imageUrl: '/images/liqu_bg@2x.png',
                success: function (res) {
                    console.log("coupon success");
                },
                fail: function (res) {
                    // 转发失败
                    console.log('coupon fail')
                },
                complete: function () {
                    console.log('coupon complete')

                }
            }
            console.log(obj);
            return obj;
        } else {
            console.log("------1");
            return {};
        }
    },
    //准备支付
    preparePay: function () {

        // Util.updateComponentData2(this, {card: {a: 'a', b: 'b', c: 'c'}});
        // console.log(this.data.card);
        /*        getPayService().buyCardPreparePay({
                    mobile: '18750198649',
                    amount: 0.01,
                    openid: 'oieCJ5ajB7QXB2G1OyRuNz4P9auc',
                    cardType: '95折会员卡',
                    cardCode: '1',
                }, (response) => {
                    if (!response.isSuccess()) {
                        Toast.tip(response.getMsg());
                    } else {
                        getPayService().wxPay(response.getData(), () => {

                        })
                    }
                });*/
    },
    webcard: function (e) {
        if (e.detail.errMsg == "getUserInfo:ok") {
            const configService = getConfigService();
            let wxCardResponse = null;
            let userInfoResponse = null;
            ToolUtil.thenSequence((complete) => {
                getUserService().fetchWxUserInfo((response) => {
                    if (!response.isSuccess()) {
                        Toast.tip(response.getMsg());
                        return;
                    }
                    userInfoResponse = response;
                    complete();
                });
            }).then((complete) => {
                configService.fetchWxCardConfig((response) => {
                    if (!response.isSuccess()) {
                        Toast.tip(response.getMsg());
                        return;
                    }
                    wxCardResponse = response;
                    const bean = response.getData();
                    const wxCardUrl = bean.getWxCardUrl();
                    console.log(wxCardUrl);
                    this.setData({url: wxCardUrl});
                    /* const extraData = {
                         encrypt_card_id: bean.getEncryptCardId(),
                         biz: bean.getBiz(),
                         outer_str: bean.getOuterStr(),
                     };
                     console.log("extraData", extraData);
                     wx.navigateToMiniProgram({
                         appId: 'wxeb490c6f9b154ef9',
                         extraData: extraData,
                         success: (res) => {
                             console.log("success");
                         },
                         fail: (res) => {
                             console.log("fail");
                         }
                     });*/
                });
            }).then((complete) => {

            }).execute();

        }
    },
    accredit: function (e) {
        if (e.detail.errMsg == "getUserInfo:ok") {
            const configService = getConfigService();
            let wxCardResponse = null;
            let userInfoResponse = null;
            ToolUtil.thenSequence((complete) => {
                getUserService().fetchWxUserInfo((response) => {
                    if (!response.isSuccess()) {
                        Toast.tip(response.getMsg());
                        return;
                    }
                    userInfoResponse = response;
                    complete();
                });
            }).then((complete) => {
                configService.fetchWxCardConfig((response) => {
                    if (!response.isSuccess()) {
                        Toast.tip(response.getMsg());
                        return;
                    }
                    wxCardResponse = response;
                    const bean = response.getData();

                    const extraData = {
                        encrypt_card_id: bean.getEncryptCardId(),
                        biz: bean.getBiz(),
                        outer_str: bean.getOuterStr(),
                    };
                    console.log("extraData", extraData);
                    wx.navigateToMiniProgram({
                        appId: 'wxeb490c6f9b154ef9',
                        extraData: extraData,
                        success: (res) => {
                            console.log("success");
                        },
                        fail: (res) => {
                            console.log("fail");
                        }
                    });
                });
            }).then((complete) => {

            }).execute();

        }
    },
    register: function () {
        const app = getApp();
        let userInfoResponse = null;
        ToolUtil.thenSequence((complete) => {
            getUserService().fetchWxUserInfo((response) => {
                if (!response.isSuccess()) {
                    Toast.tip(response.getMsg());
                    return;
                }
                userInfoResponse = response;
                complete();
            });
        }).then((complete) => {
            const wxUserBean = userInfoResponse.getData();
            const unionId = wxUserBean.getUnionId();
            console.log("unionId:" + unionId);
            getUserService().fetchCardSubmitUserInfo({unionId: unionId}, (response) => {
                if (!response.isSuccess()) {
                    Toast.tip(response.getMsg());
                    return;
                }
                const bean = response.getData();
                //来源信息设置
                const post = {};
                var data = {
                    code: '',
                    phone: bean.getMobile(),
                    member_name: bean.getName(),
                    birthday: bean.getBirthday(),
                    token: UserUtil.getToken(),
                    session_key: UserUtil.getSessionKey(),
                    encryptedData: wxUserBean.getEncryptedData(),
                    iv: wxUserBean.getIv(),
                    is_code: 2,//不使用验证码
                    recommendMobile: '',
                    recommendCardId: '',
                    lat: '',
                    lng: '',
                    post: JSON.stringify(post)
                };

                if (app.share_code) {
                    data.share_code = app.share_code
                }
                console.log('data', data);
                app.crmApi(
                    app.apiService.crm.bindPhone,
                    data
                )
            });
        }).execute();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        getSessionService().fetchSession((response) => {
            if (!response.isSuccess()) {
                Toast.tip(response.getMsg());
                return;
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        wx.hideShareMenu();
        console.log(wx.getSystemInfo);
        wx.getSystemInfo({
            success: function (res) {
                console.log("res", res);
                var version = res.SDKVersion;
                version = version.replace(/\./g, "")
                console.log(version)
                if (parseInt(version) < 120) {// 小于1.2.0的版本

                }
            }
        });
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    }
})
