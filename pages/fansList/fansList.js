var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        page: 1, //下一个页码
        page_count: 0, //总页码数
        pageSize: 5, //每页显示条数
        nomore: false,//已加载所有
        fansItem:true,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
    * 粉丝列表接口获取
    */
    fansList: function (func, page, pageSize) {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            page: page,
            pageSize: pageSize
        }
        var extraParams = {
            func: func, page: page, pageSize: pageSize
        }
        wx.showToast({
            title: '加载中',
            icon: 'loading',
            duration: 1000
        })
        app.crmApi(
            app.apiService.crm.getFansList,
            data,
            that.fansListSuccess,
            that.fansListFail,
            extraParams
        )
    },

    /** 
     * 粉丝列表接口获取 成功
     */
    fansListSuccess: function (res, param) {
        wx.hideToast();
        var that = this;
        var func = param.func;
        var page = param.page;
        var pageSize = param.pageSize;
        var info = res.data.data
        // 总条数为0或者未定义时
        if (info.total == 0 || !info.total) {
            that.setData({
                fansItem: false,
            })
            return
        }
        // 计算出的总页码，Math.ceil向上取整
        var page_count = Math.ceil(info.total / pageSize)
        var newData = res.data.data.list
        // 页码数大于计算出的页码数和没有数据的情况
        if (!newData || page > page_count) {
            that.setData({
                nomore: true
            })
            return
        }
        // 接口成功的时候重新赋值
        if (res.data.ret = 200) {
            that.setData({
                page: page,
                page_count: page_count
            })
            // 加载
            if (func == 3) {
                var oldData = that.data.fansItem
                that.setData({
                    fansItem: oldData.concat(newData)
                })
            } else {
                wx.hideNavigationBarLoading() //完成停止加载
                wx.stopPullDownRefresh() //停止下拉刷新
                that.setData({
                    fansItem: newData
                })
            }
        }
    },
    /**
     * 粉丝列表接口获取 失败
     */
    fansListFail: function () {
      var that = this;
      that.setData({
        fansItem: false,
      })
      wx.hideToast();
      wx.showToast({
          title: '加载出错！',
          image: '/images/wrong-load.png'
      })
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this
        that.fansList(1, that.data.page, that.data.pageSize);
       // this.fansList();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        var that = this;
        that.fansList(2, 1, that.data.pageSize);
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        var that = this;
        var page = that.data.page + 1;
        that.fansList(3, page, that.data.pageSize);
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})