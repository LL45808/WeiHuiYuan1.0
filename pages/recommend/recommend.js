var app = getApp()
var fun_md5 = require('../../utils/md5.js')
const page_code = 'wxpro_crm_recommend'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        page_configs: {
            recommend_bg: '',
            recommend_tit: '',
            recommend_img: ''
        },
        is_switch: true,//按钮状态,
        userInfo:null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.page_config();
        var userInfo=null;
        try{
            userInfo=app.storage.getUserInfo();
        }catch(e){
            userInfo=null;
        }
        if(userInfo==null){
            wx.showModal({
                title:'检测到您的小程序在其它地方登录,请删除小程序后重新登录',
                duration:3000,
                success:function(res){
                    if(res.confirm){
                        wx.redirectTo({
                            url: '/pages/index/index'
                        })
                    }
                }
            });
            return;
        }else{
            this.setData({userInfo:userInfo});
        }
    },

    /**
     * 粉丝列表链接跳转
     */
    fansListTap:function(){
        var that = this;
        if(!that.data.is_switch){
            return
        }
        else{
            wx.navigateTo({
                url: '/pages/fansList/fansList',
            })
            that.setData({
                is_switch:false
            })
        }
    },

    /**
     * 可配置文件
     */
    page_config: function () {
        var that = this;
        app.crmApi(
            app.apiService.crm.pageConfig,
            { page_code: page_code },
            that.pageConfigSuccess
        )
    },
    pageConfigSuccess: function (res) {
        var that = this;
        var config = res.data.data[page_code].configs;
        var tmpConfig = that.data.page_configs
        for (var i in config) {
            tmpConfig[i] = config[i].value_type == 2 ? config[i].value_text : config[i].value
        }
        that.setData({
            page_configs: tmpConfig
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
       var that = this;
       that.setData({
           is_switch:true
       })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },
    shareTap:function(){
    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        //
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        var that = this;
        if (res.from === 'button') {
            // 来自页面内转发按钮
            wx.showShareMenu({
                withShareTicket: true
            })
        }
        var timestamp = String(Date.parse(new Date()));//时间戳
        var uid = String(app.storage.getAccount().user_id);//用户ID
        var str_md5 = fun_md5.hex_md5(timestamp + uid);

        var timestamp = String(Date.parse(new Date()));//时间戳

        var userInfo=this.data.userInfo;
        var mobile = userInfo.phone;//用户ID
        //console.log('userInfo',userInfo);
        var account=app.storage.getAccount();

        var cardId=account.card_id;
        app.storage.setShareCode(str_md5);
        let resForGrpInfo = {
            token: app.storage.getAuth().token,
            openid: app.storage.getAuth().openid,
            share_code: app.storage.getShareCode()
        }
        that.share(resForGrpInfo);
        return {
            title: that.data.page_configs.recommend_tit,
            imageUrl: that.data.page_configs.recommend_bg,
            path: '/pages/index/index?share_code=' + str_md5+"&mobile="+mobile+"&cardId="+cardId,
            success: function (res) {
                /**
                 * 当用户将转发信息发送到群聊，获取用户分享的 shareTickets
                 */
                if (res.shareTickets && res.shareTickets[0]) {
                    wx.getShareInfo({
                        shareTicket: res.shareTickets[0],
                        success: resForGrpInfo => {
                            resForGrpInfo.shareTickets = res.shareTickets[0]
                            resForGrpInfo.share_code = app.storage.getShareCode()
                            that.share(resForGrpInfo)
                        }
                    })
                } else {

                }
            },
            fail: function (res) {
                // 转发失败
            },
            complete:function(){
            }
        }
    },

    /**
     * 转发接口获取
     */
    share: function (e) {
        var that = this;
        var data = {
            token: app.storage.getAuth().token,
            shareTicket: e.shareTickets,
            encodeData: e.encryptedData,
            openid: e.openid,
            iv: e.iv,
            'module': '',
            remark: '',
            sessionKey: app.storage.getAuth().session_key,
            share_code: e.share_code
        }
        app.crmApi(
            app.apiService.crm.userShare,
            data,
            that.shareSucccess,
            that.shareFail
        )
    },

    /**
     * 转发接口获取  成功
     */
    shareSucccess: function (res) {
    },

    /**
    * 转发接口获取  失败
    */
    shareFail: function (res) {
    },
})
