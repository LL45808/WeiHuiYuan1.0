import {UserUtil} from "../utils/UserUtil";

const ApiUtil = {};
const app = getApp();
ApiUtil.fetchErpUserInfo = function (callback) {
    app.crmApi(
        app.apiService.crm.getErpUserInfo,
        {card_id: UserUtil.getCardId()},
        (res) => {
            callback(true, res.data);
        }, (res) => {
            callback(false);
        });
};

export default ApiUtil;
