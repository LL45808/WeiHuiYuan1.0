import {UrlUtil} from "../utils/UrlUtil";

const config = {
    gameConfig: {
        gameApiUrl: 'https://gameservices.fwo2o.net',
        gameFrontUrl: 'https://wx8.bzqch.cn'
    }
};

const GameConfig = {
    GAME_API_CONST: {
        FETCH_USER_TYPE: 'Login/LoginGame'
    },
    getGameApiUrl(url) {
        return config.gameConfig.gameApiUrl + "/" + url;
    },
    getGameUrl(gameBean, usertype) {
        let gameUrl = config.gameConfig.gameFrontUrl + "/" + gameBean.getSuffixUrl() + "?clientid=" + UrlUtil.getUniacid() + "&usertype=" + usertype;
        return gameUrl;
    }
};

export {GameConfig};
