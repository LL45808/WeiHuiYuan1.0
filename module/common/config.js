import {ToolUtil} from "../../utils/ToolUtil";
import RequestUtil from "../api/request";
import {ApiConst, StorageConst} from "../api/const";
import {UserUtil} from "../../utils/UserUtil";
import {Util} from "../../utils/common";
import {CustomStorage} from "./StorageUtil";
import ValidateUtil from "../../utils/ValidateUtil";

class WxCardConfig {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    hasConfig() {
        return this.data.bool('has_wx_card_config', false);
    }

    getEncryptCardId() {
        return this.data.get('wx_card_config.encrypt_card_id', '');
    }

    getOuterStr() {
        return this.data.get('wx_card_config.outer_str', '');
    }

    getBiz() {
        return this.data.get('wx_card_config.biz', '');
    }

    getCardId() {
        return this.data.get('wx_card_config.card_id', '');
    }
    getWxCardUrl(){
        return this.data.get('wx_card_config.wx_card_url','');
    }
}

class WxCardSignBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getCardExt() {
        return this.data.get('jsonstr', '');
    }
}

class SystemConfigStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.SYSTEM_CONFIG_KEY);
    }
}

class SystemConfigBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getPageConfig() {
        return this.data.get('wxpro_crm_index_member.configs', []);
    }

    getCardUrl() {
        return this.data.get('card_url', '');
    }

    //是否支持余额
    isSupportBalance(){
        return this.data.bool('config.support_balance',true);
    }
    //是否支持优惠券
    isSupportCoupon(){
        return this.data.bool('config.support_coupon',true);
    }
    //是否支持积分
    isSupportIntegral(){
        return this.data.bool('config.support_integral',true);
    }
}

const SystemConfigStorageUtil = new SystemConfigStorage();

class IndexConfigStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.INDEX_CONFIG_KEY);
    }
}

class IndexConfigBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = data;
    }

    getNavList() {
        return ValidateUtil.isArray(this.data) ? this.data : [];
    }
}

const IndexConfigStorageUtil = new IndexConfigStorage();

class ConfigService {
    fetchWxCardConfig(callback) {
        const post = {};
        RequestUtil.commonRequest(ApiConst.FETCH_CONFIG_WXCARD_INFO, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "微信会员卡配置信息获取失败", (result, response) => {
                const bean = new WxCardConfig(result);
                return bean;
            });
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

    fetchWxCardSign(data, callback) {
        const post = {openid: data.openid, cardId: data.cardId, code: data.code};
        RequestUtil.commonRequest(ApiConst.FETCH_WXCARD_SIGN, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "微信会员卡签名信息获取失败", (result, response) => {
                console.log("result", result);
                const bean = new WxCardSignBean(result);
                return bean;
            });
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

    fetchSystemConfig(callback) {
        const app = getApp();
        const response = ToolUtil.getApiResponse();
        if (!SystemConfigStorageUtil.timeout()) {
            response.setSuccess(true).setData(new SystemConfigBean(SystemConfigStorageUtil.get()));
            callback(response);
            return;
        }
        app.crmApi(
            app.apiService.crm.pageConfig, {},
            res => {
                if (res.data.ret == 200) {
                    var showConfig = res.data.data;
                    SystemConfigStorageUtil.set(showConfig);
                    response.setData(new SystemConfigBean(showConfig)).setSuccess(true);
                } else {
                    response.setMsg("系统配置获取失败").setSuccess(false);
                }
                callback(response);
            },
            msg => {
                response.setSuccess(false).setMsg('系统配置获取失败');
                callback(response);
            }
        )
    }

    fetchIndexNav(callback) {
        var app = getApp();
        var response = ToolUtil.getApiResponse();
        if (!IndexConfigStorageUtil.timeout()) {
            response.setSuccess(true).setData(new IndexConfigBean(IndexConfigStorageUtil.get()));
            callback(response);
            return;
        }
        var data = {
            token: "none",
        };
        app.crmApi(
            app.apiService.crm.indexNav,
            data,
            (res) => {
                IndexConfigStorageUtil.clear();
                if (res.data.ret == 200) {
                    var list = res.data.data;
                    for (let i = 0; i < list.length; i++) {
                        const list2 = list[i];
                        for (let j = 0; j < list2.length; j++) {
                            const obj = list2[j];
                            const url = obj.url;
                            obj.isWeb = String(url).substring(0, 4) == 'http';
                        }
                    }                  
                    IndexConfigStorageUtil.set(list);
                    response.setSuccess(true).setData(new IndexConfigBean(list));
                } else {
                    response.setSuccess(false).setMsg('导航信息获取失败');
                }
                callback(response);
            },
            () => {
                IndexConfigStorageUtil.clear();
                response.setSuccess(false).setMsg("导航信息获取失败");
                callback(response);
            }
        )
    }
}


export function getConfigService() {
    return new ConfigService();
}

export {SystemConfigStorageUtil, IndexConfigStorageUtil};
