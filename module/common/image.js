import ValidateUtil from "../../utils/ValidateUtil";

class ImageService {
    defaultImage = '';
    imageField = '';
    defaultImageMap = {};
    _imageEmptyNoShow = false;

    constructor(defaultImage) {
        this.defaultImage = defaultImage;
    }

    addDefaultImage(name, value) {
        this.defaultImageMap[name] = value;
        return this;
    }

    //图片为空的时候不显示图片
    imageEmptyNoShow() {
        this._imageEmptyNoShow = true;
        return this;
    }

    setImageField(field) {
        this.imageField = field;
        return this;
    }

    getDefaultImage() {
        return this.defaultImage;
    }

    useDefaultImage(obj) {
        obj[this.imageField] = this.defaultImage;
    }

    format(obj, def) {
        if (def == 'undefined') {
            def = this.defaultImage;
        }
        const image = obj[this.imageField];
        if (ValidateUtil.isEmpty(image)) {
            obj[this.imageField] = def;
        }
        if (this._imageEmptyNoShow) {
            obj.showImage = !ValidateUtil.isEmpty(obj[this.imageField]);
        } else {
            obj.showImage = true;
        }
    }

    format2(obj, name) {
        if (this.defaultImageMap[name]) {
            const image = this.defaultImageMap[name];
            if (ValidateUtil.isEmpty(image)) {
                obj[this.imageField] = this.defaultImage;
            }
        } else {
            this.format(obj);
        }
    }
}

export function getImageService(defaultImage) {
    return new ImageService(defaultImage)
};
