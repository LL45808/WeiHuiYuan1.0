import {ToolUtil} from "../../utils/ToolUtil";
import {Util} from "../../utils/common";

class WxUserInfoBean {

    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getPortrait() {
        return this.data.get('avatarUrl', '');
    }

    getNickName() {
        return this.data.get('nickName', '');
    }

    getIv() {
        return this.data.get('iv', '');
    }

    getEncryptedData() {
        return this.data.get('encryptedData', '');
    }

    getSex() {
        return this.data.get('gender', 0);
    }
}

class WxService {
    fetchUserInfo(callback) {
        wx.getUserInfo({
            lang: 'zh_CN',
            success: function (res) {
                if (res.errMsg == 'getUserInfo:ok') {
                    const userInfo = res.userInfo;
                    userInfo.encryptedData = res.encryptedData;
                    userInfo.iv = res.iv;
                    const response = ToolUtil.getApiResponse().setSuccess(true).setData(new WxUserInfoBean(userInfo));
                    callback(response);
                } else {
                    const response = ToolUtil.getApiResponse().setSuccess(false).setMsg('获取微信用户信息失败');
                    callback(response);
                }
            },
            fail: function (res) {
                const response = ToolUtil.getApiResponse().setSuccess(false).setMsg('获取微信用户信息失败');
                callback(response);
            }
        })
    }
}

const wxServiceIns = new WxService();

export function getWxService() {
    return wxServiceIns;
}
