import {ApiConst, StorageConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import RequestUtil from "../api/request";
import ValidateUtil from "../../utils/ValidateUtil";
import CommonUtil from "../../utils/lon_lat";


class ShopService {
    fetchShopList(data, callback) {
        const post = {shopName: data.shopName};
        RequestUtil.wxappRequest(ApiConst.FETCHE_SHOP_LIST, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "门店获取失败");
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

    fetchShopListWithDistanceAsc(data, callback) {
        const post = {shopName: data.shopName};
        const origin = {lat: data.lat, lng: data.lng};
        this.fetchShopList(data, (response) => {
            if (!response.isSuccess()) {
                callback(response);
                return;
            }
            const result = response.getData();
            const list = result.list;
            for (let i = 0; i < list.length; i++) {
                var obj = list[i];
                let dis = 0;
                let compareFieldDis = 0;
                if (ValidateUtil.isEmpty(obj.lat) || ValidateUtil.isEmpty(obj.lon)) {
                    dis = 0;
                    compareFieldDis = 999999;
                } else {
                    const target = {lat: obj.lat, lng: obj.lon};
                    dis = CommonUtil.getDistance(origin, target);
                    compareFieldDis = dis;
                }
                obj.distance = dis;
                obj.compareFieldDis = compareFieldDis;
            }
            result.list = ToolUtil.resort(list, 'compareFieldDis', "asc");
            callback(response);
        });
    }
}

const shopIns = new ShopService();

export function getShopService() {
    return shopIns;
}
