import RequestUtil from "../api/request";
import {ApiConst, StorageConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import {CustomStorage} from "./StorageUtil";
import {Util} from "../../utils/common";

class OpenidServiceStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.OPENID_SERVICE_KEY);
    }
}

const openIdStorageUtil = new OpenidServiceStorage();

class OpenidService {

    fetchRelationOpenId(openid, callback) {
        const post = {
            type: 'crm',
            openid: openid
        };
        RequestUtil.commonRequest(ApiConst.FETCH_CRM_RELATION_USER_INFO, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '', (result, response) => {
                return response;
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse('请求失败'));
        });
    }

    /**
     * 该函数带有缓存的查询用户是否绑定公众号，如果没有绑定则会一直查询，如果已经成功则不需要查询
     * @param openid
     * @param callback
     */
    fetchRelationOpenId2(openid, callback) {
        const response = ToolUtil.getApiResponse();
        if (!openIdStorageUtil.timeout()) {
            const data = openIdStorageUtil.get();
            const ObjUtil = Util.getObjUtil();
            const succ = ObjUtil.get(data, 'data.status', 'error');
            if (succ == 'success') {
                console.log("绑定");
                //如果已经绑定，没有必要继续绑定
                response.setSuccess(true);
                return;
            } else {
                console.log("未绑定");
            }
            //如果没有绑定继续重新查询
        }

        this.fetchRelationOpenId(openid, (response) => {
            if (response.isSuccess()) {
                const data = response.getContent();
                openIdStorageUtil.set(data);
                openIdStorageUtil.setTimeout(3600 * 24 * 1000);
            } else {
                openIdStorageUtil.clear();
            }
            callback(response);
        });
    }
}


export function getOpenidService() {
    return new OpenidService();
}
