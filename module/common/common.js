import {ApiConst, StorageConst} from "../api/const";
import {CustomStorage} from "./StorageUtil";
import {ToolUtil} from "../../utils/ToolUtil";
import RequestUtil from "../api/request";
import {CardTypeConfig} from "../system/cardtype";
import {UserUtil} from "../../utils/UserUtil";
import {Util} from "../../utils/common";

class CustomerServiceStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.CUSTOMER_SERVICE_KEY);
    }
}


class CustomerServiceBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    _data = null;

    constructor(data) {
        this._data = data;
        this.data = Util.objectExt(data);
    }

    getMobileList() {
        const list = this.data.get('mobile_list', []);
        if (Util.isArray(list)) {
            return list;
        } else {
            return [];
        }
    }

    getData() {
        return this._data;
    }
}

const CustomerServiceUtil = new CustomerServiceStorage();

class CommonService {

    /**
     * 获取客服服务信息
     * @param callback
     */
    fetchCustomerService(callback) {
        if (!CustomerServiceUtil.timeout()) {
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setData(new CustomerServiceBean(CustomerServiceUtil.get()));
            callback(response);
            return;
        }
        RequestUtil.commonRequest(ApiConst.FETCH_CUSTOMER_SERVICE, {
            token: UserUtil.getToken()
        }, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '客服服务获取失败', (result) => {
                CustomerServiceUtil.set(result);
                return new CustomerServiceBean(result);
            });
            if (!response.isSuccess()) {
                CustomerServiceUtil.clear();
            }
            callback(response);
        }, (res) => {
            CustomerServiceUtil.clear();
            callback(ToolUtil.errorResponse('请求失败'));
        });

    }
}

export function getCommonService() {
    return new CommonService();
}
