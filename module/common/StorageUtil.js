import {TimeUtil} from "../../utils/TimeUtil";
import {StorageConst} from "../api/const";

const ExpireUtil = {
    setExpireTime: function (data, interval) {
        if (typeof data != 'object') {
            throw new Error("invoke setExpireTime params type error");
        }
        if (!interval) {
            interval = 3600000 * 2;
        }
        // console.log("interval:" + interval);
        data.expire = TimeUtil.getNowTime() + interval;
    },
    isExpire: function (data) {
        if (typeof data != 'object') {
            throw new Error("invoke setExpireTime params type error");
        }
        if (!data['expire']) {
            return true;
        }
        const expire = data['expire'];
        return expire <= TimeUtil.getNowTime();
    },
    setExpire: function (data) {
        if (typeof data != 'object') {
            throw new Error("invoke setExpireTime params type error");
        }
        data.expire = TimeUtil.getNowTime() - 1000;
    }
};

class CustomStorage {
    key = "default_key";

    constructor() {

    }

    setKey(key) {
        this.key = key;
        return this;
    }

    /**
     * 第一次设置，包含超时设置
     * @param value
     * @returns {CustomStorage}
     */
    set(value) {
        var obj = {data: value};
        var rawObj = this.getRaw();
        if (rawObj == null) {
            ExpireUtil.setExpireTime(obj, 3600000 * 8);
        } else {
            if (!rawObj.expire) {
                ExpireUtil.setExpireTime(obj, 3600000 * 8);
            }else{
                obj.expire=rawObj.expire;
            }
        }
        wx.setStorageSync(this.key, obj);
        return this;
    }

    setTimeout(interval) {
        const config = this.getRaw();
        if (config == null) {
            return this;
        }
        ExpireUtil.setExpireTime(config, interval);
        wx.setStorageSync(this.key, config);
        return this;
    }

    get() {
        try {
            const value = wx.getStorageSync(this.key);
            if (value == null) {
                return null;
            }
            const data = value.data;
            return data ? data : null;
        } catch (e) {
            return null;
        }
    }

    getRaw() {
        try {
            const value = wx.getStorageSync(this.key);
            return value ? value : null;
        } catch (e) {
            return null;
        }
    }

    clear() {
        wx.setStorageSync(this.key, null);
        return this;
    }

    timeout() {
        // return true;
        const config = this.getRaw(this.key);
        // console.log("config key:" + this.key, config);
        if (config == null) {
            return true;
        }
        return ExpireUtil.isExpire(config);
    }

}

export {CustomStorage};

export function clearAllStorage() {
    wx.clearStorageSync();
    /*for (const key in StorageConst) {
        console.log('key:' + key, 'value', wx.getStorageSync(StorageConst[key]));
        wx.setStorageSync(StorageConst[key], null);
    }*/
}

export function clearWhenLogin() {
    wx.setStorageSync(StorageConst.USER_SESSION_KEY, null);
    wx.setStorageSync(StorageConst.ONLINE_USER_INFO, null);
    wx.setStorageSync(StorageConst.ERP_USER_INFO, null);
    wx.setStorageSync(StorageConst.NOTICE_KEY, null);
};
