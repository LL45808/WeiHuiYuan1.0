import RequestUtil from "../api/request";
import {ApiConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import {Util} from "../../utils/common";
import {getCardTypePayBean} from "../user/user";

class PayBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    hasPay = false;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getAppId() {
        return this.data.get('appId');
    }

    getNonceStr() {
        return this.data.get('nonceStr');
    }

    getPackage() {
        return this.data.get('package');
    }

    getSignType() {
        return this.data.get('signType');
    }

    getTimestamp() {
        return this.data.get('timeStamp');
    }

    getPaySign() {
        return this.data.get('paySign');
    }

    isPay() {
        return this.hasPay;
    }

    setPay(pay) {
        this.hasPay = pay;
        return this;
    }
}

class PayService {

    fetchCardTypePayInfo(data, callback) {
        const post = {
            mobile: data.mobile,
            cardCode: data.cardCode
        };
        RequestUtil.commonRequest(ApiConst.FETCH_COUPON_DONATE, post, (res) => {
            
            const response = ToolUtil.commonResponse2(res, '', '', (result, response) => {
                const bean = getCardTypePayBean(result);
                bean.setPayLogExist(response.getCode() == 'paid');
                return bean;
            });
            callback(response);
        }, (res) => {
            ccallback(ToolUtil.errorResponse('请求失败'));
        });
    }

    /**
     * 会员卡购买支付准备
     */
    buyCardPreparePay(data, callback) {
        const post = {
            amount: data.amount,
            mobile: data.mobile,
            cardType: data.cardType,
            cardCode: data.cardCode,
            openid: data.openid
        };
        RequestUtil.commonRequest(ApiConst.FETCH_BUY_CARD_PREPARE, post, (res) => {
            console.log("success", res);
            const response = ToolUtil.commonResponse2(res, "", "支付信息获取失败", (result, response) => {
                console.log('res', res);
                const bean = new PayBean(result);
                bean.setPay(response.getCode() == 'already_exist_pay_log');
                return bean;
            });
            callback(response);
        }, (res) => {
            console.log("fail", res);
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    wxPay(bean, callback) {
        if (!(bean instanceof PayBean)) {
            callback(ToolUtil.errorResponse("参数无效"));
            return;
        }
        wx.requestPayment({
            'timeStamp': String(bean.getTimestamp()),
            'nonceStr': bean.getNonceStr(),
            'package': bean.getPackage(),
            'signType': bean.getSignType(),
            'paySign': bean.getPaySign(),
            'success': function (res) {
                console.log("success", res);
                const succ = res.errMsg == 'requestPayment:ok';
                const response = ToolUtil.getApiResponse();
                response.setSuccess(succ).setMsg(succ ? '支付成功' : '支付失败');
                callback(response);
            },
            'fail': function (res) {
                console.log('fail', res);
                callback(ToolUtil.errorResponse('支付失败'));
            }
        })
    }
}


export function getPayService() {
    return new PayService();
}
