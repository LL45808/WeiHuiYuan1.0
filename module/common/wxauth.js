class WxAuthService {

    /**
     * 查看是否授权获取用户信息
     * @param callback
     */
    userAuth(callback) {
        this.getAuth("scope.userInfo", callback);
    }

    getAuth(name, callback) {
        wx.getSetting({
            success: function (res) {
                console.log("success", res);
                if (res.errMsg == 'getSetting:ok') {
                    const authSetting = res.authSetting;
                    for (const key in authSetting) {
                        const value = authSetting[key];
                        if (key == name && (typeof value == 'boolean')) {
                            callback(value);
                            return;
                        }
                    }
                }
                callback(false);
            },
            fail: function (res) {
                console.log("fail", res);
                callback(false);
            }
        });
    }
}

const wxAuthServiceIns = new WxAuthService();

export function getWxAuthService() {
    return wxAuthServiceIns;
}
