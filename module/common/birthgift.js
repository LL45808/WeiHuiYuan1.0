import RequestUtil from "../api/request";
import {ApiConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import {Util} from "../../utils/common";


class BirthGiftListBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getList() {
        const list = this.data.toArray();
        if (Util.isArray(list)) {
            return list;
        } else {
            return [];
        }
    }
}

class BirthGiftDetailBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getGoodsList() {
        const goodsList = this.data.get('goodsList', []);
        /*"goodsCode":"00003",
            "goodsName":"绿致伊朗开心果（大-白）",
            "image":"https://wx3.bzqch.cn/csjqcrm/attachment/images/2/2018/08/d9zfbbRuLJaIcbiU4ijx9iFUGGBRol.jpg",
            "goodsId":"14",
            "desc":""*/
        if (Util.isArray(goodsList)) {
            return goodsList;
        } else {
            return [];
        }
    }

    getAppointId() {
        return this.data.getInt('appointId', 0);
    }

    getTitle() {
        return this.data.get('title', '');
    }

    getTimeFormat() {
        return this.data.get('timeFormat', '');
    }

    getStartDate() {
        return this.data.get('startTimeFormat', '');
    }

    getEndDate() {
        return this.data.get('endTimeFormat', '');
    }
}

class BirthGiftService {
    /**
     * 预约列表
     * @param callback
     */
    fetchGiftList(callback) {
        RequestUtil.commonRequest(ApiConst.FETCH_GIFT_LIST, {}, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "礼物信息获取失败", (result, response) => {
                const bean = new BirthGiftListBean(result);
                return bean;
            });
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

    /**
     * 礼品详情
     * @param data
     * @param callback
     */
    fetchGiftDetail(data, callback) {
        const post = {
            appointId: data.appointId
        };
        RequestUtil.commonRequest(ApiConst.FETCH_GIFT_DETAIL, post, (res) => {
            console.log('res', res);
            const response = ToolUtil.commonResponse2(res, "", "礼物详情获取失败", (result, response) => {
                const bean = new BirthGiftDetailBean(result);
                return bean;
            });
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

    /**
     * 预约
     * @param data
     * @param callback
     */
    appointment(data, callback) {
        const post = {
            appointId: data.appointId,
            birthday: data.birthday,
            mobile: data.mobile,
            goodsId: data.goodsId
        };
        RequestUtil.commonRequest(ApiConst.FETCH_GIFT_APPOINTMENT, post, (res) => {
            console.log('res', res);
            const response = ToolUtil.commonResponse2(res, "", "预约失败");
            if (response.getCode() == 'order_has_one') {
                response.setMsg("您已预约");
            }
            callback(response);
        }, () => {
            callback(ToolUtil.errorResponse("请求错误"));
        });
    }

}

export function getBirthGiftService() {
    return new BirthGiftService();
};
