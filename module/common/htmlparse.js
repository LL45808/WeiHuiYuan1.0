const tagMap = {
    button: buttonTag,
    li: liTag,
    video: videoTag,
    img: imgTag,
    table: tableTag,
    a: aTag,
    br: brTag
};

function getContent(item) {
    let content = '';
    // console.log("list:" + item.tag + "   tagType:" + item.tagType);
    // for (let i = 0; i < arr.length; i++) {
    //     const item = arr[i];
    const list = item.nodes;
    for (let j = 0; j < list.length; j++) {
        content += wxParse2(list[j]);
    }
    // }
    return content;
}

function wxParse(list) {
    console.log("list", list);
    const result = [];
    for (let i = 0; i < list.length; i++) {
        result.push(wxParse2(list[i]));
    }
    return result.join("");
}

function wxParse2(obj) {
    console.log("item", obj)
    if (obj.node == 'element') {
        return tagParse(obj);
    } else if (obj.node == 'text') {
        return WxEmojiView(obj);
    } else {
        return "";
    }
}

function tagParse(item) {
    const tag = item.tag;
    if (tagMap[tag]) {
        return tagMap[tag](item);
    } else if (item.tagType == 'block') {
        return blockTag(item);
    } else {
        return innerTag(item);
    }
}

function WxEmojiView(item) {
    let content = '';
    const list = item.textArray;
    for (let i = 0; i < list.length; i++) {
        const obj = list[i];
        if (obj.node == 'text') {
            content += obj.text;
        } else if (obj.node == 'element') {
            content += `<image class="wxEmoji" src="${obj.baseSrc}${obj.text}"/>`;
        }
    }
    const html = `<view class="WxEmojiView wxParse-inline" style="${item.styleStr ? item.styleStr : ''}">${content}</view>`;
    return html;
}

function buttonTag(item) {
    return `<button type="default" size="mini">${getContent(item)}</button>`;
}

function liTag(item) {
    // console.log(item);
    let html = `<view class="${item.classStr ? item.classStr : ''} wxParse-li" style="${item.styleStr ? item.styleStr : ''}">
        <view class="${item.classStr ? item.classStr : ''} wxParse-li-inner">
        <view class="${item.classStr ? item.classStr : ''} wxParse-li-text">
        <view class="${item.classStr ? item.classStr : ''} wxParse-li-circle"></view>
        </view>
        <view class="${item.classStr ? item.classStr : ''} wxParse-li-text">
        ${getContent(item)}
        </view>
        </view>
        </view>`;
    return html;
}

function videoTag(item) {
    return `<view class="${item.classStr ? item.classStr : ''} wxParse-${item.tag}" style="${item.styleStr ? item.styleStr : ''}">
        <video class="${item.classStr ? item.classStr : ''} wxParse-${item.tag}-video" src="${item.attr.src}"></video>
        </view>`;
}

function imgTag(item) {
    return `<image class="${item.classStr ? item.classStr : ''} wxParse-${item.tag}" data-from="${item.from}" data-src="${item.attr.src}" data-idx="${item.imgIndex}" src="${item.attr.src}" mode="aspectFit" bindload="wxParseImgLoad" bindtap="wxParseImgTap" mode="widthFix" style="width:${item.width}px;"/>`
}

function aTag(item) {
    let html = `<view bindtap="wxParseTagATap" class="wxParse-inline ${item.classStr ? item.classStr : ''} wxParse-${item.tag}" data-src="${item.attr.href}" style="${item.styleStr ? item.styleStr : ''}">${getContent(item)}</view>`;
    return html;
}

function tableTag(item) {
    let html = `<view class="${item.classStr ? item.classStr : ''} wxParse-${item.tag}" style="${item.styleStr ? item.styleStr : ''}">${getContent(item)}</view>`;
    return html;
}

function brTag(item) {
    return `<text>\n</text>`;
}

function blockTag(item) {
    return `<view class="${item.classStr ? item.classStr : ''} wxParse-${item.tag}" style="${item.styleStr ? item.styleStr : ''}">${getContent(item)}</view>`;
}

function innerTag(item) {
    return `<view class="${item.classStr ? item.classStr : ''} wxParse-${item.tag} wxParse-${item.tagType}" style="${item.styleStr ? item.styleStr : ''}">${getContent(item)}</view>`;
}

export function htmlParse(obj) {
    return wxParse(obj);
};
