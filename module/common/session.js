import {CustomStorage} from "./StorageUtil";
import {ApiConst, StorageConst} from "../api/const";
import ValidateUtil from "../../utils/ValidateUtil";
import {ToolUtil} from "../../utils/ToolUtil";
import {Util} from "../../utils/common";
import RequestUtil from "../api/request";

class UserSessionStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.USER_SESSION_KEY);
    }

    setKv(key, value) {
        const obj = this.get();
        if (obj != null) {
            obj[key] = value;
            this.set(obj);
        }
    }

    setSession(session) {
        this.set(session);
        this.setTimeout(3600000 * 8);
        // getApp().storage.setAuth(session);
    }

    clearSession() {
        this.clear();
        // getApp().storage.clearAuth();
    }

    getSession() {
        return this.get();
    }
}

const UserSessionStorageUtil = new UserSessionStorage();


class SessionBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getSessionKey() {
        return this.data.get('session_key', '');
    }

    getToken() {
        return this.data.get('token', '');
    }

    getMobile() {
        return this.data.get('mobile', '');
    }

    getTitle() {
        return this.data.get('title', '');
    }

    getOpenid() {
        return this.data.get('openid', '');
    }

    getUnionid(){
        return this.data.get('unionid','');
    }

    //session信息是否有效
    valid() {
        if (ValidateUtil.isEmpty(this.getSessionKey()) || ValidateUtil.isEmpty(this.getToken())) {
            return false;
        } else {
            return true;
        }
    }

    hasRegister() {
        return ValidateUtil.isMobile(this.getMobile());
    }
}

function createSessionBean(data) {
    return new SessionBean(data);
}

export function getSessionBean() {
    return createSessionBean(UserSessionStorageUtil.getSession());
}


class SessionManage {

    /**
     * 检查session是否有效
     */
    check() {
        const func = function () {
            console.log("check");
            setTimeout(() => {
                func();
            }, 60000);
        };
        func();
    }

    refetch() {
        getSessionService().clearSession();
        getSessionService().fetchSession((response) => {
            console.log("session:" + response.getMsg() + " status:" + response.isSuccess());
        });
    }

}

class UserSessionService {
    /**
     *
     */
    hasSession() {
        if (UserSessionStorageUtil.timeout()) {
            return false;
        }
        const obj = createSessionBean(UserSessionStorageUtil.getSession());
        return obj.valid();
    }

    clearSession() {
        UserSessionStorageUtil.clearSession();
    }

    crmLogin(outh, callback) {
        const response = ToolUtil.getApiResponse();
        const post = {unionid: outh.unionid, openid: outh.openid, session_key: outh.session_key};
        RequestUtil.commonRequest(ApiConst.CRM_WX_LOGIN, post, (res) => {
            UserSessionStorageUtil.clearSession();
            var result = res.data.data.result;
            const sessionBean = createSessionBean(result);
            if (sessionBean.valid()) {
                console.log("用户登录成功");
                UserSessionStorageUtil.setSession(result);
                UserSessionStorageUtil.setTimeout(1000 * 60 * 60 * 8);
                response.setSuccess(true).setMsg('session初始化成功').setData(sessionBean);
            } else {
                console.log("用户登录失败1");
                response.setSuccess(false).setMsg('session初始化失败');
            }
            callback(response);
        }, (res) => {
            //清除登录状态
            UserSessionStorageUtil.clearSession();
            response.setSuccess(false).setMsg('微信登录失败');
            callback(response);
        });

        /**
         * 调用后台crm微信会员登录接口
         */
        /* app.crmApi(app.apiService.crm.wxLogin, data, (res) => {
             // app.storage.setAuth(res.data.data);
             // UserSessionStorageUtil.setSession(res.data.data);
             UserSessionStorageUtil.clearSession();
             const sessionBean = createSessionBean(res.data.data);
             if (sessionBean.valid()) {
                 console.log("用户登录成功");
                 UserSessionStorageUtil.setSession(res.data.data);
                 UserSessionStorageUtil.setTimeout(1000 * 60 * 60 * 8);
                 response.setSuccess(true).setMsg('session初始化成功').setData(sessionBean);
             } else {
                 console.log("用户登录失败1");
                 response.setSuccess(false).setMsg('session初始化失败');
             }
             callback(response);
         }, () => {
             console.log("用户登录失败2");
             //清除登录状态
             UserSessionStorageUtil.clearSession();
             response.setSuccess(false).setMsg('微信登录失败');
             callback(response);
         })*/
    }

    /**
     * 获取微信session
     * @param code
     * @param callback
     */
    parseWxSession(code, callback) {
        const post = {code: code};
        RequestUtil.commonRequest(ApiConst.FETCH_WX_SESSION, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "微信session获取失败");
            /*     if (response.getCode() == 'nobind') {
                     const result = response.getData();
                     console.log('result', result);
                     //如果没有绑定微信公众号,需要去绑定微信公众号
                     PageUtil.navigateTo('attention', {openid: result.outh.openid});
                 } else {*/
            if (response.isSuccess()) {
                const result = response.getData();
                console.log("wxSession", result);
                //微信登录
                const outh = result.outh;
                this.crmLogin(outh, callback);
            } else {
                callback(ToolUtil.errorResponse("微信session获取失败"));
            }
            // }
        }, (res) => {
            callback(ToolUtil.errorResponse("微信session获取失败"));
        });
    }

    /**
     * 加载远程用户信息
     */
    fetchSession(callback) {
        console.log('session', UserSessionStorageUtil.timeout());
        if (!UserSessionStorageUtil.timeout()) {
            console.log("session未超时1", UserSessionStorageUtil.getSession());
            //没有超时直接获取信息
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setMsg('session未超时').setData(createSessionBean(UserSessionStorageUtil.getSession()));
            callback(response);
            return;
        }
        wx.checkSession({
            success: () => {
                console.log("session未超时2");
                //session没有超时
                this._login(callback);
            },
            fail: () => {
                console.log("session超时");
                //session超时之后清除session信息,重新获取
                UserSessionStorageUtil.clearSession();
                this._login(callback);
            }
        })
    }

    _login(callback) {
        const response = ToolUtil.getApiResponse();
        const app = getApp();
        if (!this.hasSession()) {
            console.log("没有session");
            wx.login({
                success: res => {
                    console.log("微信登录成功");
                    /**
                     * 微信API登录成功，再调取后台业务服务器的登录接口，获取TOKEN
                     */
                    this.parseWxSession(res.code, (response) => {
                        callback(response);
                    });
                },
                fail: res => {
                    console.log("微信登录失败");
                    UserSessionStorageUtil.clearSession();
                    response.setSuccess(false).setMsg('微信登录失败');
                    callback(response);
                }
            });
        } else {
            console.log("有session");
            response.setSuccess(true).setMsg("").setData(createSessionBean(UserSessionStorageUtil.getSession()));
            callback(response);
        }
    }


    getSession() {
        return UserSessionStorageUtil.getSession();
    }
}

const userServiceIns = new UserSessionService();

export function getSessionService() {
    return userServiceIns;
}

export {UserSessionStorageUtil};
