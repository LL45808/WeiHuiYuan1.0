const defaultCallback = function () {

};

class LoadUserConfig {

    config = {
        erp: {
            fetchCache: true,
            callback: defaultCallback
        },
        online: {
            fetchCache: true,
            callback: defaultCallback
        }
    };

    setErpFetchCache(cache) {
        this.config.erp.fetchCache = cache;
        return this;
    }

    setErpCallback(callback) {
        this.config.erp.callback = callback;
        return this;
    }

    setOnlineFetchCache(cache) {
        this.config.online.fetchCache = cache;
        return this;
    }

    setOnlineCallback(callback) {
        this.config.online.callback = callback;
        return this;
    }

    getConfig() {
        return this.config;
    }
}

export function getFetchUserInfoConfig() {
    return new LoadUserConfig();
};
