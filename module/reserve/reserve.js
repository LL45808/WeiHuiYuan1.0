import {UserUtil} from "../../utils/UserUtil";
import RequestUtil from "../api/request";
import {ApiConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import ValidateUtil from "../../utils/ValidateUtil";
var timeutil =require('../../utils/util.js');

/**
 * 寄存接口信息
 */
class ReserveService{


    /**
     * 获取门店列表
     * @param callback
     */
    fetchShopList(data, callback) {
        const post = {vipId: data.vipId, token: UserUtil.getToken()};
        RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_LIST, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '门店信息获取失败', (result) => {
                return ValidateUtil.isArray(result)?result:[];
            });
            callback(response);
        }, (res) => {
            ccallback(ToolUtil.errorResponse('请求失败'));
        });
    }

    /**
     * 获取门店商品列表
     * @param callback
     */
    fetchShopGoodsList(data, callback) {
        const post = {vipId: data.vipId, token: UserUtil.getToken(),shopId:data.shopId};
        RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_GOODS_LIST, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '门店商品获取失败', (result) => {
                return ValidateUtil.isArray(result)?result:[];
            });
            callback(response);
        }, (res) => {
            ccallback(ToolUtil.errorResponse('请求失败'));
        });
    }
  /**
  * 获取订单列表
  * @param callback
  */
  fetchShopOrderList(data, callback) {
    var time = timeutil.formatTime(new Date());
    console.log(time)
    const post = {
      token: UserUtil.getToken(),
      cardId: data.cardId,
      startDate: '2010-1-1',
      endDate: time };
    RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_ORDER_LIST, post, (res) => {
      const response = ToolUtil.commonResponse2(res, '', '获取订单列表失败', (result) => {
        return ValidateUtil.isArray(result) ? result : [];
      });
      callback(response);
    }, (res) => {
      ccallback(ToolUtil.errorResponse('请求失败'));
    });
  }
  
  /**
 * 获取订单详情
 * @param callback
 */
  fetchShopOrderGoodsList(data, callback) {
    const post = {
      token: UserUtil.getToken(),
      orderId: data.orderId,
    };
    RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_ORDER_GOODS_LIST, post, (res) => {
      const response = ToolUtil.commonResponse2(res, '', '获取订单详情失败', (result) => {
        return ValidateUtil.isArray(result) ? result : [];
      });
      callback(response);
    }, (res) => {
      ccallback(ToolUtil.errorResponse('请求失败'));
    });
  }
  
  /**
 *存订单支付
 * @param callback
 */
  fetchShopOrderPay(data, callback) {
    const post = {
      mobile:data.mobile,
      orderId: data.orderId,
    };
    RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_ORDER_PAY, post, (res) => {
      const response = ToolUtil.commonResponse2(res, '', '获取支付失败', (result) => {
        return ValidateUtil.isArray(result) ? result : [];
      });
      callback(response);
    }, (res) => {
      ccallback(ToolUtil.errorResponse('请求失败'));
    });
  }
    // ctrl:reserve
    // action:goodsImageList
    /**
     * 获取图片
     * @param callback
     */
    fetchGoodsImage(data, callback) {
        const post = {
            goodsSn:data.goodsSn,//商品编码,多个使用逗号分隔,
            thumbnail:data.thumbnail,//[1|0] 1=>缩略图,0不是缩略图
            width:data.width,//缩略图宽,
            height:data.height//缩略图高
        };
        RequestUtil.commonRequest(ApiConst.RESERVE_SHOP_GOODS_IMAGE, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '获取图片失败', (result) => {
                return ValidateUtil.isArray(result) ? result : [];
            });
            callback(response);
        }, (res) => {
            ccallback(ToolUtil.errorResponse('请求失败'));
        });
    }

}


const reserveService=new ReserveService();

/**
 *
 * @returns {ReserveService}
 */
export function getReserveService() {
    return reserveService;
}