import {TimeUtil} from "../../utils/TimeUtil";

class MemoryStorage {

    __list = {};

    set(name, value) {
        const obj = {value: value};
        this.__list[name] = obj;
        return this;
    }

    clear(name) {
        delete this.__list[name];
    }

    set2(name, value, ttl) {
        this.set(name, value).setTtl(name, ttl);
        return this;
    }

    get(name) {
        const obj = this.__list[name] ? this.__list[name] : null;
        if (obj != null) {
            if (obj['expire']) {
                const expire = obj['expire'];
                const nowTime = TimeUtil.getNowTime();
                if (nowTime > expire) {
                    return null;
                } else {
                    return obj.value;
                }
            } else {
                obj.value;
            }
        }
        return null;
    }

    setTtl(name, ttl) {
        if (this.__list[name]) {
            this.__list[name].expire = TimeUtil.getNowTime() + ttl;
        }
        return this;
    }
}

const memoryCacheIns = new MemoryStorage();

/**
 *
 * @returns {MemoryStorage}
 */
export function getCacheService() {
    return memoryCacheIns;
}
