import RequestUtil from "../api/request";
import {UserUtil} from "../../utils/UserUtil";
import {Util} from "../../utils/common";
import {ToolUtil} from "../../utils/ToolUtil";
import {CustomStorage} from "../common/StorageUtil";
import ValidateUtil from "../../utils/ValidateUtil";
import {ApiConst, StorageConst} from "../api/const";

const app = getApp();

class CardTypeStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.CARD_TYPE_CONFIG_STORAGE_KEY);
    }
}

const CardTypeUtil = new CardTypeStorage();

class CardTypeInfoBean {

    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    rawData = {};

    constructor(data) {
        this.rawData = data;
        this.data = Util.objectExt(data);
    }

    getSort() {
        return this.data.getInt('msort', 0);
    }

    getNumber() {
        return this.data.getInt('number', 0);
    }

    getCardTypeName() {
        return this.data.get('cardType', '');
    }

    getCardCode() {
        return this.data.get('code', '');
    }

    getAmount() {
        return this.data.getFloat('amount', 0);
    }

    isUpgradeNeedAudit() {
        return this.data.bool('upgrade_need_audit', false);
    }

    getRawData() {
        return this.rawData;
    }
}

/**
 * 卡类配置
 */
class CardTypeConfig {

    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    /**
     * 注册是否选择卡类
     * @returns {boolean}
     */
    isRegisterSelectCardType() {

        return this.data.bool('register_select_card_type', false);
    }

    /**
     * 注册时是否能选择门店
     */
    isRegisterSelectShop(){
        return this.data.bool('manual_select_shop',false);
    }

    isShowCardTypeValidTime(){
        return this.data.bool('showCardTypeValidTime',false);
    }

    /**
     * 注册是否需要先关注
     * @returns {*}
     */
    isRegisterNeedAttention(){
        return this.data.bool('attention.register_need_attention',false);
    }
    /**
     * 卡类列表是否为空
     * @returns {boolean}
     */
    isEmptyCardTypeList() {
        const list = this.data.get('card_type_list', []);
        if (ValidateUtil.isArray()) {
            return list.length == 0;
        } else {
            return true;
        }
    }

    /**
     * 是否计算转介绍人的积分
     * @returns {*}
     */
    isCalcRecommendedIntegral() {
        return this.data.bool('calc_recommended_integral', false);
    }

    /**
     * 获取卡类列表
     * const obj={
            msort:'排序',
            number:'自动续费额度',
            cardType:'卡类',
            code:'卡类编码',
            upgrade_need_audit:'是否需要审核',
            amount:'支付升级金额'
        };
     */
    getCardTypeList(onlyUnaudit, showAmountZero) {
        onlyUnaudit = typeof onlyUnaudit == 'boolean' ? onlyUnaudit : false;
        showAmountZero = typeof showAmountZero == 'boolean' ? showAmountZero : true;
        const list = this.data.get('card_type_list', []);
        // console.log("list", list);
        const newList = [];
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            obj.rowName = obj.cardType + " 年费" + obj.amount + "元";
            let add = false;
            if (onlyUnaudit) {
                if (!obj.upgrade_need_audit) {
                    add = true;
                }
            } else {
                add = true;
            }
            if (!showAmountZero) {
                if (obj.amount <= 0) {
                    add = false;
                }
            }
            if (add) {
                newList.push(obj);
            }
        }
        // console.log("list", list);
        return newList;
    }

    cardTypeInfoValid(cardTypeInfo) {
        if (typeof cardTypeInfo != 'object') {
            return false;
        }
        return cardTypeInfo.code != null && cardTypeInfo.cardType != null;
    }

    /**
     * 卡类比较，用户已经存在的卡类和当前选择的卡类比较
     * 1.如果存在的卡类大于等于选择的卡类等级，则不需要做升级处理
     * 2.如果存在存在的卡类小于选择的卡类等级，需要做升级处理
     */
    isUpgradeByCardTypeCompare(nowCardType, selectCardType) {
        const list = this.getCardTypeList(false);
        let nowCardIndex = 0;
        let selectCardIndex = 0;
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            if (nowCardType == obj.cardType) {
                nowCardIndex = i;
            }
            if (selectCardType == obj.cardType) {
                selectCardIndex = i;
            }
        }
        return nowCardIndex < selectCardIndex;
    }

    cardTypeEqual(nowCardType, selectCardType) {
        const list = this.getCardTypeList(false);
        let nowCardIndex = -1;
        let selectCardIndex = -2;
        for (let i = 0; i < list.length; i++) {
            const obj = list[i];
            if (nowCardType == obj.cardType) {
                nowCardIndex = i;
            }
            if (selectCardType == obj.cardType) {
                selectCardIndex = i;
            }
        }
        return nowCardIndex == selectCardIndex;
    }
}


class CardTypeService {

    /**
     * 獲取卡类
     * @param callback
     */
    fetchCardTypeConfig(callback) {
        if (!CardTypeUtil.timeout()) {
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setData(new CardTypeConfig(CardTypeUtil.get()));
            callback(response);
            return;
        }
        RequestUtil.commonRequest(ApiConst.FETCH_CARDTYPE_INFO, {
            token: UserUtil.getToken()
        }, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '卡类信息获取失败', (result) => {
                console.log('result',result);
                CardTypeUtil.set(result);
                CardTypeUtil.setTimeout(60 * 60 * 1000);
                return new CardTypeConfig(result);
            });
            if (!response.isSuccess()) {
                CardTypeUtil.clear();
            }
            callback(response);
        }, (res) => {
            CardTypeUtil.clear();
            callback(ToolUtil.errorResponse('卡类信息获取失败'));
        });

    }

    /**
     * 获取用户可以展示的卡类列表
     * 1.如果所有会员类都不需要审核，直接返回所有会员卡类
     * 2.如果有需要审核，查询是否审核
     * 3.最终返回不需要审核和审核通过的会员卡类
     * @param data
     * @param cardTypeList
     * @param callback
     */
    fetchUserEnableDisplayCardTypeList(data, cardTypeList, callback) {
        let needRequest = false;
        const cardTypeBeanList = [];
        const noNeedAuditList = [];//不需要审核的卡类列表
        for (let i = 0; i < cardTypeList.length; i++) {
            const cardBean = getCardTypeInfnoBean(cardTypeList[i]);
            cardTypeBeanList.push(cardBean);
            if (cardBean.isUpgradeNeedAudit()) {
                needRequest = true;
            } else {
                noNeedAuditList.push(cardTypeList[i]);
            }
        }
        if (!needRequest) {
            //不需要请求直接返回原本的卡类
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setData(cardTypeList);
            callback(response);
        }

        const post = {uid: data.uid};
        RequestUtil.commonRequest(ApiConst.FETCH_USER_AUDIT_PASS_CARDTYPE_LIST, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '信息获取失败');
            if (response.isSuccess()) {
                const list = response.getData();
                if (!Util.isArray(list)) {
                    response.setData(noNeedAuditList);
                } else {
                    if (list.length == 0) {
                        response.setData(noNeedAuditList);
                    } else {
                        const newList = [];
                        for (let i = 0; i < cardTypeBeanList.length; i++) {
                            const cardBean = cardTypeBeanList[i];
                            if (cardBean.isUpgradeNeedAudit()) {
                                for (let j = 0; j < list.length; j++) {
                                    const cardCode = list[j];
                                    if (cardBean.getCardCode() == cardCode) {
                                        //已审核
                                        newList.push(cardBean.getRawData());
                                    }
                                }
                            } else {
                                newList.push(cardBean.getRawData());
                            }
                        }
                        response.setData(newList);
                    }
                }
            } else {
                response.setData(noNeedAuditList);
            }
            response.setSuccess(true);
            callback(response);
        }, (res) => {
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setData(noNeedAuditList);
            callback(response);
        });
    }
}

export {CardTypeConfig};

export function getCardTypeInfnoBean(data) {
    return new CardTypeInfoBean(data);
}

export function getCardTypeService() {
    return new CardTypeService();
};
