import {ApiConst} from "../api/const";
import {ToolUtil} from "../../utils/ToolUtil";
import RequestUtil from "../api/request";
import {Util} from "../../utils/common";


class PayBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    hasPay = false;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getAppId() {
        return this.data.get('appId');
    }

    getNonceStr() {
        return this.data.get('nonceStr');
    }

    getPackage() {
        return this.data.get('package');
    }

    getSignType() {
        return this.data.get('signType');
    }

    getTimestamp() {
        return this.data.get('timeStamp');
    }

    getPaySign() {
        return this.data.get('paySign');
    }

    isPay() {
        return this.hasPay;
    }

    setPay(pay) {
        this.hasPay = pay;
        return this;
    }
}

class PayService {

    preparePay(openid, tid, callback) {
        RequestUtil.shopRequest(ApiConst.SHOP_ORDER_PREPARE_PAY, {openid: openid, tid: tid}, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "支付信息获取失败", (result, response) => {
                const bean = new PayBean(result);
                bean.setPay(false);
                return bean;
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse('支付信息获取失败'));
        });
    }

    wxPay(bean,callback) {
        if (!(bean instanceof PayBean)) {
            callback(ToolUtil.errorResponse("参数无效"));
            return;
        }
        wx.requestPayment({
            'timeStamp': String(bean.getTimestamp()),
            'nonceStr': bean.getNonceStr(),
            'package': bean.getPackage(),
            'signType': bean.getSignType(),
            'paySign': bean.getPaySign(),
            'success': function (res) {
                console.log("success", res);
                const succ = res.errMsg == 'requestPayment:ok';
                const response = ToolUtil.getApiResponse();
                response.setSuccess(succ).setMsg(succ ? '支付成功' : '支付失败');
                callback(response);
            },
            'fail': function (res) {
                console.log('fail', res);
                callback(ToolUtil.errorResponse('支付失败'));
            }
        })
    }
}


export function getPayService() {
    return new PayService();
}
