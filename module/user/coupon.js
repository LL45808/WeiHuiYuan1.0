import {ToolUtil} from "../../utils/ToolUtil";
import RequestUtil from "../api/request";
import {ApiConst, StorageConst} from "../api/const";
import {Util} from "../../utils/common";
import {CustomStorage} from "../common/StorageUtil";
import {CardTypeConfig} from "../system/cardtype";

class CouponTypeStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.EXCHANGE_COUPON_TYPE_LIST_KEY);
    }
}

const CouponTypeUtil = new CouponTypeStorage();

class CouponBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    fromCardId = "";
    donateUsername = '';

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getStatu() {
        return this.data.get('status', '');
    }

    getId() {
        return this.data.get('id', '');//券号
    }

    getName() {
        return this.data.get('name', '');//券名
    }

    getType() {
        return this.data.get('type', '');//券类型,折扣券,代金券
    }

    getAmount() {
        return this.data.get('amount', 0);
    }

    getDiscount() {
        return this.data.get('discount', '');
    }

    getStartDate() {
        return this.data.get('startDate', '');
    }

    getEndDate() {
        return this.data.get('endDate', '');
    }

    setFromCardId(cardId) {
        this.fromCardId = cardId;
        return this;
    }

    getFromCardId() {
        return this.fromCardId;
    }

    setDonateUsername(name) {
        this.donateUsername = name;
        return this;
    }

    getDonateUsername() {
        return this.donateUsername;
    }
}

class CouponService {

    /**
     * 获取转赠优惠券
     * @param data
     * @param callback
     */
    getDonateCoupon(data, callback) {
        const post = {couponCode: data.couponCode};
        RequestUtil.commonRequest(ApiConst.FETCH_DONATE_COUPON, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "券信息获取失败", (result, response) => {
                const fromCardId = Util.get(result, 'fromCardId', '');
                const coupon = Util.get(result, 'coupon', {});
                const bean = new CouponBean(coupon);
                bean.setFromCardId(fromCardId);
                bean.setDonateUsername(Util.get(result, 'donateUsername', ''));
                return bean;
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     * 获取兑换券类列表
     * @param callback
     */
    fetchExchangeCouponTypeList(callback) {
        if (!CouponTypeUtil.timeout()) {
            const response = ToolUtil.getApiResponse();
            response.setSuccess(true).setData(CouponTypeUtil.get());
            callback(response);
            return;
        }
        RequestUtil.commonRequest(ApiConst.FETCH_EXCHANGE_COUPON_TYPE_LIST, {}, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "券类获取失败", (result, response) => {
                if (!Util.isArray(result)) {
                    return [];
                }
                const list = [];
                for (let i = 0; i < result.length; i++) {
                    const obj = result[i];
                    list.push({
                        name: obj.name,
                        type: obj.type,
                        amount: obj.amount,
                        discount: obj.discount,
                        couponTypeId: obj.couponTypeId,
                        number: obj.number,
                        id: obj.id,
                        url:obj.url,
                        content:obj.content
                    });
                }
                return list;
            });
            if (response.isSuccess()) {
                CouponTypeUtil.set(response.getData());
                CouponTypeUtil.setTimeout(60 * 1000 * 5);
            } else {
                CouponTypeUtil.clear();
            }
            callback(response);
        }, (res) => {
            CouponTypeUtil.clear();
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     * 领取优惠券
     * @param data
     * @param callback
     */
    fetchDonateCoupon(data, callback) {
        const post = {
            mobile: data.mobile,
            id: data.id,//券号,
        };
        console.log("post", post);
        RequestUtil.commonRequest(ApiConst.FETCH_COUPON_DONATE_RECEIVE, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "领取失败");
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     *
     * @param data
     * @param callback
     */
    integralExchangeCoupon(data, callback) {
        const post = {
            cardId: data.cardId,
            id: data.id
        };
        RequestUtil.commonRequest(ApiConst.INTEGRAL_EXCHANGE_COUPON, post, (res) => {
            console.log("res", res);
            const response = ToolUtil.commonResponse2(res, "", "兑换失败");
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     * 获取erp券信息
     * @param data
     * @param callback
     */
    fetchErpSendCouponInfo(data, callback) {
        const post = {
            topicId: data.topicId,
        };
        RequestUtil.commonRequest(ApiConst.FETCH_ERP_SEND_COUPON_INFO, post, (res) => {
            console.log("res", res);
            const response = ToolUtil.commonResponse2(res, "", "券信息获取失败", (result) => {
                const obj = {
                    startDate: result.startTimeFormat,
                    endDate: result.endTimeFormat,
                    total: result.total,
                    sendTotal: result.sendTotal,
                    surplusTotal: result.surplusTotal,
                    title: result.title,
                    couponTypeName: result.couponTypeName,
                    limitNum: result.limitNum
                }
                return obj;
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     * 领取erp券
     * @param data
     * @param callback
     */
    receiveErpSendCoupon(data, callback) {
        const post = {
            topicId: data.topicId,
            mobile: data.mobile
        };
        RequestUtil.commonRequest(ApiConst.RECEIVE_ERP_SEND_COUPON, post, (res) => {
            console.log("res", res);
            const response = ToolUtil.commonResponse2(res, "", "券领取失败");
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }
}

export function getCouponService() {
    return new CouponService();
}
