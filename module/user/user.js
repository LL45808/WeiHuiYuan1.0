import RequestUtil from "../api/request";
import {createErpUserBean, createOnlineUserBean, UserUtil} from "../../utils/UserUtil";
import {Util} from "../../utils/common";
import ValidateUtil from "../../utils/ValidateUtil";
import {ToolUtil} from "../../utils/ToolUtil";
import {ApiConst, StorageConst} from "../api/const";
import {CustomStorage} from "../common/StorageUtil";
import {getWxService} from "../common/wx";
import {getCacheService} from "../cache/cache";

class OnlineUserStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.ONLINE_USER_INFO);
    }


    setUserInfo(data) {
        this.set(data);
        // getApp().storage.setUserInfo(data);
    }

    clearUserInfo() {
        this.clear();
        // getApp().storage.clearUserInfo();
    }

    getUserInfo() {
        const obj = this.get();
        if (obj == null) {
            return {};
        } else {
            return obj;
        }
    }
}

class ErpUserInfoStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.ERP_USER_INFO);
    }

    setUserInfo(data) {
        this.set(data);
        // getApp().storage.setAccount(data);
    }

    clearUserInfo() {
        this.clear();
        // getApp().storage.clearAccount();
    }

    getUserInfo() {
        const obj = this.get();
        if (obj == null) {
            return {};
        } else {
            return obj;
        }
    }
}

class NoticeStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.NOTICE_KEY);
    }
}

const NoticeStorageUtil = new NoticeStorage();

class ErpUserBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    //erp用户是否有效
    valid() {
        return !ValidateUtil.isEmpty(this.getCardId());
    }

    getCardType() {
        return this.data.get('cardType', '');
    }

    getUsername() {
        return this.data.get('username', '');
    }

    getCardId() {
        return this.data.get('cardId', '');
    }

    getSex() {
        return this.data.get('sex', '');
    }

    getBirthday() {
        return this.data.get('birthday', '');
    }

    getMobile() {
        return this.data.get('mobile', '');
    }

    getShopCode() {
        return this.data.get('shopCode', '');
    }

    getShopName() {
        return this.data.get('shopName', '');
    }

    getIntegral() {
        return this.data.getFloat('integral', 0);
    }

    getAmount() {
        return this.data.getFloat('amount', 0);
    }

//寄存
    getVipId() {
        return this.data.get('id', 0);
    }

    getCardId() {
        return this.data.get('cardId', 0);
    }
}

class UserAutoUpgradeBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    /**
     * 用户能否自动升级，主要在注册的时候，如果支持自动升级，可以不需要支付步骤
     */
    userEnableAutoUpgrade() {
        return this.data.get('autoUpgrade', false);
    }

}

class WxUserBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getOpenId() {
        return this.data.get('openId');
    }

    getUnionId() {
        return this.data.get('unionId');
    }

    getNickName() {
        return this.data.get('nickName');
    }

    getIv() {
        return this.data.get('iv', '');
    }

    getEncryptedData() {
        return this.data.get('encryptedData', '');
    }
}

class CardTypePayBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;
    payLogExist = false;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getAmount() {
        return this.data.get('amount');
    }

    getOrderSn() {
        return this.data.get('ordersn');
    }

    getMobile() {
        return this.data.get('amount');
    }

    getCardCode() {
        return this.data.get('cardCode');
    }

    getCardTypeName() {
        return this.data.get('cardName');
    }

    getOpenid() {
        return this.data.get('openid');
    }

    existPayLog() {
        return this.payLogExist;
    }

    setPayLogExist(exist) {
        this.payLogExist = exist;
        return this;
    }
}

class CardSubmitUserInfoBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getMobile() {
        return this.data.get('mobile', '');
    }

    getBirthday() {
        return this.data.get('birthday', '');
    }

    getName() {
        return this.data.get('name', '');
    }

    getWxCardId() {
        return this.data.get('card_id', '');
    }

    getUserCode() {
        return this.data.get('usercode', '');
    }

    hasRegister() {
        const status = this.data.getInt('status', 0);
        return status == 1;
    }
}

class UserService {
    /**
     * 获取用户在erp中的数据
     */
    fetchUserInErp(data, callback) {
        const post = {mobile: data.mobile, token: UserUtil.getToken()};
        RequestUtil.commonRequest(ApiConst.FETCH_USER_ERP, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "用户信息获取失败", (result) => {
                return new ErpUserBean(result);
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("用户信息获取失败"));
        });
    }

    /**
     * 用户能否自动升级
     * @param callback
     */
    fetchUserEnableUpgrade(data, callback) {
        const post = {cardId: data.cardId, cardType: data.cardType, token: UserUtil.getToken()};
        RequestUtil.commonRequest(ApiConst.FETCH_ENABLE_AUTO_UPGRADE_CARDTYPE, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '', (result) => {
                return new UserAutoUpgradeBean(result);
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse('请求失败'));
        });
    }

    /**
     * 获取用户扫码信息
     * @param callback
     */
    fetchUserScanCodeInfo(data, callback) {
        const post = {unionid: data.unionId, token: UserUtil.getToken()};
        RequestUtil.commonRequest(ApiConst.FETCH_USER_SCAN_CODE_INFO, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '');
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse('请求失败'));
        });
    }

    /**
     * 获取微信用户信息
     * @param callback
     */
    fetchWxUserInfo(callback) {
        const _this = this;
        wx.getUserInfo({
            withCredentials: true,
            lang: 'zh_CN',
            success: function (res) {
                console.log("获取用户信息", res);
                _this.parseUserInfo({
                    iv: res.iv,
                    encryptedData: res.encryptedData
                }, (response) => {
                    console.log("解析", response.getData());
                    callback(response);
                });
            },
            fail: function (res) {
                callback(ToolUtil.errorResponse('用户信息获取失败'));
            }
        })
    }

    parseUserInfo(data, callback) {
        var post = {
            encryptedData: data.encryptedData,
            iv: data.iv,
            session_key: UserUtil.getSessionKey()
        };
        const errormsg = '用户信息解析出错';
        const app = getApp();
        app.crmApi(
            app.apiService.crm.getWxPhone,
            post,
            (res) => {
                console.log('parse', res);
                const response = ToolUtil.getApiResponse();
                const resData = res.data;
                if (Util.isEmpty(resData.data)) {
                    response.setSuccess(false);
                    response.setMsg(errormsg);
                } else {
                    const data = Util.get(resData, "data", []);
                    if (resData.ret == 200) {
                        data.iv = post.iv;
                        data.encryptedData = post.encryptedData;
                        const bean = new WxUserBean(data);
                        response.setData(bean);
                        response.setSuccess(true);
                    } else {
                        response.setSuccess(false);
                        response.setMsg(errormsg);
                    }
                }
                callback(response);
            },
            () => {
                callback(ToolUtil.errorResponse(errormsg));
            }
        )
    }

    upgradeCardTypeByPay(data, callback) {
        const post = {
            mobile: data.mobile,
            cardType: data.cardType
        };
        RequestUtil.commonRequest(ApiConst.PAY_UPGRADE_CARDTYPE, post, (res) => {
            const response = ToolUtil.commonResponse2(res, '', '', (result, response) => {
                const bean = getCardTypePayBean(result);
                return bean;
            });
            callback(response);
        }, (res) => {
            ccallback(ToolUtil.errorResponse('请求失败'));
        });
    }

    //获取会员开卡信息
    fetchCardSubmitUserInfo(data, callback) {
        const post = {unionId: data.unionId};
        RequestUtil.commonRequest(ApiConst.FETCH_CARD_SUBMIT_USER_INFO, post, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "开卡信息获取失败", (result) => {
                console.log("result", result);
                return new CardSubmitUserInfoBean(result);
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }

    /**
     * 加载线上用户信息
     */
    fetchOnlineUserInfo(callback, fetchCache) {
        fetchCache = typeof fetchCache == 'boolean' ? fetchCache : true;
        const response = ToolUtil.getApiResponse();
        if (fetchCache) {
            if (!OnlineUserInfoStorageUtil.timeout()) {
                console.log("线上用户信息未超时");
                const data = OnlineUserInfoStorageUtil.getUserInfo();
                response.setSuccess(true).setData(createOnlineUserBean(data));
                callback(response, data);
                return;
            }
        }
        const app = getApp();
        app.crmApi(
            app.apiService.crm.memInfo,
            {token: UserUtil.getToken()},
            res => {
                console.log("获取线上用户信息", res);
                var info = OnlineUserInfoStorageUtil.getUserInfo();
                if (res.data.ret == 200) {
                    var data = res.data.data;

                    if (info.nick_name) {
                        Object.assign(info, data);
                    }
                    OnlineUserInfoStorageUtil.setUserInfo(data);
                    response.setSuccess(true).setMsg('').setData(createOnlineUserBean(data));
                } else {
                    response.setSuccess(false).setMsg('用户信息获取失败');
                }
                callback(response);
            },
            err => {
                console.log("获取线上用户信无效");
                response.setSuccess(false).setMsg('用户信息获取失败');
                callback(response);
            }
        )
    }

    clearErpUserInfo() {
        ErpUserInfoStorageUtil.clearUserInfo();
    }

    updateOnlineUserInfo() {
        const key = 'user_updateOnlineUserInfo';
        const result = getCacheService().get(key);
        if (result != null) {
            return;
        }
        getCacheService().set(key, true).setTtl(key, 3600000);//一个小时设置

        //2019-1-11
        getWxService().fetchUserInfo((response) => {
            if (!response.isSuccess()) {
                return;
            }
            const bean = response.getData();
            const post = {};
            post.nick_name = bean.getNickName();
            post.user_headimg = bean.getPortrait();
            post.sex = bean.getSex();
            post.token = UserUtil.getToken();
            const app = getApp();
            //app.crmApi(app.apiService.crm.memberEdit, post);

            var userInfo = app.storage.getUserInfo();
            userInfo.pic_head = post.user_headimg;
            userInfo.sex = post.sex;
            userInfo.nick_name = post.nick_name;
            app.storage.setUserInfo(userInfo)
        });
    }

    /**
     * 加载远程erp信息
     */
    fetchErpUserInfo(callback, fetchCache) {
        fetchCache = typeof fetchCache == 'boolean' ? fetchCache : true;
        const response = ToolUtil.getApiResponse();
        if (fetchCache) {
            if (!ErpUserInfoStorageUtil.timeout()) {
                console.log("erp用户信息未超时");
                const data = ErpUserInfoStorageUtil.getUserInfo();
                response.setSuccess(true).setData(createErpUserBean(data));
                callback(response, data);
                return;
            }
        }
        getApp().crmApi(
            getApp().apiService.crm.acctDetail,
            {
                token: UserUtil.getToken()
            },
            res => {
                if (res.data.ret == 200 && res.data.data && res.data.data.user_id) {
                    // console.log("-----------1");
                    response.setSuccess(true).setData(createErpUserBean(res.data.data));
                    ErpUserInfoStorageUtil.setUserInfo(res.data.data);
                } else {
                    // console.log("-----------2");
                    response.setSuccess(false).setMsg('用户信息获取失败');
                    ErpUserInfoStorageUtil.clearUserInfo();
                }
                callback(response);
            },
            err => {
                // console.log("-----------3");
                response.setSuccess(false).set('用户信息获取失败');
                ErpUserInfoStorageUtil.clearUserInfo();
                callback(response);
            }
        )
    }

    fetchNoticeInfo(callback) {
        var response = ToolUtil.getApiResponse();
        if (!NoticeStorageUtil.timeout()) {
            response.setSuccess(true).setData(NoticeStorageUtil.get());
            callback(response);
            return;
        }
        //获取用户通知信息

        var data = {
            token: UserUtil.getToken(),
            page: 1,
            page_size: 4
        };
        var app = getApp();
        /**
         * 当token存在时，才请求消息接口
         */
        if (!ValidateUtil.isEmpty(data.token)) {
            app.crmApi(
                app.apiService.crm.getNoticeList,
                data,
                (res) => {
                    var info = res.data.data;
                    var list = Util.get(info, 'list', []);
                    NoticeStorageUtil.clear();
                    NoticeStorageUtil.set(list).setTimeout(30 * 1000);
                    if (res.data.ret = 200) {
                        response.setSuccess(true).setData(list);
                    }
                    callback(response);
                },
                () => {
                    NoticeStorageUtil.clear();
                    response.setSuccess(true).setData([]);
                    callback(response);
                },
            )
        } else {
            NoticeStorageUtil.clear();
            response.setSuccess(true).setData([]);
            callback(response);
        }
    }

}

export function getCardTypePayBean(data) {
    return new CardTypePayBean(data);
}

export function getUserService() {
    return new UserService();
};

const ErpUserInfoStorageUtil = new ErpUserInfoStorage();
const OnlineUserInfoStorageUtil = new OnlineUserStorage();
export {ErpUserInfoStorageUtil, OnlineUserInfoStorageUtil, NoticeStorageUtil};
