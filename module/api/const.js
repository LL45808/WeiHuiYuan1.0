const ApiConst = {
    //用户erp信息
    FETCH_WX_SESSION: {
        action: 'wxSession',
        ctrl: 'user'
    },
    //crm登录
    CRM_WX_LOGIN: {
        action: 'wxLogin',
        ctrl: 'user'
    },
    //用户erp信息
    FETCH_USER_ERP: {
        action: 'erpUserInfo',
        ctrl: 'user'
    },
    //用户能否自升级卡类
    FETCH_ENABLE_AUTO_UPGRADE_CARDTYPE: {
        action: 'enableAutoUpgradeCardType',
        ctrl: 'user'
    },
    //卡类信息
    FETCH_CARDTYPE_INFO: {
        action: 'cardTypeConfig',
        ctrl: 'cardtype'
    },
    //购卡支付准备
    FETCH_BUY_CARD_PREPARE: {
        action: 'preparePayForCardType',
        ctrl: 'pay'
    },
    FETCH_CARDTYPE_PAY_INFO: {
        action: 'getPayInfo',
        ctrl: 'pay'
    },
    PAY_UPGRADE_CARDTYPE: {
        action: 'cardTypeUpgrade',
        ctrl: 'user'
    },
    //客服服务信息
    FETCH_CUSTOMER_SERVICE: {
        action: 'customerService',
        ctrl: 'config'
    },
    //获取用户信息，判断用户是否存在
    FETCH_USER_INFO: {
        action: 'fetchUserInfo',
        ctrl: 'user'
    },
    //生日礼物预约F
    FETCH_GIFT_APPOINTMENT: {
        action: 'appointment',
        ctrl: 'gift'
    },
    //生日礼物预约列表
    FETCH_GIFT_LIST: {
        action: 'giftList',
        ctrl: 'gift'
    },
    //生日礼物预约详情
    FETCH_GIFT_DETAIL: {
        action: 'giftDetail',
        ctrl: 'gift'
    },
    //优惠券转赠
    FETCH_COUPON_DONATE: {
        action: 'donationCoupon',
        ctrl: 'coupon'
    },
    //优惠券转赠领取
    FETCH_COUPON_DONATE_RECEIVE: {
        action: 'receiveDonateCoupon',
        ctrl: 'coupon'
    },
    //优惠券转赠取消
    FETCH_COUPON_DONATE_CANCEL: {
        action: 'cancelCoupon',
        ctrl: 'coupon'
    },
    //积分换券，券类列表
    FETCH_EXCHANGE_COUPON_TYPE_LIST: {
        action: 'exchangeCouponTypeList',
        ctrl: 'coupon'
    },
    //积分换券
    INTEGRAL_EXCHANGE_COUPON: {
        action: 'integralExchangeCoupon',
        ctrl: 'coupon'
    },
    //获取用户审核通过的卡类列表
    FETCH_USER_AUDIT_PASS_CARDTYPE_LIST: {
        action: 'getAuditPassCardTypeList',
        ctrl: 'user'
    },
    FETCH_DONATE_COUPON: {
        action: 'getDonateInfo',
        ctrl: 'coupon'
    },
    //获取erp发券信息
    FETCH_ERP_SEND_COUPON_INFO: {
        action: 'erpSendCouponInfo',
        ctrl: 'coupon'
    },
    //领取erp发券
    RECEIVE_ERP_SEND_COUPON: {
        action: 'receiveErpCoupon',
        ctrl: 'coupon'
    },
    //获取微信卡包信息
    FETCH_CONFIG_WXCARD_INFO: {
        action: 'cardConfig',
        ctrl: 'config'
    },
    //获取微信会员卡签名
    FETCH_WXCARD_SIGN: {
        action: 'wxcardSign',
        ctrl: 'config'
    },
    //获取会员开卡是提交的用户信息
    FETCH_CARD_SUBMIT_USER_INFO: {
        action: 'fetchSubmitUserCardInfo',
        ctrl: 'user'
    },
    //获取会员开卡是提交的用户信息
    FETCH_CRM_RELATION_USER_INFO: {
        action: 'queryRelationOpenid',
        ctrl: 'user'
    },
    //商城订单支付准备
    SHOP_ORDER_PREPARE_PAY: {
        action: 'preparePay',
        ctrl: 'pay'
    },
    //游戏配置
    GAME_CONFIG: {
        action: 'gameConfig',
        ctrl: 'game'
    },
    //寄存用户门店列表
    RESERVE_SHOP_LIST: {
        action: 'shopList',
        ctrl: 'reserve'
    },
    //寄存用户门店列表
    RESERVE_SHOP_GOODS_LIST: {
        action: 'shopGoodsList',
        ctrl: 'reserve'
    },
    //寄存订单列表
    RESERVE_SHOP_ORDER_LIST: {
        action: 'orderList',
        ctrl: 'reserve'
    },
    //客订订单详情
    RESERVE_SHOP_ORDER_GOODS_LIST: {
        action: 'orderGoodsList',
        ctrl: 'reserve'
    },
    //客订订单支付
    RESERVE_SHOP_ORDER_PAY: {
        action: 'balancePay',
        ctrl: 'reserve'
    },
    //
    RESERVE_SHOP_GOODS_IMAGE: {
        action: 'goodsImageList',
        ctrl: 'reserve'
    },
    //获取店铺列表信息
    FETCHE_SHOP_LIST: {
        action: 'shopList',
        ctrl: 'shop'
    },
    //用户扫码信息
    FETCH_USER_SCAN_CODE_INFO: {
        action: 'scanCodeInfo',
        ctrl: 'user'
    },
};

const StorageConst = {
    CARD_TYPE_CONFIG_STORAGE_KEY: 'card_type_config_storage_key',
    CUSTOMER_SERVICE_KEY: 'customer_service_key',
    EXCHANGE_COUPON_TYPE_LIST_KEY: 'exchange_coupon_type_list_key',
    PAGE_REDIRECT_KEY: 'page_redirect_key',
    USER_SESSION_KEY: 'user_session_key',
    ONLINE_USER_INFO: 'online_user_info',
    ERP_USER_INFO: 'erp_user_info',
    SYSTEM_CONFIG_KEY: 'system_config_key',
    INDEX_CONFIG_KEY: 'index_config_key',
    NOTICE_KEY: 'notice_key',
    OPENID_SERVICE_KEY: 'openid_service_key',
    GAME_SERVICE_KEY: 'game_service_key'
};

export {ApiConst, StorageConst};
