import {Util} from "../../utils/common";
import {UrlUtil} from "../../utils/UrlUtil";

const RequestUtil = {};
RequestUtil.request = function (service, postData, success, fail) {
    success = Util.getFunction(success);
    fail = Util.getFunction(fail);
    getApp().crmApi(
        service,
        postData,
        (res, params, name) => {
            success(res);
        },
        (res, params, name) => {
            if (name == 'success') {
                success(res);
            } else {
                fail(res);
            }
        }
    )
};
RequestUtil.request2 = function (url, postData, success, fail, extParam) {
    success = Util.getFunction(success);
    fail = Util.getFunction(fail);
    /**
     * 设置请求头部
     */
    var h = {'content-type': 'application/json'};
    var app = this;
    wx.request({
        url: url,
        data: postData,
        header: h,
        success: function (res) {
            typeof success == 'function' ? success(res, extParam, 'success') : 1
        },
        fail: function (res) {
            console.log("request fail", res);
            typeof fail == 'function' ? fail(res, extParam, 'fail') : 1
        }
    })
};

RequestUtil.commonRequest = function (obj, postData, success, fail) {
    if (typeof postData != 'object') {
        postData = {};
    }
    postData.action = obj.action;
    postData.ctrl = obj.ctrl;
    console.log(postData);
    const data = {post: JSON.stringify(postData)};
    RequestUtil.request(getApp().apiService.crm.commonRequest, data, success, fail);
};

/**
 * 商城请求
 * @param obj
 * @param postData
 * @param success
 * @param fail
 */
RequestUtil.shopRequest = function (obj, postData, success, fail) {
    if (typeof postData != 'object') {
        postData = {};
    }
    postData.action = obj.action;
    postData.ctrl = obj.ctrl;
    console.log(postData);
    const data = {post: JSON.stringify(postData)};
    const url = UrlUtil.getSassShopUrl() + '?uniacid=' + UrlUtil.getUniacid();
    RequestUtil.request2(url, data, success, fail, {});
};
/**
 * 小程序请求
 * @param obj
 * @param postData
 * @param success
 * @param fail
 */
RequestUtil.wxappRequest = function (obj, postData, success, fail) {
    if (typeof postData != 'object') {
        postData = {};
    }
    postData.action = obj.action;
    postData.ctrl = obj.ctrl;
    const data = {post: JSON.stringify(postData)};
    const url = UrlUtil.getSassWxappUrl() + '?uniacid=' + UrlUtil.getUniacid();
    console.log('url:'+url);
    RequestUtil.request2(url, data, success, fail, {});
};
RequestUtil.gameApiRequest = function (url, postData, success, fail) {
    success = Util.getFunction(success);
    fail = Util.getFunction(fail);
    /**
     * 设置请求头部
     */
    var h = {'content-type': 'application/json'};
    var app = this;
    wx.request({
        url: url,
        data: postData,
        header: h,
        success: function (res) {
            typeof success == 'function' ? success(res, 'success') : 1
        },
        fail: function (res) {
            console.log("request fail", res);
            typeof fail == 'function' ? fail(res, 'fail') : 1
        }
    })
};

export default RequestUtil;
