import RequestUtil from "../api/request";
import {GameConfig} from "../../api/config";
import {UrlUtil} from "../../utils/UrlUtil";
import {ToolUtil} from "../../utils/ToolUtil";
import {Util} from "../../utils/common";
import {CustomStorage} from "../common/StorageUtil";
import {ApiConst, StorageConst} from "../api/const";


function processGameResponse(res, errmsg, succmsg) {
    var statusCode = res.statusCode;
    const response = ToolUtil.getApiResponse();
    let msg = Util.get(res, "data.Message", "");
    if (statusCode >= 200 && statusCode < 300) {
        const content = res.data;
        const objIns = Util.objectExt(content);
        response.setSuccess(objIns.getInt('Statu') == 1).setData(objIns.get('Openids'));
        if (response.isSuccess()) {
            response.setMsg(ToolUtil.getMsg(msg, succmsg));
        } else {
            response.setMsg(ToolUtil.getMsg(msg, errmsg));
        }
        return response;
    }
    response.setSuccess(false).setMsg(ToolUtil.getMsg(msg, errmsg));
    return response;
}

class GameStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.GAME_SERVICE_KEY);
    }
}

const GameStorageUtil = new GameStorage();

class GameConfigBean {

    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getGameList() {
        /**
         * name:
         * type:
         * suffixurl
         */
        return this.data.get('gameList', []);
    }
}

class GameBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getSuffixUrl() {
        return this.data.get('suffixurl', '');
    }

}

class GameService {

    fetchUserType(data, callback) {
        const post = {id: UrlUtil.getUniacid(), extraz: '', phone: data.mobile};

        const url = GameConfig.getGameApiUrl(GameConfig.GAME_API_CONST.FETCH_USER_TYPE);
        RequestUtil.gameApiRequest(url, post, (res) => {
            callback(processGameResponse(res, '用户信息获取失败', ''));
        }, () => {
            callback(ToolUtil.errorResponse("用户信息获取失败"));
        })
    }

    fetchGameList(callback, fetchCache) {
        fetchCache = typeof fetchCache == 'boolean' ? fetchCache : true;
        const response = ToolUtil.getApiResponse();
        if (fetchCache) {
            if (!GameStorageUtil.timeout()) {
                const data = GameStorageUtil.get();
                response.setSuccess(true).setData(new GameConfigBean(data));
                callback(response);
                return;
            }
        }

        RequestUtil.shopRequest(ApiConst.GAME_CONFIG, {}, (res) => {
            const response = ToolUtil.commonResponse2(res, "", "游戏配置获取失败", (result, response) => {
                if (response.isSuccess()) {
                    GameStorageUtil.set(result).setTimeout(8 * 3600 * 1000);
                } else {
                    GameStorageUtil.clear();
                }
                return new GameConfigBean(result);
            });
            callback(response);
        }, (res) => {
            callback(ToolUtil.errorResponse("请求失败"));
        });
    }
}

const gameServiceIns = new GameService();

export function getGameService() {
    return gameServiceIns;
}

export function createGameBean(data){
    return new GameBean(data);
}
export {GameBean};
