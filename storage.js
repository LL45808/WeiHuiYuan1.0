import {ErpUserInfoStorageUtil, OnlineUserInfoStorageUtil} from "./module/user/user";
import {UserSessionStorageUtil} from "./module/common/session";
import {IndexConfigStorageUtil, SystemConfigStorageUtil} from "./module/common/config";

module.exports = function (app) {

    const auth_key = 'auth' //设置AUTH 缓存
    const account_key = 'account' //设置账户信息
    const user_info_key = 'user_info' //设置个人基本信息
    const addr_key = 'addrRoom' //设置行政单位信息
    const indexNav_key = 'indexNav' //设置模块信息
    const share_key = 'share_code' //设置分享码
    const pBarcode = 'pay_barcode'
    const pQrcode = 'pay_qrcode'
    const notice = 'show_notice'
    const login = 'show_login'
    const config = 'show_config'

    /**
     * 读取缓存
     */
    function readData(key, callback) {

        return wx.getStorageSync(key)

    }

    /**
     * 设置缓存
     */
    function setData(key, data) {

        wx.setStorageSync(key, data)

    }

    /**
     * 设置缓存
     */
    function clearData(key) {

        wx.setStorageSync(key, null)

    }

    return {
        /**
         * 设置AUTH 缓存
         */
        setAuth(data) {
            // setData(auth_key, data)
            UserSessionStorageUtil.setSession(data);
        },
        getAuth() {
            return UserSessionStorageUtil.getSession();
            // return readData(auth_key)
        },
        clearAuth() {
            // clearData(auth_key);
            UserSessionStorageUtil.clearSession();
        },

        /**
         * 设置会员基本信息缓存
         */
        setAccount(data) {
            // setData(account_key, data);
            ErpUserInfoStorageUtil.setUserInfo(data);
        },
        getAccount() {
            return ErpUserInfoStorageUtil.getUserInfo();
        },
        clearAccount() {
            ErpUserInfoStorageUtil.clearUserInfo();
        },
        /**
         * 设置个人基本信息缓存
         */
        setUserInfo(data) {
            // setData(user_info_key, data);
            OnlineUserInfoStorageUtil.setUserInfo(data);
        },
        getUserInfo() {
            // return readData(user_info_key)
            return OnlineUserInfoStorageUtil.getUserInfo();
        },
        clearUserInfo() {
            // clearData(user_info_key);
            OnlineUserInfoStorageUtil.clearUserInfo();
        },
        /**
         * 设置地址库缓存
         */
        setAddrRoom(data) {
            setData(addr_key, data)
        },
        getAddrRoom() {
            return readData(addr_key)
        },
        clearAddrRoom() {
            clearData(addr_key);
        },
        /**
         * 设置首页nav缓存
         */
        setIndexNav(data) {
            // setData(indexNav_key, data)
            IndexConfigStorageUtil.set(data);
        },
        getIndexNav() {
            // return readData(indexNav_key)
            return IndexConfigStorageUtil.get();
        },
        clearIndexNav() {
            // clearData(indexNav_key);
            IndexConfigStorageUtil.clear();
        },
        /**
         * 设置share_code缓存
         */
        setShareCode(data) {
            setData(share_key, data)
        },
        getShareCode() {
            return readData(share_key)
        },
        clearShareCode() {
            clearData(share_key);
        },
        /**
         * 支付条形码缓存
         */
        setPayBarcode(data) {
            setData(pBarcode, data)
        },
        getPayBarcode(data) {
            return readData(pBarcode)
        },
        clearPayBarcode() {
            clearData(pBarcode);
        },
        /**
         * 支付二维码缓存
         */
        setPayQrcode(data) {
            setData(pQrcode, data)
        },
        getPayQrcode(data) {
            return readData(pQrcode)
        },
        clearPayQrcode() {
            clearData(pQrcode);
        },
        /**
         * 显示提示
         */
        setShowNotice(data) {
            setData(notice, data)
        },
        getShowNotice() {
            return readData(notice)
        },
        clearShowNotice() {
            clearData(notice);
        },
        /**
         * 登录提示
         */
        setShowLogin(data) {
            setData(login, data)
        },
        getShowLogin() {
            return readData(login)
        },
        clearShowLogin() {
            clearData(login);
        },
        /**
         * 配置
         */
        setShowConfig(data) {
            // setData(config, data)
            SystemConfigStorageUtil.set(data);
        },
        getShowConfig() {
            // return readData(config)
            return SystemConfigStorageUtil.get();
        },
        clearShowConfig() {
            // clearData(config);
            SystemConfigStorageUtil.clear();
        },

    }
}
