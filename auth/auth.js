/**
 * 获取用户信息，并且存入缓存
 *
 * @author meroc Chen <398515393@qq.com> 2017-11-17
 */
import {getSessionService} from "../module/common/session";
import {Toast} from "../utils/Toast";
import {PageRedirectUtil} from "../utils/PageUtil";
import {getUserService} from "../module/user/user";
import {UserUtil} from "../utils/UserUtil";

module.exports = function (app) {
    let session_key = app.storage.getAuth().session_key;
    console.log("session_key:" + session_key);
    checkSession()

    /**
     * 检查小程序SESSION是否过期
     */
    function checkSession() {

        console.log("检查session");
        getSessionService().fetchSession((response) => {
            if (!response.isSuccess()) {
                console.log("session fetch fail");
                return;
            }
            const sessionBean = response.getData();
            if (sessionBean.hasRegister()) {
                getUserService().fetchOnlineUserInfo((response) => {
                }, false);
                getUserService().fetchErpUserInfo((response) => {
                }, false);
            }
        });

        return;
        if (!session_key) {
            console.log("没有session,准备登录");
            login();
        } else {
            console.log("检查session");
            wx.checkSession({
                success: getUserInfo,
                fail: login
            })
        }
    }

    /**
     * 后台登录，获取访问服务器的TOKEN值
     */
    function login() {
        if (app.inAuth) {
            console.log("已经登录");
            return
        } else {
            console.log("未登录");
            app.inAuth = true
        }
        /**
         * 先调取微信API的登录接口获取CODE
         *
         * CODE是微信认证必须传递的参数
         */
        wx.login({
            success: res => {
                /**
                 * 微信API登录成功，再调取后台业务服务器的登录接口，获取TOKEN
                 */
                const data = {code: res.code}
                /**
                 * 调用后台crm微信会员登录接口
                 */
                app.crmApi(app.apiService.crm.wxLogin, data, loginSuccess, loginFail)
            },
            fail: res => {
            }
        })
    }

    /**
     * 登录成功
     */
    function loginSuccess(res) {
        console.log("登录成功", res);
        app.storage.setAuth(res.data.data)
        /**
         * 取消鉴权状态
         */
        app.inAuth = false
        /**
         * 登录成功后，获取用户信息
         */
        getUserInfo()
    }

    /**
     * 登录失败
     */
    function loginFail(res) {

    }

    /**
     * 微信小程序获取用户微信数据
     */
    function getUserInfo() {
        /**
         * 调用微信API, 先获取用户的微信个人资料
         */
        wx.getUserInfo({
            lang: 'zh_CN',              //设置语言为中文
            success: function (res) {   //用户同意微信数据授权
                console.log("微信用户信息", res)
                /**
                 * 将会员资料保存到APP的全局变量中，方便全局调用
                 */
                let data = {
                    token: app.storage.getAuth().token
                }
                const wxUserInfo = res.userInfo;
                var userInfo = {}
                if (wxUserInfo) {
                    if (userInfo.nick_name != wxUserInfo.nickName) {
                        userInfo.nick_name = wxUserInfo.nickName
                        data.nick_name = wxUserInfo.nickName
                    }
                    if (userInfo.pic_head != wxUserInfo.avatarUrl) {
                        userInfo.pic_head = wxUserInfo.avatarUrl
                        data.user_headimg = wxUserInfo.avatarUrl
                    }
                    if (userInfo.sex != wxUserInfo.gender) {
                        userInfo.sex = wxUserInfo.gender
                        data.sex = wxUserInfo.gender
                    }
                }
                /**
                 * 用户同意授权微信数据后，若用户数据有更新，则调用后台业务服务器接口，将用户最新信息更新到服务器
                 */
                if (data.nick_name || data.user_headimg || data.sex) {
                    app.crmApi(app.apiService.crm.memberEdit, data);
                    console.log("更新用户信息", data);
                    app.storage.setUserInfo(userInfo)
                } else {
                    console.log("不更新用户信息");
                }

                backToPage()
            },
            fail: function (res) { //用户拒绝微信数据授权
                backToPage()
            }
        })
    }

    /**
     * 跳转到注册页面
     */
    function member_login() {
        var loginUrl = '/pages/member/login/login'
        /**
         * 获取加载的页面
         */
        var pages = getCurrentPages()
        /**
         * 获取当前页面的对象
         */
        var currentPage = pages[pages.length - 1]
        /**
         * 当前页面url
         */
        if (currentPage && currentPage.route) {
            /**
             * 当前页面已经是登录页面时，不需要跳转
             */
            if (loginUrl != '/' + currentPage.route) {
                loginUrl += '?redirect=' + currentPage.router
                wx.redirectTo({
                    url: loginUrl
                })
            }
        } else {
            /**
             * 跳转登录页面
             */
            wx.redirectTo({
                url: loginUrl
            })
        }
    }

    /**
     * 获取认证完毕，返回页面
     */
    function backToPage() {
        console.log("backToPage");
        app.inAuth = false

        /**
         * 通过不同的页面启动，执行函数名统一的回调
         */
        const pages = getCurrentPages()

        const currentPage = pages[pages.length - 1]

        if (typeof currentPage == 'object' && currentPage.launchCallback && typeof currentPage.launchCallback == 'function') {
            console.log("首页回调:launchCallback-1");
            currentPage.launchCallback()
        } else {
            console.log("-------------------------launchCallback-2");
            var url = currentPage.route
            if (url != 'pages/member/login/login') {

                wx.showToast({
                    title: '您已重新登录！',
                })
                wx.redirectTo({
                    url: '/' + url,
                })
            }


        }

    }

}
