/**
 * 首页提示操作动画组件
 */
Component({
    options: {
        multipleSlots: true//   在组件定义时的选项中启用多slot支持
    },
    properties: {
        extraData: {
            type: Object,
            value: {}
        }
    },
    data: {
        extraData: {}
    },
    /**
     * 组件的方法列表
     */
    methods: {
        /**
         * 注册
         */
        register(e) {
            console.log("register data", this.data);
            this.triggerEvent('register', e);
        }

    }
})
