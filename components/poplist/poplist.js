// pages/components/dialog/downpop/downpop.js
import {Util} from "../../utils/common";

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        list: {
            type: Array,
            value: []
        },
        hidden: {
            type: Boolean,
            value: true
        },
        mKey:{
            type:String,
            value:""
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        hidden: false,
        list: []
    },

    /**
     * 组件的方法列表
     */
    methods: {
        click: function (e) {
            var index = e.currentTarget.dataset.index;
            var item = this.data.list[index];
            this.cancel(e);
            this.triggerEvent('click', item);
        },
        cancel: function (event) {
            this.anim(true);
            // this.triggerEvent('cancel',{});
        },
        hide: function () {
            this.cancel();
        },
        show: function () {
            this.anim(false);
        },
        updateData: function (params) {
            Util.updateComponentData(this, params);
        },
        anim: function (hidden) {
            if (hidden) {
                this.updateData({
                    hidden: true
                });
                return;
            }
            var that = this;
            var animation = wx.createAnimation({
                duration: 200,
                timingFunction: 'linear',
            });
            that.animation = animation;
            animation.translateY(800).step();

            that.updateData({
                animationData: animation.export(),
                hidden: hidden
            });
            animation.translateY(0).step();
            that.updateData({
                animationData: animation.export()
            });
        }
    }
});
