/**
 * 首页提示操作动画组件
 */
Component({

  options: {

    multipleSlots: true//   在组件定义时的选项中启用多slot支持

  },

  /**
   * 组件的方法列表
   */
  methods: {

    /**
     * 微信授权信息
     */
    accredit(e) {
      var data = {}
      if (e.detail.errMsg == 'getUserInfo:ok') {
        data = e.detail
      }
      this.triggerEvent('accredit', { data })
    }

  }
})
