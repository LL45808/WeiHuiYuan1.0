/**
 * 首页提示操作动画组件
 */
Component({

  options: {

    multipleSlots: true//   在组件定义时的选项中启用多slot支持

  },

  /**
   * 组件的属性列表
   */
  properties: {


  },

  /**
   * 组件的初始数据
   */
  data: {
    
  },
  
  /**
   * 组件的方法列表
   */
  methods: {

    /**
     * 微信授权手机号
     */
    getPhoneNumber: function (e) {
      var data = {}
      if (e.detail.encryptedData) {
        data.iv = e.detail.iv
        data.encryptedData = e.detail.encryptedData
      }
      this.triggerEvent('accredit', { data })
    },

  }
})
