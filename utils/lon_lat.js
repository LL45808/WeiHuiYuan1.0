import {TimeUtil} from "./TimeUtil";
import StringUtil from "./StringUtil";

const CommonUtil = {};

CommonUtil.getPayCode = function (cardId) {
    let time = TimeUtil.getNowDate('Y-m-d h:i:s');
    time = String(time).replace(/[:\- ]/g, '');
    // console.log('time:' + time);
    const code = cardId + time.substring(4, 12);
    let clen = code.length;
    let tmpCode = '';
    for (let i = 0; i < clen; i++) {
        const dec = code[i];
        switch (dec) {
            case '1':
                tmpCode += '3';
                break;
            case '2':
                tmpCode += '5';
                break;
            case '3':
                tmpCode += '9';
                break;
            case '4':
                tmpCode += '0';
                break;
            case '5':
                tmpCode += '8';
                break;
            case '6':
                tmpCode += '4';
                break;
            case '7':
                tmpCode += '7';
                break;
            case '8':
                tmpCode += '6';
                break;
            case '9':
                tmpCode += '1';
                break;
            case '0':
                tmpCode += '2';
                break;
        }
    }
    // console.log('tmpCode:' + tmpCode);
    tmpCode = StringUtil.reverse(tmpCode);
    // console.log('tmpCode:' + tmpCode);
    let newCode = '';
    for (let k = 0; k < clen; k += 2) {
        newCode += tmpCode[k + 1] + tmpCode[k];
    }
    return newCode;
};

function Rad(d) {
    return d * Math.PI / 180.0;//经纬度转换成三角函数中度分表形式。
}

CommonUtil.getDistance = function (origin, target) {
    var lat1 = parseFloat(origin.lat);
    var lng1 = parseFloat(origin.lng);
    var lat2 = parseFloat(target.lat);
    var lng2 = parseFloat(target.lng);

    var radLat1 = Rad(lat1);
    var radLat2 = Rad(lat2);
    var a = radLat1 - radLat2;
    var b = Rad(lng1) - Rad(lng2);
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
        Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378.137;// EARTH_RADIUS;
    s = Math.round(s * 10000) / 10000; //输出为公里
    //s=s.toFixed(4);
    return s;
}

export default CommonUtil;
