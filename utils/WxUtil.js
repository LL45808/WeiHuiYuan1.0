import {Util} from "./common";
import ValidateUtil from "./ValidateUtil";
import {UserConfigUtil, UserUtil} from "./UserUtil";

const WxUtil = {};

const app = getApp();

WxUtil.wxCard = function (callback) {
    // wxeb490c6f9b154ef9
    const obj = WxUtil.getWxCardInfo();
    wx.navigateToMiniProgram({
        appId: 'wxeb490c6f9b154ef9', //固定为此 appid，不可改动
        extraData: obj.data, // 包括 encrypt_card_id, outer_str, biz三个字段，须从 step3 中获得的链接中获取参数
        success: function (res) {
            console.log("success", res);
        },
        fail: function (res) {
            console.log("fail", res);
        },
        complete: function () {
        }
    })
};
WxUtil.cardStatus = function (valid, hasBind, data) {
    return {
        valid: valid,
        hasBind: hasBind,
        data: data
    }
};
WxUtil.getWxCardInfo = function () {
    const account = app.storage.getAccount();
    const hasWxConfig = Util.get(account, 'has_wx_card_config', null);
    if (hasWxConfig == null || !hasWxConfig) {
        return WxUtil.cardStatus(false, false, {});
    }
    const bindWxCard = Util.get(account, "bind_wx_card", null);

    if (bindWxCard == null) {
        return WxUtil.cardStatus(false, false, {});
    }
    const wxCardConfig = Util.get(account, "wx_card_config", null);
    if (!Util.isObj(wxCardConfig)) {
        return WxUtil.cardStatus(false, false);
    }

    const encryptCardId = Util.get(wxCardConfig, 'encrypt_card_id', '');
    const outerStr = Util.get(wxCardConfig, 'outer_str', '');
    const biz = Util.get(wxCardConfig, 'biz', '');
    const cardId = Util.get(wxCardConfig, 'card_id', '');
    const openid = Util.get(wxCardConfig, 'openid', '');
    const code = '114006979211';
    if (ValidateUtil.isEmpty(encryptCardId) || Util.isEmpty(outerStr) || Util.isEmpty(biz)) {
        // return WxUtil.cardStatus(false, false, {});
    }

    if (typeof bindWxCard == 'boolean' && bindWxCard) {
        return WxUtil.cardStatus(true, true, {});
    }
    return WxUtil.cardStatus(true, false, {
        encrypt_card_id: encryptCardId,
        outer_str: outerStr,
        biz: biz
    });
};
/**
 * 是否能检查用户门店信息
 * @returns {boolean}
 */
WxUtil.enableCheckUserShopInfo = function () {
    if (!WxUtil.enableUpdateShopInfo()) {
        return false;
    }
    const hasCheckShopInfo = UserConfigUtil.hasCheckUserShopInfo();
    return !hasCheckShopInfo;
};

/**
 * 判断门店是否需要更新
 * @returns {boolean}
 */
WxUtil.enableUpdateShopInfo = function () {
    const account = UserUtil.getAccount();
    const shopData = Util.get(account, 'shopData', null);
    if (shopData == null) {
        return false;
    }
    const shopCode = Util.get(shopData, 'shopCode', '');
    if (!ValidateUtil.isEmpty(shopCode)) {
        return false;
    }
    const enableUpdateShop = Util.get(shopData, 'enableUpdateShop', false);
    if (!enableUpdateShop) {
        return false;
    }
    return true;
};

WxUtil.updateUserBelongShop = function (args) {
    const account = args.account;
    const lat = args.lat;
    const lng = args.lng;

    if (UserConfigUtil.hasCheckUserShopInfo()) {
        return;
    }
    if (!WxUtil.enableUpdateShopInfo()) {
        return;
    }

    const cardId = Util.get(account, 'card_id', '');
    const checkCallback = () => {
        UserConfigUtil.setCheckUserShopInfo(true);
    };
    app.crmApi(
        app.apiService.crm.updateUserBelongShop, {
            token: UserUtil.getToken(),
            card_id: cardId,
            lat: lat,
            lng: lng
        },
        res => {
            account.shopData.hasCheckShopInfo = true;
            app.storage.setAccount(account);
            // console.log("success", res);
            checkCallback();
        },
        res => {
            // console.log("error", res);
            checkCallback();
        }
    )
}

export default WxUtil;
