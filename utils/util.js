function renderTime(date) {

    var arr = date.split(/[- : \/]/)
    var Year = parseInt(arr[0])
    var Month = parseInt(arr[1])
    var Day = parseInt(arr[2])
    var Hours = parseInt(arr[3])
    var Minutes = parseInt(arr[4])
    var Seconds = parseInt(arr[5])
    var CurrentDate = "";
    CurrentDate += Year + "-";
    if (Month >= 10) {
        CurrentDate += Month + "-";
    }
    else {
        CurrentDate += "0" + Month + "-";
    }
    if (Day >= 10) {
        CurrentDate += Day;
    }
    else {
        CurrentDate += "0" + Day;
    }
    if (Hours < 10) {
        Hours = "0" + Hours;
    }
    if (Minutes < 10) {
        Minutes = "0" + Minutes;
    }
    if (Seconds < 10) {
        Seconds = "0" + Seconds;
    }
    return CurrentDate + " " + Hours + ":" + Minutes + ":" + Seconds;

}

function renderPosTime(data) {

    var time_array = data.split('T')

    var time_two_array = time_array[1].split('.')

    return time_array[0] + ' ' + time_two_array[0]

}
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
//返回  小时：分钟+10  用于预约至少10分钟后
const timeNow = date => {
  const hour = date.getHours()
  const minute = date.getMinutes()
  return [hour, minute].map(formatNumber).join(':')
}
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

module.exports = {
    renderTime: renderTime,
    renderPosTime: renderPosTime,
  formatTime: formatTime,
  timeNow: timeNow
}
