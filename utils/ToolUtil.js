import {Util} from "./common";
import {TimeUtil} from "./TimeUtil";

const Promise = require('../utils/promise').Promise;

function ApiResponse() {
    var params = {msg: '', data: null, success: false, code: '', content: ''};

    this.setCode = function (code) {
        params.code = code;
        return this;
    };
    this.getCode = function () {
        return params.code;
    };
    //业务成功
    this.setSuccess = function (success) {
        params.success = success;
        return this;
    };
    this.isSuccess = function () {
        return params.success;
    };
    this.setMsg = function (msg) {
        params.msg = msg;
        return this;
    };
    this.getMsg = function () {
        return params.msg;
    };
    this.setData = function (data) {
        params.data = data;
        return this;
    };
    this.getData = function () {
        return params.data;
    };
    this.get = function (name, def) {
        return Util.get(params.data, name, def);
    }
    this.setContent = function (content) {
        params.content = content;
        return this;
    };
    this.getContent = function () {
        return params.content;
    };
}

const ToolUtil = {};
ToolUtil.getSex = function (sex) {
    return sex == 1 ? '先生' : '女士';
};
ToolUtil.getMsg = function (msg, defaultMsg) {
    return Util.isEmpty(msg) ? defaultMsg : msg;
};
ToolUtil.getApiResponse = function () {
    var data = new ApiResponse();
    return data;
};
ToolUtil.errorResponse = function (msg) {
    var data = new ApiResponse();
    data.setSuccess(false);
    data.setMsg(msg);
    return data;
};

/**
 * @param res 微信success回调接口的res
 * @param succmsg
 * @param errmsg
 * @returns {ApiResponse}
 */
ToolUtil.commonResponse = function (res, succmsg, errmsg) {
    var response = new ApiResponse();
    var statusCode = res.statusCode;
    if (statusCode >= 200 && statusCode < 300) {
        var ret = Util.get(res, "data.ret", null);
        if (ret != null) {
            if (ret == 200) {
                let msg = Util.get(res, "data.msg", "");
                response.setSuccess(true).setMsg(ToolUtil.getMsg(msg, succmsg)).setData(res.data);
                return response;
            }
        }
    }
    let msg = Util.get(res, "data.msg", "");
    response.setSuccess(false).setMsg(ToolUtil.getMsg(msg, errmsg));
    return response;
};
ToolUtil.commonResponse2 = function (res, succmsg, errmsg, processData) {
    var response = new ApiResponse();
    var statusCode = res.statusCode;
    if (statusCode >= 200 && statusCode < 300) {
        var ret = Util.get(res, "data.ret", null);
        if (ret != null) {
            if (ret == 200) {
                let msg = Util.get(res, "data.msg", "");
                let result = Util.get(res, "data.data.result", {});
                msg = ToolUtil.getMsg(msg, Util.get(res, "data.data.msg", ""));
                console.log("msg", msg);
                const success = Util.get(res, "data.data.status", "error") == 'success';
                const code = Util.get(res, 'data.data.code', '');
                response.setCode(code);
                if (typeof processData == 'function') {
                    result = processData(result, response);
                }
                response.setSuccess(success).setMsg(ToolUtil.getMsg(msg, succmsg)).setData(result).setContent(res.data);
                return response;
            }
        }
    }
    let msg = Util.get(res, "data.msg", "");
    response.setSuccess(false).setMsg(ToolUtil.getMsg(msg, errmsg));
    return response;
};

class TimeCountDown {
    complete = null;
    callback = null;
    endTime = null;
    identify = null;
    interval = 60;
    stop = true;//是否正在运行

    invokeCallback(value) {
        this.callback(value);
    }

    invokeComplete() {
        this.identify = null;
        this.stop = true;
        this.complete();
    }

    countdown() {
        const nowTime = TimeUtil.getNowTime();
        if (nowTime > this.endTime) {
            this.invokeComplete();
            return;
        }
        const surplus = this.endTime - nowTime;
        let value = Math.ceil(surplus / 1000);
        value = value <= 0 ? 0 : value;
        this.invokeCallback(value);
        if (value > 0) {
            this.identify = setTimeout(() => {
                this.countdown();
            }, 1000);
        }
    }

    start() {
        if (!this.stop) {
            return false;
        }
        this.stop = false;
        this.endTime = TimeUtil.getNowTime() + this.interval;
        this.countdown();
        return true;
    }

    isStop() {
        return this.stop;
    }

    init(ttl, callback, complete) {
        this.interval = ttl * 1000;
        this.complete = Util.getFunction(complete);
        this.callback = Util.getFunction(callback);
    }

    cancel() {
        if (this.identify != null) {
            clearTimeout(this.identify);
            this.invokeComplete();
        }
    }
}

/**
 * 秒倒计时
 * @param time
 * @param callback 每秒回调
 * @param complete 倒计时完成
 * @returns {TimeCountDown}
 */
ToolUtil.secondsCountdown = function (time, callback, complete) {
    const ins = new TimeCountDown();
    ins.init(time, callback, complete);
    return ins;
};

ToolUtil.fetchWxUserInfo = function (callback) {
    wx.getUserInfo({
        withCredentials: true,
        lang: 'zh_CN',
        success: function (res) {
            console.log(res);
            const encryptedData = res.encryptedData;
            const iv = res.iv;
            callback({encryptedData: encryptedData, iv: iv});
        },
        fail: function (res) {
            console.log(res);
            const encryptedData = ''
            const iv = ''
            callback({encryptedData: encryptedData, iv: iv});
        }
    })
};

class TaskManage2 {
    callbackList = [];

    then(callback) {
        this.callbackList.push(callback);
        return this;
    }

    execute() {
        const callback = this.callbackList.shift();
        if (callback != null) {
            callback(() => {
                this.execute();
            });
        }
    }
}

/**
 * 按顺序执行
 * @param executeCallback
 * @returns {TaskManage2}
 */
ToolUtil.thenSequence = function (executeCallback) {
    const task = new TaskManage2();
    task.then(executeCallback);
    return task;
};

ToolUtil.testPro = function () {

    /* const promiseList = [];
     var promise = new Promise(function (resolve, reject) {
         resolve();
     });
     promiseList.push(promise);
     var promise2 = new Promise(function (resolve, reject) {
         resolve();
     });
     promiseList.push(promise2);
     console.log('promiseList', promiseList);
     Promise.all(promiseList).then(() => {
         console.log("------1");
     });*/
};

class TaskManage {
    callbackList = [];
    then(callback, params) {
        this.callbackList.push({callback: callback, params: params});
        return this;
    }
    execute() {
        if (this.callbackList.length == 0) {
            return;
        }
        const promiseList = [];
        const lastData = this.callbackList.pop();

        const lastCallback = lastData.callback;
        for (let i = 0; i < this.callbackList.length; i++) {
            const data = this.callbackList[i];
            const callback = data.callback;
            const params = data.params;
            promiseList.push(new Promise(function (resolve, reject) {
                callback(resolve, params);
            }));
        }
        if (promiseList.length > 0) {
            Promise.all(promiseList).then(function (results) {
                lastCallback();
            });
        } else {
            lastCallback();
        }
    }
}

/**
 * 多个请求任务异步同步执行
 * 该工具至少需要2个then才会生效,最后一个then用于通知多任务完成
 * @param executeCallback
 * @returns {TaskManage}
 */
ToolUtil.then = function (executeCallback, params) {
    const task = new TaskManage();
    task.then(executeCallback, params);
    return task;
};
ToolUtil.initAsync=function(){
    return new TaskManage();
};

ToolUtil.sleep = function (numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
        now = new Date();
        if (now.getTime() > exitTime)
            return;
    }
};
ToolUtil.resort = function (data, field, order) {
    var sortFunc = function (a, b) {
        //desc
        if (order == 'asc') {
            return parseInt(a[field]) - parseInt(b[field]);
        } else {
            return parseInt(b[field]) - parseInt(a[field]);
        }
    }
    return data.sort(sortFunc);
};

ToolUtil.multiSortNum = function (data, fields) {
    var len = 0;
    // var len = fields.length - 1;
    for (const key in fields) {
        len++;
    }
    var multiSortFunc = function (a, b) {
        var index = 0;
        for (const field in fields) {
            index++;
            const order = fields[field];
            if (index == len || a[field] !== b[field]) {
                //最后一个不管相等或不相等都计算
                //不相等直接使用字段比较
                if (order == 'asc') {
                    return parseInt(a[field]) - parseInt(b[field]);
                } else {
                    return parseInt(b[field]) - parseInt(a[field]);
                }
            } else {
                continue;
            }
        }
    };
    return data.sort(multiSortFunc);
};
export {ToolUtil};
