import {UrlUtil} from "./UrlUtil";
import {Util} from "./common";
import {TimeUtil} from "./TimeUtil";
import {StorageConst} from "../module/api/const";
import {CustomStorage} from "../module/common/StorageUtil";

class PageRedirectBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        console.log("data", data);
        this.data = Util.objectExt(data);
    }

    getFromSource() {
        return this.data.get('data.fromSource', '');
    }

    getQuery() {
        return this.data.get('data', '');
    }

    getPage() {
        return this.data.get('page', '');
    }
}

class PageRedirectStorage extends CustomStorage {
    constructor() {
        super();
        this.setKey(StorageConst.PAGE_REDIRECT_KEY);
    }

}

const PageRedirectStorageUtil = new PageRedirectStorage();

const sourceMap = [];
sourceMap['donateReceiveCoupon'] = {page: 'shareindex', keys: {couponCode: 'couponCode'}, type: 'share'};
sourceMap['erpCoupon'] = {page: 'erpReceiveCoupon', keys: {id: 'topicId'}, type: 'scene'};

//页面链接管理,管理从其它地方进入小程序，需要注册，注册完成之后重新进入点击的页面
class PageRedirect {

    /**
     * app.js中执行改函数，获取来源信息
     * @param options
     */
    parse(options) {
        if (!Util.isObj(options)) {
            return;
        }
        if (options.redirect) {
            //重定向不需要处理
            return;
        }
        if (options.scene) {
            this.parseScene(options);
        } else {
            this.parseShare(options);
        }
    }

    parseShare(options) {
        const source = Util.get(options, 'fromSource', '');
        if (sourceMap[source]) {
            const obj = sourceMap[source];
            const keyList = obj.keys;
            const page = obj.page;
            const data = {fromSource: source};
            for (const key in keyList) {
                const actKey = keyList[key];
                if (options[key]) {
                    data[actKey] = options[key];
                }
            }
            PageRedirectStorageUtil.set({source: source, data: data, page: page});
        }
    }

    /**
     * 二维码扫码场景进入处理
     * @param options
     */
    parseScene(options) {
        const scene = String(decodeURIComponent(options.scene));
        const params = scene.split("&");
        const query = {};
        for (let i = 0; i < params.length; i++) {
            const kv = String(params[i]).split('=');
            if (kv.length == 2) {
                query[kv[0]] = kv[1];
            }
        }
        const source = Util.get(query, 'type', '');
        if (sourceMap[source]) {
            const obj = sourceMap[source];
            const keyMap = obj.keys;
            const page = obj.page;
            const data = {fromSource: source};
            for (const key in keyMap) {
                const actualKey = keyMap[key];
                if (query[key]) {
                    data[actualKey] = query[key];
                }
            }
            PageRedirectStorageUtil.set({source: source, data: data, page: page});
        }
    }

    getBean() {
        const config = PageRedirectStorageUtil.get();
        return new PageRedirectBean(config);
    }

    getData() {
        return PageRedirectStorageUtil.get();
    }

    clear() {
        PageRedirectStorageUtil.clear();
    }

    redirect(callback) {
        const config = PageRedirectStorageUtil.get();
        console.log("-------config", config);
        if (config != null) {
            //清空从定向信息
            const bean = this.getBean();
            //重定向
            const page = bean.getPage();
            // console.log("page:" + page);
            const query = bean.getQuery();
            query.redirect = true;
            PageRedirectStorageUtil.clear();
            PageUtil.navigateTo(page, query);
        } else {
            if (typeof callback == 'function') {
                callback();
            }
        }
    }
}

const PageRedirectUtil = new PageRedirect();

const PageUtil = {};

//控制每次导航间隔时间
class TimeInfo {

    startTime = TimeUtil.getNowTime();
    stop = true;
    interval = 2000;
    timeout = 5000;

    reset() {
        this.stop = true;
    }

    invoke(ttl, timeout) {
        this.interval = ttl * 1000;
        if (timeout != null) {
            this.timeout = timeout * 1000;
        }
        if (!this.stop) {
            return;
        }
        this.stop = false;
        setTimeout(() => {
            //如果
        }, this.timeout);
    }
}

const timeMap = [];
PageUtil.hasPage = function (targetPage) {
    const pages = getCurrentPages();
    targetPage = String(UrlUtil.getPageUrl(targetPage, null)).substring(1);
    for (const key in pages) {
        const page = pages[key].route;
        if (targetPage == page) {
            return true;
        }
    }
    return false;
};
PageUtil.getPage = function () {
    const pages = getCurrentPages();
    if (pages.length > 0) {
        return pages[pages.length - 1];
    } else {
        return null;
    }
};
PageUtil.navigateTo = function (url, params) {
    var url = UrlUtil.getPageUrl(url, params);
    console.log("url", url);
    wx.navigateTo({
        url: url,
        success: function (e) {
            console.log("success", e);
        },
        fail: function (e) {
            console.log("fail", e);
            // PageUtil.redirectTo(url);
        }, complete: function (e) {
            console.log("complete", e);
        }
    });
};
PageUtil.redirectTo = function (url, params) {
    url = UrlUtil.getPageUrl(url, params);
    wx.redirectTo({
        url: url,
        success: function (e) {
            console.log("success", e);
        },
        fail: function (e) {
            console.log("fail", e);
        }, complete: function (e) {
            console.log("complete", e);
        }
    });
};
PageUtil.toPage = function (url, list) {
    PageUtil.navigateTo(url, list);
};
PageUtil.reLaunch = function (url, list) {
    if (!Util.isArray(list)) {
        list = [];
    }
    url = UrlUtil.getPageUrl(url);
    if (list.length > 0) {
        url += "?" + list.join("&");
    }
    PageUtil.redirectTo(url);
};
PageUtil.back = function () {
    wx.navigateBack({
        success: function (res) {
            console.log("success", res);
        },
        fail: function (res) {
            console.log("success", res);
        }
    });
};
export {PageRedirectUtil};
export default PageUtil;
