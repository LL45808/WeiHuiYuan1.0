function getPageUrl(page, params) {
    var map = [];
    map['giftdetail'] = '/pages/member/birthgift/giftdetail/giftdetail';
    map['shareindex'] = '/pages/coupon/shareindex/shareindex';
    map['index'] = '/pages/index/index';
    map['couponlist'] = '/pages/coupon/coupon';
    map['erpReceiveCoupon'] = '/pages/member/coupon/receive/receive';//领取erp优惠券
    map['register'] = '/pages/member/login/login';
    map['attention'] = '/pages/member/attention/attention';
    map['gamelist'] = '/pages/game/list/list';
    map['game'] = '/pages/game/game/game';
    map['web'] = '/pages/web/web';  
    var path = map[page] != null ? map[page] : page;
    if (params) {
        var list = [];
        for (const key in params) {
            var v = params[key];
            list.push(key + '=' + v);
        }
        path += '?' + list.join("&");
    }
    return path;
};

let baseUrl = null;

function getRequestBaseUrl() {
    if (baseUrl == null) {
        baseUrl = getApp().apiUrls.crm;
    }
    var pos = String(baseUrl).search('foewin');
    return String(baseUrl).substring(0, pos - 1);
}

function getUniacid() {
    return getApp().apiUrls.uniacid;
}

//一个项目请求不会改变，可以直接保存为变量

function getSassRequestUrl() {
    return getRequestBaseUrl() + '/jqcrm/forerp';
}

function getSassShopUrl() {
    return getSassRequestUrl() + '/shop/index.php';
}
function getSassWxappUrl() {
    return getSassRequestUrl() + '/wxapp/index.php';
}
export const UrlUtil = {
    getPageUrl: getPageUrl,
    getRequestBaseUrl: getRequestBaseUrl,
    getUniacid: getUniacid,
    getSassRequestUrl: getSassRequestUrl,
    getSassShopUrl: getSassShopUrl,
    getSassWxappUrl:getSassWxappUrl,
    toPage(url) {
        wx.navigateTo({
            url: url,
            success: function (e) {
                console.log("success", e);
            },
            fail: function (e) {
                console.log("fail", e);
            }, complete: function (e) {
                console.log("complete", e);
            }
        });
    }
};
