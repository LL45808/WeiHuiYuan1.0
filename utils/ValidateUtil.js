const ValidateUtil = {};
ValidateUtil.isMobile = function (value) {

    var mobile = /^1[0-9]\d{9}$/;
    return (mobile.test(value));
};
ValidateUtil.trim = function (a) {

    if (typeof (a) == "string") {
        return a.replace(/^\s*|\s*$/g, "");
    } else {
        return a;
    }
};
ValidateUtil.isArray = function (object) {
    return Object.prototype.toString.call(object) === '[object Array]';
},
    ValidateUtil.isEmpty = function (a) {

        switch (typeof (a)) {
            case "string":
                return ValidateUtil.trim(a).length == 0 ? true : false;
                break;
            case "number":
                return a == 0;
                break;
            case "object":
                return a == null;
                break;
            case "array":
                return a.length == 0;
                break;
            default:
                return true;
        }
    };

ValidateUtil.checkRealname = function (username) {
    var filterRule = /^[A-Za-z0-9\u4e00-\u9fa5]{1,20}$/;
    if (!filterRule.test(username)) {
        return false;
    } else {
        return true;
    }
}
export default ValidateUtil;
