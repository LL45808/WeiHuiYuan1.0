if (!DateFormat) {
    // Based on http://jacwright.com/projects/javascript/date_format
    var DateFormat = function (sFmt) {

        var fmt = sFmt;
        var _this = this;
        var replaceChars = {
            longMonths: ["January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October",
                "November", "December"],
            shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                "Aug", "Sep", "Oct", "Nov", "Dec"],
            longDays: ["Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday"],
            shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],

            // Day
            d: function (date) {

                return (date.getDate() < 10 ? '0' : '') + date.getDate();
            },
            D: function (date) {

                return replaceChars.shortDays[date.getDay()];
            },
            j: function (date) {

                return date.getDate();
            },
            l: function (date) {

                return replaceChars.longDays[date.getDay()];
            },
            N: function (date) {

                return date.getDay() + 1;
            },
            S: function (date) {

                return (date.getDate() % 10 == 1 && date.getDate() != 11 ? 'st'
                    : (date.getDate() % 10 == 2 && date.getDate() != 12 ? 'nd'
                        : (date.getDate() % 10 == 3
                        && date.getDate() != 13 ? 'rd' : 'th')));
            },
            w: function (date) {

                return date.getDay();
            },
            z: function (date) {

                return "Not Yet Supported";
            },
            // Week
            W: function (date) {

                return "Not Yet Supported";
            },
            // Month
            F: function (date) {

                return replaceChars.longMonths[date.getMonth()];
            },
            m: function (date) {

                return (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
            },
            M: function (date) {

                return replaceChars.shortMonths[date.getMonth()];
            },
            n: function (date) {

                return date.getMonth() + 1;
            },
            t: function (date) {

                return "Not Yet Supported";
            },
            // Year
            L: function (date) {

                return (((date.getFullYear() % 4 == 0) && (date.getFullYear() % 100 != 0)) || (date
                    .getFullYear() % 400 == 0)) ? '1' : '0';
            },
            o: function (date) {

                return "Not Supported";
            },
            Y: function (date) {

                return date.getFullYear();
            },
            y: function (date) {

                return ('' + date.getFullYear()).substr(2);
            },
            // Time
            a: function (date) {

                return date.getHours() < 12 ? 'am' : 'pm';
            },
            A: function (date) {

                return date.getHours() < 12 ? 'AM' : 'PM';
            },
            B: function (date) {

                return "Not Yet Supported";
            },
            g: function (date) {

                return date.getHours() % 12 || 12;
            },
            G: function (date) {

                return date.getHours();
            },
            h: function (date) {

                return ((date.getHours() % 12 || 12) < 10 ? '0' : '')
                    + (date.getHours() % 12 || 12);
            },
            H: function (date) {

                return (date.getHours() < 10 ? '0' : '') + date.getHours();
            },
            i: function (date) {

                return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
            },
            s: function (date) {

                return (date.getSeconds() < 10 ? '0' : '') + date.getSeconds();
            },
            // Timezone
            e: function (date) {

                return "Not Yet Supported";
            },
            I: function (date) {

                return "Not Supported";
            },
            O: function (date) {

                return (-date.getTimezoneOffset() < 0 ? '-' : '+')
                    + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0'
                        : '')
                    + (Math.abs(date.getTimezoneOffset() / 60)) + '00';
            },
            P: function (date) {

                return (-date.getTimezoneOffset() < 0 ? '-' : '+')
                    + (Math.abs(date.getTimezoneOffset() / 60) < 10 ? '0'
                        : '')
                    + (Math.abs(date.getTimezoneOffset() / 60))
                    + ':'
                    + (Math.abs(date.getTimezoneOffset() % 60) < 10 ? '0'
                        : '')
                    + (Math.abs(date.getTimezoneOffset() % 60));
            },
            T: function (date) {

                var m = date.getMonth();
                date.setMonth(0);
                var result = date.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,
                    '$1');
                date.setMonth(m);
                return result;
            },
            Z: function (date) {

                return -date.getTimezoneOffset() * 60;
            },
            // Full Date/Time
            c: function (date) {

                return date.format("Y/m/d") + "T" + date.format("H:i:sP");
            },
            r: function (date) {

                return date.toString();
            },
            U: function (date) {

                return date.getTime() / 1000;
            }
        };

        return {
            format: function (oDate) {

                var out = '';
                for (var i = 0; i < fmt.length; i++) {
                    var c = fmt.charAt(i);
                    if (replaceChars[c]) {
                        out += replaceChars[c](oDate);
                    } else {
                        out += c;
                    }
                }
                return out;
            }
        };
    };

    DateFormat.getDateInstance = function (format) {

        format = format == null ? "Y/m/d h:i:s" : format;
        return new DateFormat(format);
    };
}


var TimeUtil = {};
TimeUtil.getCompatibleDateFormat = function (str) {
    return String(str).replace(/-/g, "/");
};
TimeUtil.getTimestamp = function (time) {
    if (time == null) {
        return new Date().getTime();
    }
    var str = String(time);
    if (str.length == 10) {
        time = str + '000';
        time = parseInt(time)
    }
    return time;
};
/**
 * 格式化时间戳'Y-m-d H:i:s'
 */
TimeUtil.timestampFormat = function (time, format) {
    // var format = TimeUtil.getCompatibleDateFormat(_format);
    var str = String(time);
    if (str.length == 10) {
        time = str + '000';
        time = parseInt(time)
    }
    format = String(format);
    var date = new Date();
    date.setTime(time);
    return DateFormat.getDateInstance(format).format(date);
};
//转化为时间戳
TimeUtil.transToTimestramp = function (date) {
    return Date.parse(new Date(TimeUtil.getCompatibleDateFormat(date)));
};
/**
 * 获取当前时间
 * @param format
 * @returns {*}
 */
TimeUtil.getNowDate = function (format) {
    format = format == null ? 'Y/m/d' : format;
    return TimeUtil.timestampFormat(TimeUtil.getTimestamp(), format);
};
TimeUtil.getNowTime = function () {
    return new Date().getTime();
}

TimeUtil.test = function () {
    console.log('test-----');

    var obj = TimeUtil.getMonthInterval();
    console.log(TimeUtil.timestampFormat(obj.start, 'Y/m/d'));
    console.log(TimeUtil.timestampFormat(obj.end, 'Y/m/d'));
};

TimeUtil.formatTimeInterval = function (obj) {
    var s = TimeUtil.timestampFormat(obj.start, 'Y/m/d');
    var e = TimeUtil.timestampFormat(obj.end, 'Y/m/d');
    return {start: s, end: e};
};

/**
 * 获取interval间隔的时间区间
 */
TimeUtil.getDayInterval = function (time, interval) {
    if (time == null) {
        time = TimeUtil.getTimestamp();
    }
    var dateInfo = TimeUtil.getDateInfo(time);
    var year = dateInfo.year;
    var month = dateInfo.month;
    var day = dateInfo.day;
    var start = new Date(year, month, day).getTime();
    var e = start + (interval * 86400000);
    if (interval > 0) {
        return {start: start, end: e};
    } else {
        return {end: start, start: e};
    }
};

/**
 * 获取每周的时间区间
 * @param time
 * @returns {{start: number, end: *}}
 */
TimeUtil.getWeekInterval = function (time) {

    var d = TimeUtil.getDateInfo(time).weekday - 1;
    var interval = d * 86400000;
    var nowDate = TimeUtil.timestampFormat(time, 'Y/m/d');
    var nowTime = TimeUtil.transToTimestramp(nowDate);
    var start = nowTime - interval;
    var end = TimeUtil.getNowTime();
    return {start: start, end: end};
};

/**
 * 获取当月的时间区间
 */
TimeUtil.getMonthInterval = function (_year, _month) {
    var d = TimeUtil.getDateInfo(TimeUtil.getNowTime());
    var year = _year != null ? _year : d.year;
    var month = _month != null ? _month : d.month;


    var start = new Date(year, month, 1).getTime();
    var interval = TimeUtil.getMonthDays(start);
    var e = new Date(year, month, interval).getTime();
    if (interval > 0) {
        return {start: start, end: e};
    } else {
        return {end: start, start: e};
    }
};

/**
 * 获取某月份的天数
 * @param time
 */
TimeUtil.getMonthDays = function (time) {
    var newDate = new Date();
    newDate.setTime(time);
    var year = newDate.getFullYear();
    var month = newDate.getMonth();
    var d = new Date(year, month, 0);
    return d.getDate();
}
/**
 *
 * @param time
 * @returns {{year: number, month: number, day: number}}
 */
TimeUtil.getDateInfo = function (time) {
    var newDate = new Date();
    newDate.setTime(time);
    return {
        year: newDate.getFullYear(),
        month: parseInt(newDate.getMonth()) + 1,
        day: newDate.getDate(),
        weekday: newDate.getDay()
    };
};
TimeUtil.monthInterval = function (time, interval = 0) {

    var d1 = TimeUtil.getDateInfo(time);
    var year = d1.year;
    var month = d1.month;
    // console.log('year:'+year+"  month:"+month);
    var ttt = TimeUtil.getMonthInterval(year, month);
    interval = Math.abs(interval);
    var firstTime = 0;
    for (var i = 0; i < interval; i++) {
        //获取时间戳月初的时间戳
        // console.log("for:"+year + '-' + (month+1) + '-1');
        firstTime = Date.parse(new Date(year + '/' + (month + 1) + '/1'));
        // firstTime = TimeUtil.transToTimestramp(year + '-' + (month+1) + '-1');
        // console.log("firstTime:"+firstTime);
        var ftime = firstTime - 86400;
        var d2 = TimeUtil.getDateInfo(ftime);
        // console.log("d2",d2);
        year = d2.year;
        month = d2.month;
    }
    // console.log('year-1:'+year+"  month-1:"+month);
    var arr = TimeUtil.getMonthInterval(year, month);
    arr['end'] = ttt['end'];
    return arr;
};
export {TimeUtil};
