import {BaseObjectWrapper, Util} from "./common";
import {getWxAuthService} from "../module/common/wxauth";
import {getSessionService} from "../module/common/session";
import {Toast} from "./Toast";
import PageUtil, {PageRedirectUtil} from "./PageUtil";
import {ToolUtil} from "./ToolUtil";
import {getUserService} from "../module/user/user";
import {UserUtil} from "./UserUtil";
import {ApiConst} from "../module/api/const";

const pageConfig = {};
pageConfig.loading = function () {
    if (this.loadingComponent) {
        this.loadingComponent.showLoading();
    }
};
pageConfig.closeLoading = function () {
    if (this.loadingComponent) {
        this.loadingComponent.hideAniEnd();
    }
};

//打开用户授权弹窗
pageConfig.openUserAuthPop = function () {
    console.log("-----------openUserAuth");
    console.log(this.data.accredit);
    this.setData({accredit: true});
    console.log(this.data.accredit);
};
//关闭授权弹窗
pageConfig.closeUserAuthPop = function () {
    this.setData({accredit: false});
};
//用户授权成功
pageConfig.userAuthCallback = function (e) {
    console.log("userAuthCallback", e);
    this.closeUserAuthPop();
    const config = this.data.innerConfig;
    console.log('config',config);
    this.initSessionAndUserInfo(config);
};

/**
 * 需要初始化用户信息时在页面调用
 * @param config
 *
 */
pageConfig.initSessionAndUserInfo = function (config) {
    console.log("config", config);
    this.setData({innerConfig: config});
    getWxAuthService().userAuth((hasAuth) => {
        if (!hasAuth) {
            console.log("用户授权");
            //未授权，弹出授权
            this.openUserAuthPop();
        } else {
            console.log("加载用户信息");
            this.loadSessionAndUserInfo(config);
        }
    });
};
//用户信息准备完成,如果需要使用用戶信息，需要先在页面实现该函数
//,不要直接在页面调用
/*pageConfig.userInfoReady = function () {
};*/
pageConfig.fetchUserInfo = function (config) {
    if (!(config instanceof BaseObjectWrapper)) {
        config = Util.createConfig(config);
    }
    const onlineUserCallback = config.func("online.callback");
    const erpUserCallback = config.func("erp.callback");
    const onlineFetchCache = config.bool("online.fetchCache", true);
    const erpFetchCache = config.bool("erp.fetchCache", true);
    const userLoadComplete = config.func("userLoadComplete");

    console.log("erp cache:" + erpFetchCache + "  online cache:" + onlineFetchCache);
    let userInfoReady = function () {

    };
    if (typeof this['userInfoReady'] == 'function') {
        userInfoReady = this['userInfoReady'];
    }
    // const userInfoReady = config.func('userInfoReady');

    ToolUtil.then((complete) => {
        getUserService().fetchOnlineUserInfo((response) => {
            console.log("online user info", UserUtil.getUserInfo());
            if (!response.isSuccess()) {
                return;
            }
            complete();
            onlineUserCallback();
        }, onlineFetchCache);
    }).then((complete) => {
        getUserService().fetchErpUserInfo((response) => {
            console.log("erp user info", UserUtil.getAccount());
            if (!response.isSuccess()) {
                return;
            }
            complete();
            erpUserCallback();
        }, erpFetchCache);
    }).then(() => {
        userInfoReady(true);
        userLoadComplete();
    }).execute();
};
//加载session和用户信息,不要直接在页面调用
pageConfig.loadSessionAndUserInfo = function (config) {
    if (!Util.isObj(config)) {
        config = {};
    }
    getSessionService().fetchSession((response) => {
        if (!response.isSuccess()) {
            // this.closeLoading();
            Toast.tip(response.getMsg());
            return;
        }
        const sessionBean = response.getData();
        console.log('sessionBean', sessionBean);
        if (sessionBean.hasRegister()) {
            //初始化信息
            config.userLoadComplete = () => {
                //用户加载完成，如果有重定向就到重定向页面
                PageRedirectUtil.redirect();
            };
            this.fetchUserInfo(config);
        } else {
            //未注册，前往注册页面
            PageUtil.redirectTo('register');
        }
    });
};
pageConfig.customOnload = function (options) {
    if (typeof this.selectComponent == 'function') {
        this.loadingComponent = this.selectComponent('#loading');
        this.dialog = this.selectComponent('#dialog');
    }
};
pageConfig.customOnready = function () {
    Util.networkCheck(() => {
        this.dialog.showWarning("请检查您的网络～", "网络未链接！");
    });
};
pageConfig.getValue = function (name, def) {
    return Util.get(this.data, name, def);
};
pageConfig.setValue = function (name, value) {
    return Util.set(this.data, name, value);
};
pageConfig.updateData = function (data) {
    Util.updateComponentData(this, data);
};
pageConfig.show = function (title, content, type) {
    content = content == null ? '' : content;
    type = type == null ? 'warning' : type;
    this.dialog.showDialog({
        'type': type,
        'title': title,
        'content': content
    });
};
pageConfig.showWarning = function (content, title) {
    if (title == 'error') {
        title = '出错啦';
    }
    this.show(title, content, 'warning');
};
pageConfig.setTitle = function (title) {
    wx.setNavigationBarTitle({title: title});
};
export default pageConfig;
