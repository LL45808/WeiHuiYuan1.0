import {Util} from "./common";

export const Toast = {
    show(msg) {
        wx.showToast({title: msg, icon: 'none', image: '/images/caution.png'});
    },
    tip(msg, config) {
        var ins = Util.createConfig(config);
        var duration = ins.getFloat('ttl', 1.5) * 1000;
        var callback = ins.func('end');
        wx.showToast(
            {
                title: msg,
                icon: 'none',
                duration: duration,
                complete: () => {
                    setTimeout(callback, duration);
                }
            }
        );
    },
    show2(msg, callback) {
        callback = Util.getFunction(callback);
        wx.showModal({
            content: msg,
            showCancel: false,
            success: (res) => {
                if (res.confirm) {
                    callback(true);
                } else if (res.cancel) {
                    callback(false);
                } else {
                    callback(false);
                }
            },
            fail: () => {

            },
            complete: () => {

            }
        });
    }
};

