import {TimeUtil} from './TimeUtil';
import ValidateUtil from "./ValidateUtil";
import {UrlUtil} from "./UrlUtil";

function namespaceHelper(name, container, separator) {
    var ns = name.split(separator || '.'), o = container, i, len;
    for (i = 0, len = ns.length; i < len; i++) {
        o = o[ns[i]] = o[ns[i]] || {};
    }
    return o;
}

var ObjUtil = {
    get: function (data, key, def) {
        if (typeof data != 'object' || data == null) {
            data = [];
        }
        if (typeof def == 'undefined') {
            def = null;
        }
        var list = String(key).split(".");
        if (list.length == 1) {
            return data[key] == null ? def : data[key];
        }
        var lastName = list.pop();//弹出最后一个
        var nowData = data;
        for (let i = 0; i < list.length; i++) {
            const name = list[i];
            if (typeof nowData[name] == 'undefined') {
                return def;
            }
            if (typeof nowData[name] != 'object') {
                return def;
            } else {
                nowData = nowData[name];
            }
        }
        return typeof nowData[lastName] == 'undefined' ? def : nowData[lastName];
    },
    set: function (data, key, value) {
        var list = key.split('.');
        var ln = list.pop();
        var object = data;
        if (list.length > 0) {
            object = namespaceHelper(key, data);
        }
        if (value == null) {
            delete object[ln];
        } else {
            object[ln] = value;
        }
        return object[ln];
    },
    /**
     * params覆盖data的参数,不存在则新增
     * @param data
     * @param params
     */
    cover: function (data, params) {
        var func = function (data, params) {
            for (var index in params) {
                var existKey = data[index] != null;
                if (!existKey) {
                    //若data种不存在该指，先初始化
                    data[index] = Util.initVar(params[index]);
                }
                if (typeof params[index] == 'object') {
                    var item = data[index];
                    func(item, params[index]);
                } else {
                    data[index] = params[index];
                }
            }
        };
        func(data, params);
    },
    exist: function (data, name) {
        return ObjUtil.get(data, name, null) != null;
    }
};

function BaseObjectWrapper() {
    var __data__ = [];
    var _this = this;
    var prefix = null;
    var time = new Date().getTime() + '_' + Math.random();
    var defaultFunc = function () {
    };
    this.getKey = function (name) {
        if (typeof name == 'number') {
            return String(name);
        }
        if (prefix == null) {
            return name;
        } else {
            var list = String(name).split(".");
            var v = [];
            for (var i = 0; i < list.length; i++) {
                v.push(prefix + list[i]);
            }
            return v.join('.');
        }
    };

    //内容不存在返回null
    this.get = function (key, def) {
        key = _this.getKey(key);
        var re = ObjUtil.get(__data__, key, def);
        return re;
    };

    this.bool = function (key, def) {
        var val = this.get(key, def);
        if (typeof val == 'boolean') {
            return val;
        } else {
            if (isNaN(val)) {
                return def;
            } else {
                return parseInt(val) > 0;
            }
        }
    };

    this.fetch = function (name, def) {
        // 不存在则设置默认值
        if (!_this.exist(name)) {
            if (typeof def == 'function') {
                _this.set(name, def());
            } else {
                _this.set(name, def);
            }
        }
        return _this.get(name);
    };

    this.set = function (name, value) {

        return ObjUtil.set(__data__, this.getKey(name), value);
    };

    this.del = function (name) {
        var value = _this.get(name, null);
        if (value != null) {
            _this.set(name, null);
        }
    };

    this.exist = function (name) {
        return _this.get(name, null) != null;
    };

    this.getData = function () {
        return __data__;
    };
    this.toArray = function () {
        return __data__;
    };
    this.func = function (name, def) {
        var v = _this.get(name, null);
        return typeof v != 'function' ? (def != null ? def : defaultFunc) : v;
    };
    this.getInt = function (key, def) {
        var r = this.get(key, def);
        return isNaN(r) ? 0 : parseInt(r);
    };
    this.getFloat = function (key, def) {
        var r = this.get(key, def);
        return isNaN(r) ? 0 : parseFloat(r);
    };

    this.setData = function (c) {
        __data__ = typeof c != 'object' ? [] : c;
        return _this;
    };
    this.setPrefix = function (_prefix) {
        prefix = _prefix;
        return _this;
    };
    this.size = function () {
        var s = 0;
        for (var i in __data__) s++;
        return s;
    };
}


var Util = {};
Util.clone = function (obj) {
    if (Util.isArray(obj)) {
        return [...obj];
    } else {
        return {...obj};
    }
};
Util.getObjUtil = function () {
    return ObjUtil;
};
Util.filterNull = function (value) {
    return Util.isEmpty(value) ? "" : value;
};
Util.retainPoint = function (number, scale, removeEndZero) {
    removeEndZero = typeof removeEndZero == 'boolean' ? removeEndZero : false;
    var list = String(number).split('.');
    if (list.length == 1) {
        return number;
    } else {
        var s1 = list[0];
        var s2 = list[1];
        var ls = [];
        for (let i = 0; i < s2.length; i++) {
            const ch = s2[i];
            if (i < scale) {
                var add = true;
                if (removeEndZero) {
                    add = !(ch == '0');
                }
                if (add) {
                    ls.push(ch);
                }
            }
        }
        if (ls.length > 0) {
            s2 = ls.join("");
        }
        var re = s1 + "." + s2;
        return parseFloat(re);
    }
};
Util.isArray = function (object) {
    return Object.prototype.toString.call(object) === '[object Array]';
};
Util.isObj = function (obj) {
    if (Util.isEmpty(obj)) {
        return false;
    }
    return typeof obj == 'object';
};
Util.initVar = function (value) {
    if (typeof value == 'object') {
        return Util.isArray(value) ? [] : {};
    }
    return '';
};


Util.updateObject = function (data, params) {
    ObjUtil.cover(data, params);
};
Util.updateComponentData2 = function (_this, params) {
    var data = _this.data;
    console.log("params", params);
    var func = function (data, params) {
        for (var index in params) {
            var existKey = data[index] != null;
            console.log("key:" + index + "  exist:" + existKey);
            if (!existKey) {
                //若data种不存在该key，先初始化
                data[index] = Util.initVar(params[index]);
            }
            if (typeof params[index] == 'object') {
                if (Util.isArray(params[index])) {
                    console.log('----1');
                    data[index] = params[index];
                } else {
                    console.log('----2');
                    var item = data[index];
                    func(item, params[index]);
                }
            } else {
                console.log('----3');
                data[index] = params[index];
            }
        }
    };
    func(data, params);
    _this.setData(data);
};

Util.updateComponentData = function (_this, params) {
    var data = _this.data;
    var func = function (data, params) {
        for (var index in params) {
            var existKey = data[index] != null;
            if (!existKey) {
                //若data种不存在该key，先初始化
                data[index] = Util.initVar(params[index]);
            }
            if (typeof params[index] == 'object') {
                if (Util.isArray(params[index])) {
                    data[index] = params[index];
                } else {
                    var item = data[index];
                    func(item, params[index]);
                }
            } else {
                data[index] = params[index];
            }
        }
    };
    func(data, params);
    _this.setData(data);
};

/**
 *
 * @param data
 * @param key   a.b.c
 * @param def  默认值
 * @returns {*}
 */
Util.get = function (data, key, def) {
    return ObjUtil.get(data, key, def);
};
/**
 *
 * @param data
 * @param key  a.b.c
 * @param value  c的值
 * @returns {*}
 */
Util.set = function (data, key, value) {
    return ObjUtil.set(data, key, value);
};
Util.getTimestamp = function (time) {
    return TimeUtil.getTimestamp(time);
};
Util.getNowDate = function (format) {
    return TimeUtil.getNowDate(format);
};

//苹果系统只对时间格式兼容问题
Util.getCompatibleDateFormat = function (str) {
    return String(str).replace(/-/g, "/");
};
//获取时间间隔，可阅读格式
Util.getNowDateInterval = function (_format, _interval) {

    var interval = _interval ? _interval : 0;
    var format = Util.getCompatibleDateFormat(_format);
    var arr = TimeUtil.getDayInterval(null, interval);
    var start = TimeUtil.timestampFormat(arr['start'], format);
    var end = TimeUtil.timestampFormat(arr['end'], format);
    return {start: start, end: end};
};

Util.intval = function (value) {
    return isNaN(value) ? 0 : parseInt(value);
};
Util.floatval = function (value, scale) {
    return isNaN(value) ? 0 : parseFloat(value).toFixed(scale);
}
Util.autoval = function (value, scale, def) {
    def = def == null ? 0 : def;
    if (isNaN(value)) {
        return def;
    } else {
        if (String(value).search(/\./) > 0) {
            return parseFloat(value).toFixed(scale);
        } else {
            return value;
        }
    }
};
Util.createConfig = function (config) {
    var obj = new BaseObjectWrapper();
    obj.setData(config);
    return obj;
};
//提供对象扩展功能
Util.objectExt = function (config) {
    var obj = new BaseObjectWrapper();
    obj.setData(config);
    return obj;
};
Util.createObj = function (data) {

    var Obj = function (data) {
        var arr = {};
        /**
         *
         * @param queryKey 通过url传递key
         * @param requestKey http请求传递给服务器的key
         */
        this.set = function (queryKey, requestKey) {
            if (data[queryKey] != null) {
                arr[requestKey] = data[queryKey];
            }
        };
        this.getObj = function () {
            return arr;
        };
    };
    return new Obj(data);
};

Util.debugInfo = [];
Util.start = function (key) {
    if (key == null) {
        key = 'debug';
    }
    Util.debugInfo[key] = {start: 0, end: 0};
    Util.debugInfo[key].start = TimeUtil.getNowTime();
};
Util.end = function (key) {
    if (key == null) {
        key = 'debug';
    }
    Util.debugInfo[key].end = TimeUtil.getNowTime();
    var info = Util.debugInfo[key];
    console.log('speed time:' + (info.end - info.start) + "ms");
};


Util.formatMoney = function (num) {
    if (isNaN(num)) {
        return 0;
    }
    var val = String(num);
    var end = '';
    if (val.indexOf(".") > 0) {
        var arr = val.split(".");
        end = '.' + arr[1];
        num = arr[0];
    }

    var num = (num || 0).toString(), result = '';
    while (num.length > 3) {
        result = ',' + num.slice(-3) + result;
        num = num.slice(0, num.length - 3);
    }
    if (num) {
        result = num + result;
    }
    return result + end;
};

Util.trim = function (a) {

    if (typeof (a) == "string") {
        return a.replace(/^\s*|\s*$/g, "");
    } else {
        return a;
    }
};
Util.isEmpty = function (a) {
    return ValidateUtil.isEmpty(a);
};
Util.getFunction = function (callback) {
    if (typeof callback == 'function') {
        return callback
    } else {
        return function () {
            return {};
        };
    }
};

Util.networkCheck = function (callback) {
    wx.onNetworkStatusChange(function (res) {
        if (!res.isConnected) {
            callback(false);
        } else {
            callback(true);
        }
    });
};
Util.getWebHost = function () {
   return UrlUtil.getRequestBaseUrl();
};

class Paging {

    _render = null;
    _pageSize = 10;
    _list = [];
    _totalPage = 0;
    _nowPage = 0;

    constructor(list, pageSize) {
        this._list = ValidateUtil.isArray(list) ? list : [];
        this._pageSize = parseFloat(pageSize);
        const count = this._list.length;
        this._totalPage = Math.ceil(count / pageSize);
    }

    /**
     *
     * @param render  回调，参数1是否结束,结束返回true，没有结束返回false,参数2,分页数据
     * @returns {Paging}
     */
    setRender(render) {
        this._render = render;
        return this;
    }

    callRender() {
        if (!this.enableLoad()) {
            this._render(true, []);
            return;
        }

        const start = this._nowPage * this._pageSize;
        const newList = this._list.slice(start, (start + this._pageSize));
        this._nowPage++;
        this._render(false, newList);
    }

    enableLoad() {
        console.log(this._nowPage + "  " + this._totalPage);
        return this._nowPage < this._totalPage;
    }

    execute() {
        if (typeof this._render == 'function') {
            this.callRender();
        }
    }
}
Util.createPaging=function (list,pageSize) {
    return new Paging(list,pageSize);
};
var CommonUtil = {};

const tempMap={};
Util.setTempValue=function (name,value) {
    tempMap[name]=value;
};
Util.getTempValue=function (name) {
    return tempMap[name];
};

Util.Rad = function (d) {
  return d * Math.PI / 180.0;//经纬度转换成三角函数中度分表形式。
};

Util.getDistance = function (origin, target) {
  var lat1 = parseFloat(origin.lat);
  var lng1 = parseFloat(origin.lng);
  var lat2 = parseFloat(target.lat);
  var lng2 = parseFloat(target.lng);

  var radLat1 = Util.Rad(lat1);
  var radLat2 = Util.Rad(lat2);
  var a = radLat1 - radLat2;
  var b = Util.Rad(lng1) - Util.Rad(lng2);
  var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
    Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
  s = s * 6378.137;// EARTH_RADIUS;
  s = Math.round(s * 10000) / 10000; //输出为公里
  //s=s.toFixed(4);
  return s;
};
export {Util, BaseObjectWrapper};
