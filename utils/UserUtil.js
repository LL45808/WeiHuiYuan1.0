import {Util} from "./common";

const USER_CONFIG_KEY = 'user_config_key';
const UserConfigUtil = {
    clearConfigInfo: function () {
        wx.setStorageSync(USER_CONFIG_KEY, null);
    },
    updateConfigInfo: function (data) {
        wx.setStorageSync(USER_CONFIG_KEY, data);
    },
    getConfig: function () {
        try {
            const value = wx.getStorageSync(USER_CONFIG_KEY);
            return value ? value : null;
        } catch (e) {
            return null;
        }
    },
    hasCheckUserShopInfo: function () {
        const config = this.getConfig();
        return Util.get(config, 'has_check_user_shop_info', false);
    },
    setCheckUserShopInfo: function (bool) {
        const config = this.getConfig();
        config.has_check_user_shop_info = bool;
        this.updateConfigInfo(config);
    },
    initConfig: function () {
        const config = this.getConfig();
        if (config == null) {
            this.updateConfigInfo({
                has_check_user_shop_info: false
            });
        }
    }
};

class AccountBean {

    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getIntegral() {
        return Util.floatval(this.data.get('intergral', 0), 3);
    }

    getCardId() {
        return this.data.get('card_id', '');
    }

    getBirthday() {
        return this.data.get('birthday');
    }

    getUserId() {
        return this.data.get('user_id', 0);
    }


    getVipId() {
        return this.data.get('vipId', 0);
    }

    getCardTypeStartDate() {
        return this.data.get('card_type_start_date', '');
    }

    getCardTypeEndDate() {
        return this.data.get('card_type_end_date', '');
    }

    reduceIntegral(number) {
        const account = getApp().storage.getAccount();
        account.intergral = this.getIntegral() - number;
        getApp().storage.setAccount(account);
    }

    addIntegral(number) {
        const account = getApp().storage.getAccount();
        account.intergral = this.getIntegral() + number;
        getApp().storage.setAccount(account);
    }

}

class UserBean {
    /**
     *
     * @type {BaseObjectWrapper}
     */
    data = null;

    constructor(data) {
        this.data = Util.objectExt(data);
    }

    getMobile() {
        return this.data.get('phone', '');
    }
}

const UserUtil = {
    //判断用户是否登录
    getToken() {
        return getApp().storage.getAuth().token;
    },
    getSessionKey() {
        return getApp().storage.getAuth().session_key;
    },
    getAccount() {
        return getApp().storage.getAccount();
    },
    getUserInfo() {
        return getApp().storage.getUserInfo();
    },
    getUniacid() {
        return getApp().apiUrls.uniacid;
    },
    getCardId() {
        const account = getApp().storage.getAccount();
        return Util.get(account, 'card_id', '');
    },
    getWxCardId() {
        const account = getApp().storage.getAccount();
        return Util.get(account, 'wx_card_config.card_id', '');
    },
    getWxCardWebUrl(uniacid, cardId) {
        var url = Util.getWebHost() + '/html/active.html?uniacid=' + uniacid + "&cardId=" + cardId + "&num=" + Math.random();
        return url;
    },
    getRichtextWebUrl(content) {
        var url = Util.getWebHost() + '/html/richtext.html?content=' + encodeURIComponent(content) + "&num=" + Math.random();
        return url;
    },
    getAccountBean() {
        return new AccountBean(getApp().storage.getAccount());
    },
    getUserBean() {
        return new UserBean(getApp().storage.getUserInfo());
    }
};

export function createOnlineUserBean(data) {
    return new UserBean(data);
}

export function createErpUserBean(data) {
    return new AccountBean(data);
}

export {UserUtil, UserConfigUtil};
