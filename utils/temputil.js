const map = [];

export function setTempValue(name, value) {
    map[name] = value;
}

export function getTempValue(name) {
    return map[name] ? map[name] : null;
}
